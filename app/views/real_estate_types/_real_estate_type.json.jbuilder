json.extract! real_estate_type, :id, :real_estate_type_desc, :created_at, :updated_at
json.url real_estate_type_url(real_estate_type, format: :json)
