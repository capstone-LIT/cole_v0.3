json.extract! stakeholder_status, :id, :stakeholder_status_desc, :created_at, :updated_at
json.url stakeholder_status_url(stakeholder_status, format: :json)
