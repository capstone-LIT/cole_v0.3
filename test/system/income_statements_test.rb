require "application_system_test_case"

class IncomeStatementsTest < ApplicationSystemTestCase
  setup do
    @income_statement = income_statements(:one)
  end

  test "visiting the index" do
    visit income_statements_url
    assert_selector "h1", text: "Income Statements"
  end

  test "creating a Income statement" do
    visit income_statements_url
    click_on "New Income Statement"

    fill_in "Advertising", with: @income_statement.advertising
    fill_in "Bad Debt", with: @income_statement.bad_debt
    fill_in "Bank Cc Fee", with: @income_statement.bank_cc_fee
    fill_in "Cost Of Goods", with: @income_statement.cost_of_goods
    fill_in "Dep An Am", with: @income_statement.dep_an_am
    fill_in "Emp Benefit Cost", with: @income_statement.emp_benefit_cost
    fill_in "Est Growth Rate", with: @income_statement.est_growth_rate
    fill_in "Income Stmt Status", with: @income_statement.income_stmt_status_id
    fill_in "Insurance Cost", with: @income_statement.insurance_cost
    fill_in "Interest Exp", with: @income_statement.interest_exp
    fill_in "Legal Acct Exp", with: @income_statement.legal_acct_exp
    fill_in "Maintenance", with: @income_statement.maintenance
    fill_in "Non Rec Exp", with: @income_statement.non_rec_exp
    fill_in "Organization", with: @income_statement.organization_id
    fill_in "Outside Exp", with: @income_statement.outside_exp
    fill_in "Own Insurance Exp", with: @income_statement.own_insurance_exp
    fill_in "Own Retirement", with: @income_statement.own_retirement
    fill_in "Own Veh Exp", with: @income_statement.own_veh_exp
    fill_in "Owners Travel Exp", with: @income_statement.owners_travel_exp
    fill_in "Partner Comp", with: @income_statement.partner_comp
    fill_in "Payroll", with: @income_statement.payroll
    fill_in "Pension Profit Share", with: @income_statement.pension_profit_share
    fill_in "Pretax Profit", with: @income_statement.pretax_profit
    fill_in "Rent", with: @income_statement.rent
    fill_in "Sal Adj", with: @income_statement.sal_adj
    fill_in "Stmt Comment", with: @income_statement.stmt_comment
    fill_in "Stmt Year", with: @income_statement.stmt_year
    fill_in "Tax And Lic", with: @income_statement.tax_and_lic
    fill_in "Total Sales", with: @income_statement.total_sales
    fill_in "Utility", with: @income_statement.utility
    fill_in "Veh Exp", with: @income_statement.veh_exp
    click_on "Create Income statement"

    assert_text "Income statement was successfully created"
    click_on "Back"
  end

  test "updating a Income statement" do
    visit income_statements_url
    click_on "Edit", match: :first

    fill_in "Advertising", with: @income_statement.advertising
    fill_in "Bad Debt", with: @income_statement.bad_debt
    fill_in "Bank Cc Fee", with: @income_statement.bank_cc_fee
    fill_in "Cost Of Goods", with: @income_statement.cost_of_goods
    fill_in "Dep An Am", with: @income_statement.dep_an_am
    fill_in "Emp Benefit Cost", with: @income_statement.emp_benefit_cost
    fill_in "Est Growth Rate", with: @income_statement.est_growth_rate
    fill_in "Income Stmt Status", with: @income_statement.income_stmt_status_id
    fill_in "Insurance Cost", with: @income_statement.insurance_cost
    fill_in "Interest Exp", with: @income_statement.interest_exp
    fill_in "Legal Acct Exp", with: @income_statement.legal_acct_exp
    fill_in "Maintenance", with: @income_statement.maintenance
    fill_in "Non Rec Exp", with: @income_statement.non_rec_exp
    fill_in "Organization", with: @income_statement.organization_id
    fill_in "Outside Exp", with: @income_statement.outside_exp
    fill_in "Own Insurance Exp", with: @income_statement.own_insurance_exp
    fill_in "Own Retirement", with: @income_statement.own_retirement
    fill_in "Own Veh Exp", with: @income_statement.own_veh_exp
    fill_in "Owners Travel Exp", with: @income_statement.owners_travel_exp
    fill_in "Partner Comp", with: @income_statement.partner_comp
    fill_in "Payroll", with: @income_statement.payroll
    fill_in "Pension Profit Share", with: @income_statement.pension_profit_share
    fill_in "Pretax Profit", with: @income_statement.pretax_profit
    fill_in "Rent", with: @income_statement.rent
    fill_in "Sal Adj", with: @income_statement.sal_adj
    fill_in "Stmt Comment", with: @income_statement.stmt_comment
    fill_in "Stmt Year", with: @income_statement.stmt_year
    fill_in "Tax And Lic", with: @income_statement.tax_and_lic
    fill_in "Total Sales", with: @income_statement.total_sales
    fill_in "Utility", with: @income_statement.utility
    fill_in "Veh Exp", with: @income_statement.veh_exp
    click_on "Update Income statement"

    assert_text "Income statement was successfully updated"
    click_on "Back"
  end

  test "destroying a Income statement" do
    visit income_statements_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Income statement was successfully destroyed"
  end
end
