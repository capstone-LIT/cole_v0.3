class CreateHistEarningRankings < ActiveRecord::Migration[5.2]
  def change
    create_table :hist_earning_rankings do |t|
      t.string :hist_earning_desc

      t.timestamps
    end
  end
end
