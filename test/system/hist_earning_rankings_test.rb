require "application_system_test_case"

class HistEarningRankingsTest < ApplicationSystemTestCase
  setup do
    @hist_earning_ranking = hist_earning_rankings(:one)
  end

  test "visiting the index" do
    visit hist_earning_rankings_url
    assert_selector "h1", text: "Hist Earning Rankings"
  end

  test "creating a Hist earning ranking" do
    visit hist_earning_rankings_url
    click_on "New Hist Earning Ranking"

    fill_in "Hist Earning Desc", with: @hist_earning_ranking.hist_earning_desc
    click_on "Create Hist earning ranking"

    assert_text "Hist earning ranking was successfully created"
    click_on "Back"
  end

  test "updating a Hist earning ranking" do
    visit hist_earning_rankings_url
    click_on "Edit", match: :first

    fill_in "Hist Earning Desc", with: @hist_earning_ranking.hist_earning_desc
    click_on "Update Hist earning ranking"

    assert_text "Hist earning ranking was successfully updated"
    click_on "Back"
  end

  test "destroying a Hist earning ranking" do
    visit hist_earning_rankings_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Hist earning ranking was successfully destroyed"
  end
end
