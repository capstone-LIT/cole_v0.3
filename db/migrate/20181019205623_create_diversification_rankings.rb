class CreateDiversificationRankings < ActiveRecord::Migration[5.2]
  def change
    create_table :diversification_rankings do |t|
      t.string :diversification_desc

      t.timestamps
    end
  end
end
