class IncomeStmtStatusesController < ApplicationController
  before_action :set_income_stmt_status, only: [:show, :edit, :update, :destroy]

  # GET /income_stmt_statuses
  # GET /income_stmt_statuses.json
  def index
    @income_stmt_statuses = IncomeStmtStatus.all
  end

  # GET /income_stmt_statuses/1
  # GET /income_stmt_statuses/1.json
  def show
  end

  # GET /income_stmt_statuses/new
  def new
    @income_stmt_status = IncomeStmtStatus.new
  end

  # GET /income_stmt_statuses/1/edit
  def edit
  end

  # POST /income_stmt_statuses
  # POST /income_stmt_statuses.json
  def create
    @income_stmt_status = IncomeStmtStatus.new(income_stmt_status_params)

    respond_to do |format|
      if @income_stmt_status.save
        format.html { redirect_to @income_stmt_status, notice: 'Income stmt status was successfully created.' }
        format.json { render :show, status: :created, location: @income_stmt_status }
      else
        format.html { render :new }
        format.json { render json: @income_stmt_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /income_stmt_statuses/1
  # PATCH/PUT /income_stmt_statuses/1.json
  def update
    respond_to do |format|
      if @income_stmt_status.update(income_stmt_status_params)
        format.html { redirect_to @income_stmt_status, notice: 'Income stmt status was successfully updated.' }
        format.json { render :show, status: :ok, location: @income_stmt_status }
      else
        format.html { render :edit }
        format.json { render json: @income_stmt_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /income_stmt_statuses/1
  # DELETE /income_stmt_statuses/1.json
  def destroy
    @income_stmt_status.destroy
    respond_to do |format|
      format.html { redirect_to income_stmt_statuses_url, notice: 'Income stmt status was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_income_stmt_status
      @income_stmt_status = IncomeStmtStatus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def income_stmt_status_params
      params.require(:income_stmt_status).permit(:stmt_status_desc)
    end
end
