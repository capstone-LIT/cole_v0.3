class StakeholderStatusesController < ApplicationController
  before_action :set_stakeholder_status, only: [:show, :edit, :update, :destroy]

  # GET /stakeholder_statuses
  # GET /stakeholder_statuses.json
  def index
    @stakeholder_statuses = StakeholderStatus.all
  end

  # GET /stakeholder_statuses/1
  # GET /stakeholder_statuses/1.json
  def show
  end

  # GET /stakeholder_statuses/new
  def new
    @stakeholder_status = StakeholderStatus.new
  end

  # GET /stakeholder_statuses/1/edit
  def edit
  end

  # POST /stakeholder_statuses
  # POST /stakeholder_statuses.json
  def create
    @stakeholder_status = StakeholderStatus.new(stakeholder_status_params)

    respond_to do |format|
      if @stakeholder_status.save
        format.html { redirect_to @stakeholder_status, notice: 'Stakeholder status was successfully created.' }
        format.json { render :show, status: :created, location: @stakeholder_status }
      else
        format.html { render :new }
        format.json { render json: @stakeholder_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /stakeholder_statuses/1
  # PATCH/PUT /stakeholder_statuses/1.json
  def update
    respond_to do |format|
      if @stakeholder_status.update(stakeholder_status_params)
        format.html { redirect_to @stakeholder_status, notice: 'Stakeholder status was successfully updated.' }
        format.json { render :show, status: :ok, location: @stakeholder_status }
      else
        format.html { render :edit }
        format.json { render json: @stakeholder_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stakeholder_statuses/1
  # DELETE /stakeholder_statuses/1.json
  def destroy
    @stakeholder_status.destroy
    respond_to do |format|
      format.html { redirect_to stakeholder_statuses_url, notice: 'Stakeholder status was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_stakeholder_status
      @stakeholder_status = StakeholderStatus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def stakeholder_status_params
      params.require(:stakeholder_status).permit(:stakeholder_status_desc)
    end
end
