require "application_system_test_case"

class AppraisalCalcStatusesTest < ApplicationSystemTestCase
  setup do
    @appraisal_calc_status = appraisal_calc_statuses(:one)
  end

  test "visiting the index" do
    visit appraisal_calc_statuses_url
    assert_selector "h1", text: "Appraisal Calc Statuses"
  end

  test "creating a Appraisal calc status" do
    visit appraisal_calc_statuses_url
    click_on "New Appraisal Calc Status"

    fill_in "Calc Status Desc", with: @appraisal_calc_status.calc_status_desc
    click_on "Create Appraisal calc status"

    assert_text "Appraisal calc status was successfully created"
    click_on "Back"
  end

  test "updating a Appraisal calc status" do
    visit appraisal_calc_statuses_url
    click_on "Edit", match: :first

    fill_in "Calc Status Desc", with: @appraisal_calc_status.calc_status_desc
    click_on "Update Appraisal calc status"

    assert_text "Appraisal calc status was successfully updated"
    click_on "Back"
  end

  test "destroying a Appraisal calc status" do
    visit appraisal_calc_statuses_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Appraisal calc status was successfully destroyed"
  end
end
