class FinancialSourceTypesController < ApplicationController
  before_action :set_financial_source_type, only: [:show, :edit, :update, :destroy]

  # GET /financial_source_types
  # GET /financial_source_types.json
  def index
    @financial_source_types = FinancialSourceType.all
  end

  # GET /financial_source_types/1
  # GET /financial_source_types/1.json
  def show
  end

  # GET /financial_source_types/new
  def new
    @financial_source_type = FinancialSourceType.new
  end

  # GET /financial_source_types/1/edit
  def edit
  end

  # POST /financial_source_types
  # POST /financial_source_types.json
  def create
    @financial_source_type = FinancialSourceType.new(financial_source_type_params)

    respond_to do |format|
      if @financial_source_type.save
        format.html { redirect_to @financial_source_type, notice: 'Financial source type was successfully created.' }
        format.json { render :show, status: :created, location: @financial_source_type }
      else
        format.html { render :new }
        format.json { render json: @financial_source_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /financial_source_types/1
  # PATCH/PUT /financial_source_types/1.json
  def update
    respond_to do |format|
      if @financial_source_type.update(financial_source_type_params)
        format.html { redirect_to @financial_source_type, notice: 'Financial source type was successfully updated.' }
        format.json { render :show, status: :ok, location: @financial_source_type }
      else
        format.html { render :edit }
        format.json { render json: @financial_source_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /financial_source_types/1
  # DELETE /financial_source_types/1.json
  def destroy
    @financial_source_type.destroy
    respond_to do |format|
      format.html { redirect_to financial_source_types_url, notice: 'Financial source type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_financial_source_type
      @financial_source_type = FinancialSourceType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def financial_source_type_params
      params.require(:financial_source_type).permit(:financial_source_type_desc)
    end
end
