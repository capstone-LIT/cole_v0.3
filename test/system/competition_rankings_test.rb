require "application_system_test_case"

class CompetitionRankingsTest < ApplicationSystemTestCase
  setup do
    @competition_ranking = competition_rankings(:one)
  end

  test "visiting the index" do
    visit competition_rankings_url
    assert_selector "h1", text: "Competition Rankings"
  end

  test "creating a Competition ranking" do
    visit competition_rankings_url
    click_on "New Competition Ranking"

    fill_in "Competition Desc", with: @competition_ranking.competition_desc
    click_on "Create Competition ranking"

    assert_text "Competition ranking was successfully created"
    click_on "Back"
  end

  test "updating a Competition ranking" do
    visit competition_rankings_url
    click_on "Edit", match: :first

    fill_in "Competition Desc", with: @competition_ranking.competition_desc
    click_on "Update Competition ranking"

    assert_text "Competition ranking was successfully updated"
    click_on "Back"
  end

  test "destroying a Competition ranking" do
    visit competition_rankings_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Competition ranking was successfully destroyed"
  end
end
