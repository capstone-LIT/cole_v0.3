json.extract! depth_of_management_ranking, :id, :depth_of_mgmt_desc, :created_at, :updated_at
json.url depth_of_management_ranking_url(depth_of_management_ranking, format: :json)
