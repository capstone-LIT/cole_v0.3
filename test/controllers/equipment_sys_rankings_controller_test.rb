require 'test_helper'

class EquipmentSysRankingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @equipment_sys_ranking = equipment_sys_rankings(:one)
  end

  test "should get index" do
    get equipment_sys_rankings_url
    assert_response :success
  end

  test "should get new" do
    get new_equipment_sys_ranking_url
    assert_response :success
  end

  test "should create equipment_sys_ranking" do
    assert_difference('EquipmentSysRanking.count') do
      post equipment_sys_rankings_url, params: { equipment_sys_ranking: { eqp_sys_desc: @equipment_sys_ranking.eqp_sys_desc } }
    end

    assert_redirected_to equipment_sys_ranking_url(EquipmentSysRanking.last)
  end

  test "should show equipment_sys_ranking" do
    get equipment_sys_ranking_url(@equipment_sys_ranking)
    assert_response :success
  end

  test "should get edit" do
    get edit_equipment_sys_ranking_url(@equipment_sys_ranking)
    assert_response :success
  end

  test "should update equipment_sys_ranking" do
    patch equipment_sys_ranking_url(@equipment_sys_ranking), params: { equipment_sys_ranking: { eqp_sys_desc: @equipment_sys_ranking.eqp_sys_desc } }
    assert_redirected_to equipment_sys_ranking_url(@equipment_sys_ranking)
  end

  test "should destroy equipment_sys_ranking" do
    assert_difference('EquipmentSysRanking.count', -1) do
      delete equipment_sys_ranking_url(@equipment_sys_ranking)
    end

    assert_redirected_to equipment_sys_rankings_url
  end
end
