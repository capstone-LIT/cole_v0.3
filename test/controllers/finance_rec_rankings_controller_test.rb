require 'test_helper'

class FinanceRecRankingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @finance_rec_ranking = finance_rec_rankings(:one)
  end

  test "should get index" do
    get finance_rec_rankings_url
    assert_response :success
  end

  test "should get new" do
    get new_finance_rec_ranking_url
    assert_response :success
  end

  test "should create finance_rec_ranking" do
    assert_difference('FinanceRecRanking.count') do
      post finance_rec_rankings_url, params: { finance_rec_ranking: { finance_rec_desc: @finance_rec_ranking.finance_rec_desc } }
    end

    assert_redirected_to finance_rec_ranking_url(FinanceRecRanking.last)
  end

  test "should show finance_rec_ranking" do
    get finance_rec_ranking_url(@finance_rec_ranking)
    assert_response :success
  end

  test "should get edit" do
    get edit_finance_rec_ranking_url(@finance_rec_ranking)
    assert_response :success
  end

  test "should update finance_rec_ranking" do
    patch finance_rec_ranking_url(@finance_rec_ranking), params: { finance_rec_ranking: { finance_rec_desc: @finance_rec_ranking.finance_rec_desc } }
    assert_redirected_to finance_rec_ranking_url(@finance_rec_ranking)
  end

  test "should destroy finance_rec_ranking" do
    assert_difference('FinanceRecRanking.count', -1) do
      delete finance_rec_ranking_url(@finance_rec_ranking)
    end

    assert_redirected_to finance_rec_rankings_url
  end
end
