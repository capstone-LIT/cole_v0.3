require 'test_helper'

class DiversificationRankingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @diversification_ranking = diversification_rankings(:one)
  end

  test "should get index" do
    get diversification_rankings_url
    assert_response :success
  end

  test "should get new" do
    get new_diversification_ranking_url
    assert_response :success
  end

  test "should create diversification_ranking" do
    assert_difference('DiversificationRanking.count') do
      post diversification_rankings_url, params: { diversification_ranking: { diversification_desc: @diversification_ranking.diversification_desc } }
    end

    assert_redirected_to diversification_ranking_url(DiversificationRanking.last)
  end

  test "should show diversification_ranking" do
    get diversification_ranking_url(@diversification_ranking)
    assert_response :success
  end

  test "should get edit" do
    get edit_diversification_ranking_url(@diversification_ranking)
    assert_response :success
  end

  test "should update diversification_ranking" do
    patch diversification_ranking_url(@diversification_ranking), params: { diversification_ranking: { diversification_desc: @diversification_ranking.diversification_desc } }
    assert_redirected_to diversification_ranking_url(@diversification_ranking)
  end

  test "should destroy diversification_ranking" do
    assert_difference('DiversificationRanking.count', -1) do
      delete diversification_ranking_url(@diversification_ranking)
    end

    assert_redirected_to diversification_rankings_url
  end
end
