class ApplicationMailer < ActionMailer::Base
  default from: 'LimitlessIT.UH@gmail.com'
  layout 'mailer'
end
