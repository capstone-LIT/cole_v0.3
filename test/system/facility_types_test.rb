require "application_system_test_case"

class FacilityTypesTest < ApplicationSystemTestCase
  setup do
    @facility_type = facility_types(:one)
  end

  test "visiting the index" do
    visit facility_types_url
    assert_selector "h1", text: "Facility Types"
  end

  test "creating a Facility type" do
    visit facility_types_url
    click_on "New Facility Type"

    fill_in "Facility Type Desc", with: @facility_type.facility_type_desc
    click_on "Create Facility type"

    assert_text "Facility type was successfully created"
    click_on "Back"
  end

  test "updating a Facility type" do
    visit facility_types_url
    click_on "Edit", match: :first

    fill_in "Facility Type Desc", with: @facility_type.facility_type_desc
    click_on "Update Facility type"

    assert_text "Facility type was successfully updated"
    click_on "Back"
  end

  test "destroying a Facility type" do
    visit facility_types_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Facility type was successfully destroyed"
  end
end
