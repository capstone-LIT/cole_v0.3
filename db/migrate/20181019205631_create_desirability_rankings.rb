class CreateDesirabilityRankings < ActiveRecord::Migration[5.2]
  def change
    create_table :desirability_rankings do |t|
      t.string :desirability_desc

      t.timestamps
    end
  end
end
