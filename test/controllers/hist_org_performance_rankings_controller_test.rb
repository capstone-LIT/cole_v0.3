require 'test_helper'

class HistOrgPerformanceRankingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @hist_org_performance_ranking = hist_org_performance_rankings(:one)
  end

  test "should get index" do
    get hist_org_performance_rankings_url
    assert_response :success
  end

  test "should get new" do
    get new_hist_org_performance_ranking_url
    assert_response :success
  end

  test "should create hist_org_performance_ranking" do
    assert_difference('HistOrgPerformanceRanking.count') do
      post hist_org_performance_rankings_url, params: { hist_org_performance_ranking: { hist_org_performance_ranking_desc: @hist_org_performance_ranking.hist_org_performance_ranking_desc } }
    end

    assert_redirected_to hist_org_performance_ranking_url(HistOrgPerformanceRanking.last)
  end

  test "should show hist_org_performance_ranking" do
    get hist_org_performance_ranking_url(@hist_org_performance_ranking)
    assert_response :success
  end

  test "should get edit" do
    get edit_hist_org_performance_ranking_url(@hist_org_performance_ranking)
    assert_response :success
  end

  test "should update hist_org_performance_ranking" do
    patch hist_org_performance_ranking_url(@hist_org_performance_ranking), params: { hist_org_performance_ranking: { hist_org_performance_ranking_desc: @hist_org_performance_ranking.hist_org_performance_ranking_desc } }
    assert_redirected_to hist_org_performance_ranking_url(@hist_org_performance_ranking)
  end

  test "should destroy hist_org_performance_ranking" do
    assert_difference('HistOrgPerformanceRanking.count', -1) do
      delete hist_org_performance_ranking_url(@hist_org_performance_ranking)
    end

    assert_redirected_to hist_org_performance_rankings_url
  end
end
