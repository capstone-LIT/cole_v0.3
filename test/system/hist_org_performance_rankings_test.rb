require "application_system_test_case"

class HistOrgPerformanceRankingsTest < ApplicationSystemTestCase
  setup do
    @hist_org_performance_ranking = hist_org_performance_rankings(:one)
  end

  test "visiting the index" do
    visit hist_org_performance_rankings_url
    assert_selector "h1", text: "Hist Org Performance Rankings"
  end

  test "creating a Hist org performance ranking" do
    visit hist_org_performance_rankings_url
    click_on "New Hist Org Performance Ranking"

    fill_in "Hist Org Performance Ranking Desc", with: @hist_org_performance_ranking.hist_org_performance_ranking_desc
    click_on "Create Hist org performance ranking"

    assert_text "Hist org performance ranking was successfully created"
    click_on "Back"
  end

  test "updating a Hist org performance ranking" do
    visit hist_org_performance_rankings_url
    click_on "Edit", match: :first

    fill_in "Hist Org Performance Ranking Desc", with: @hist_org_performance_ranking.hist_org_performance_ranking_desc
    click_on "Update Hist org performance ranking"

    assert_text "Hist org performance ranking was successfully updated"
    click_on "Back"
  end

  test "destroying a Hist org performance ranking" do
    visit hist_org_performance_rankings_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Hist org performance ranking was successfully destroyed"
  end
end
