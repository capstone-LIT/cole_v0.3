json.extract! stakeholder_rank, :id, :stakeholder_rank_desc, :created_at, :updated_at
json.url stakeholder_rank_url(stakeholder_rank, format: :json)
