require 'test_helper'

class IncomeStatementsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @income_statement = income_statements(:one)
  end

  test "should get index" do
    get income_statements_url
    assert_response :success
  end

  test "should get new" do
    get new_income_statement_url
    assert_response :success
  end

  test "should create income_statement" do
    assert_difference('IncomeStatement.count') do
      post income_statements_url, params: { income_statement: { advertising: @income_statement.advertising, bad_debt: @income_statement.bad_debt, bank_cc_fee: @income_statement.bank_cc_fee, cost_of_goods: @income_statement.cost_of_goods, dep_an_am: @income_statement.dep_an_am, emp_benefit_cost: @income_statement.emp_benefit_cost, est_growth_rate: @income_statement.est_growth_rate, income_stmt_status_id: @income_statement.income_stmt_status_id, insurance_cost: @income_statement.insurance_cost, interest_exp: @income_statement.interest_exp, legal_acct_exp: @income_statement.legal_acct_exp, maintenance: @income_statement.maintenance, non_rec_exp: @income_statement.non_rec_exp, organization_id: @income_statement.organization_id, outside_exp: @income_statement.outside_exp, own_insurance_exp: @income_statement.own_insurance_exp, own_retirement: @income_statement.own_retirement, own_veh_exp: @income_statement.own_veh_exp, owners_travel_exp: @income_statement.owners_travel_exp, partner_comp: @income_statement.partner_comp, payroll: @income_statement.payroll, pension_profit_share: @income_statement.pension_profit_share, pretax_profit: @income_statement.pretax_profit, rent: @income_statement.rent, sal_adj: @income_statement.sal_adj, stmt_comment: @income_statement.stmt_comment, stmt_year: @income_statement.stmt_year, tax_and_lic: @income_statement.tax_and_lic, total_sales: @income_statement.total_sales, utility: @income_statement.utility, veh_exp: @income_statement.veh_exp } }
    end

    assert_redirected_to income_statement_url(IncomeStatement.last)
  end

  test "should show income_statement" do
    get income_statement_url(@income_statement)
    assert_response :success
  end

  test "should get edit" do
    get edit_income_statement_url(@income_statement)
    assert_response :success
  end

  test "should update income_statement" do
    patch income_statement_url(@income_statement), params: { income_statement: { advertising: @income_statement.advertising, bad_debt: @income_statement.bad_debt, bank_cc_fee: @income_statement.bank_cc_fee, cost_of_goods: @income_statement.cost_of_goods, dep_an_am: @income_statement.dep_an_am, emp_benefit_cost: @income_statement.emp_benefit_cost, est_growth_rate: @income_statement.est_growth_rate, income_stmt_status_id: @income_statement.income_stmt_status_id, insurance_cost: @income_statement.insurance_cost, interest_exp: @income_statement.interest_exp, legal_acct_exp: @income_statement.legal_acct_exp, maintenance: @income_statement.maintenance, non_rec_exp: @income_statement.non_rec_exp, organization_id: @income_statement.organization_id, outside_exp: @income_statement.outside_exp, own_insurance_exp: @income_statement.own_insurance_exp, own_retirement: @income_statement.own_retirement, own_veh_exp: @income_statement.own_veh_exp, owners_travel_exp: @income_statement.owners_travel_exp, partner_comp: @income_statement.partner_comp, payroll: @income_statement.payroll, pension_profit_share: @income_statement.pension_profit_share, pretax_profit: @income_statement.pretax_profit, rent: @income_statement.rent, sal_adj: @income_statement.sal_adj, stmt_comment: @income_statement.stmt_comment, stmt_year: @income_statement.stmt_year, tax_and_lic: @income_statement.tax_and_lic, total_sales: @income_statement.total_sales, utility: @income_statement.utility, veh_exp: @income_statement.veh_exp } }
    assert_redirected_to income_statement_url(@income_statement)
  end

  test "should destroy income_statement" do
    assert_difference('IncomeStatement.count', -1) do
      delete income_statement_url(@income_statement)
    end

    assert_redirected_to income_statements_url
  end
end
