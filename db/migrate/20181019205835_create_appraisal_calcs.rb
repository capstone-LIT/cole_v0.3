class CreateAppraisalCalcs < ActiveRecord::Migration[5.2]
  def change
    create_table :appraisal_calcs do |t|
      t.references :organization, foreign_key: true
      t.float :year_one_conf
      t.float :year_two_conf
      t.float :year_three_conf
      t.float :projected_conf
      t.integer :trans_found
      t.float :mult_of_rev
      t.float :mult_of_rev_var
      t.float :mult_of_sde
      t.float :mult_of_sde_var
      t.float :est_val_rev_conf
      t.float :est_value_sde_conf
      t.float :asset_approach_conf
      t.float :income_approach_conf
      t.float :market_approach_conf
      t.float :buyer_down_pmt
      t.float :debt_assum
      t.float :seller_finance
      t.float :comm_finance
      t.float :cont_earn_out
      t.date :report_date
      t.string :interim_start
      t.string :interim_end
      t.references :business_type, foreign_key: true
      t.references :facility_type, foreign_key: true
      t.float :est_loan_fee
      t.integer :term
      t.float :capt_expend_est
      t.float :new_owner_sal
      t.references :appraisal_calc_status, foreign_key: true

      t.timestamps
    end
  end
end
