class LocAndFacRankingsController < ApplicationController
  before_action :set_loc_and_fac_ranking, only: [:show, :edit, :update, :destroy]

  # GET /loc_and_fac_rankings
  # GET /loc_and_fac_rankings.json
  def index
    @loc_and_fac_rankings = LocAndFacRanking.all
  end

  # GET /loc_and_fac_rankings/1
  # GET /loc_and_fac_rankings/1.json
  def show
  end

  # GET /loc_and_fac_rankings/new
  def new
    @loc_and_fac_ranking = LocAndFacRanking.new
  end

  # GET /loc_and_fac_rankings/1/edit
  def edit
  end

  # POST /loc_and_fac_rankings
  # POST /loc_and_fac_rankings.json
  def create
    @loc_and_fac_ranking = LocAndFacRanking.new(loc_and_fac_ranking_params)

    respond_to do |format|
      if @loc_and_fac_ranking.save
        format.html { redirect_to @loc_and_fac_ranking, notice: 'Loc and fac ranking was successfully created.' }
        format.json { render :show, status: :created, location: @loc_and_fac_ranking }
      else
        format.html { render :new }
        format.json { render json: @loc_and_fac_ranking.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /loc_and_fac_rankings/1
  # PATCH/PUT /loc_and_fac_rankings/1.json
  def update
    respond_to do |format|
      if @loc_and_fac_ranking.update(loc_and_fac_ranking_params)
        format.html { redirect_to @loc_and_fac_ranking, notice: 'Loc and fac ranking was successfully updated.' }
        format.json { render :show, status: :ok, location: @loc_and_fac_ranking }
      else
        format.html { render :edit }
        format.json { render json: @loc_and_fac_ranking.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /loc_and_fac_rankings/1
  # DELETE /loc_and_fac_rankings/1.json
  def destroy
    @loc_and_fac_ranking.destroy
    respond_to do |format|
      format.html { redirect_to loc_and_fac_rankings_url, notice: 'Loc and fac ranking was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_loc_and_fac_ranking
      @loc_and_fac_ranking = LocAndFacRanking.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def loc_and_fac_ranking_params
      params.require(:loc_and_fac_ranking).permit(:loc_and_fac_desc)
    end
end
