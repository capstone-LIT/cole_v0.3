require "application_system_test_case"

class BalanceSheetStatusesTest < ApplicationSystemTestCase
  setup do
    @balance_sheet_status = balance_sheet_statuses(:one)
  end

  test "visiting the index" do
    visit balance_sheet_statuses_url
    assert_selector "h1", text: "Balance Sheet Statuses"
  end

  test "creating a Balance sheet status" do
    visit balance_sheet_statuses_url
    click_on "New Balance Sheet Status"

    fill_in "Sheet Desc", with: @balance_sheet_status.sheet_desc
    click_on "Create Balance sheet status"

    assert_text "Balance sheet status was successfully created"
    click_on "Back"
  end

  test "updating a Balance sheet status" do
    visit balance_sheet_statuses_url
    click_on "Edit", match: :first

    fill_in "Sheet Desc", with: @balance_sheet_status.sheet_desc
    click_on "Update Balance sheet status"

    assert_text "Balance sheet status was successfully updated"
    click_on "Back"
  end

  test "destroying a Balance sheet status" do
    visit balance_sheet_statuses_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Balance sheet status was successfully destroyed"
  end
end
