require 'test_helper'

class EmployeeSkillTurnoverRankingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @employee_skill_turnover_ranking = employee_skill_turnover_rankings(:one)
  end

  test "should get index" do
    get employee_skill_turnover_rankings_url
    assert_response :success
  end

  test "should get new" do
    get new_employee_skill_turnover_ranking_url
    assert_response :success
  end

  test "should create employee_skill_turnover_ranking" do
    assert_difference('EmployeeSkillTurnoverRanking.count') do
      post employee_skill_turnover_rankings_url, params: { employee_skill_turnover_ranking: { emp_skill_turn_desc: @employee_skill_turnover_ranking.emp_skill_turn_desc } }
    end

    assert_redirected_to employee_skill_turnover_ranking_url(EmployeeSkillTurnoverRanking.last)
  end

  test "should show employee_skill_turnover_ranking" do
    get employee_skill_turnover_ranking_url(@employee_skill_turnover_ranking)
    assert_response :success
  end

  test "should get edit" do
    get edit_employee_skill_turnover_ranking_url(@employee_skill_turnover_ranking)
    assert_response :success
  end

  test "should update employee_skill_turnover_ranking" do
    patch employee_skill_turnover_ranking_url(@employee_skill_turnover_ranking), params: { employee_skill_turnover_ranking: { emp_skill_turn_desc: @employee_skill_turnover_ranking.emp_skill_turn_desc } }
    assert_redirected_to employee_skill_turnover_ranking_url(@employee_skill_turnover_ranking)
  end

  test "should destroy employee_skill_turnover_ranking" do
    assert_difference('EmployeeSkillTurnoverRanking.count', -1) do
      delete employee_skill_turnover_ranking_url(@employee_skill_turnover_ranking)
    end

    assert_redirected_to employee_skill_turnover_rankings_url
  end
end
