json.extract! financial_source_type, :id, :financial_source_type_desc, :created_at, :updated_at
json.url financial_source_type_url(financial_source_type, format: :json)
