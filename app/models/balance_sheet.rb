class BalanceSheet < ApplicationRecord
  belongs_to :organization
  belongs_to :financial_source_type
  belongs_to :accounting_method
  belongs_to :balance_sheet_status
  before_save :update_status
  before_save :calculate_fields
  after_save :check_completion


  public
    def calc_cash_adjustment
      0 - self.cash_on_hand
    end
    def calc_ar_adjustment
      0 - self.accounts_r
    end
    def calc_inv_adjustment
      self.inv_req - self.inventory
      end
    def calc_ffev_adjustment
      self.curr_ffe_v - self.ffe_v
    end
    def calc_estate_adjustment
      0 - self.real_estate_value
      end
    def calc_deprec_and_amort
      self.accum_a + self.accum_d
      end
    def calc_d_a_adjustment
      0 - self.calc_deprec_and_amort
      end
    def calc_other_asset_adjustment
      0 - self.other_asset
      end
    def calc_ap_adjustment
      0 - self.accounts_p
      end
    def calc_debt_adjustment
      0 - self.bank_debt
      end
    def calc_other_debt_adjustment
      0 - self.other_liability
    end
    def total_current_asset_adjustment
      self.calc_cash_adjustment + self.calc_ar_adjustment + self.calc_inv_adjustment
    end
    def total_fixed_asset_adjustment
      self.calc_ffev_adjustment + self.calc_estate_adjustment - self.calc_deprec_and_amort
    end
    def total_asset_adjustment
      self.total_current_asset_adjustment + self.total_fixed_asset_adjustment + self.calc_other_asset_adjustment
    end
    def total_liabilities_adjustment
      self.calc_ap_adjustment + self.calc_debt_adjustment + self.calc_other_debt_adjustment
    end
  def check_completion
    self.organization.create_appraisal
  end
  def update_status
    self.attributes.each do |obj|
      if obj.nil?
        return
      end
    end
    if self.balance_sheet_status_id != 1
    self.balance_sheet_status_id = 2
      end
  end

  def un_nil(obj)
    if obj
      obj
    else
      0
    end
  end

  def calculate_fields
    self.other_asset = un_nil(self.total_asset) -
        un_nil(self.cash_on_hand) - un_nil(self.accounts_r) - un_nil(self.inventory) - un_nil(self.real_estate_value) -
        un_nil(ffe_v) - un_nil(accum_d) - un_nil(accum_a)
    self.other_liability = un_nil(self.total_liability) - un_nil(accounts_p) - un_nil(bank_debt)
  end
end
