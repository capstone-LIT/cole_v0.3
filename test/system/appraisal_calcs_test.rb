require "application_system_test_case"

class AppraisalCalcsTest < ApplicationSystemTestCase
  setup do
    @appraisal_calc = appraisal_calcs(:one)
  end

  test "visiting the index" do
    visit appraisal_calcs_url
    assert_selector "h1", text: "Appraisal Calcs"
  end

  test "creating a Appraisal calc" do
    visit appraisal_calcs_url
    click_on "New Appraisal Calc"

    fill_in "Appraisal Calc Status", with: @appraisal_calc.appraisal_calc_status_id
    fill_in "Asset Approach Conf", with: @appraisal_calc.asset_approach_conf
    fill_in "Business Type", with: @appraisal_calc.business_type_id
    fill_in "Buyer Down Pmt", with: @appraisal_calc.buyer_down_pmt
    fill_in "Capt Expend Est", with: @appraisal_calc.capt_expend_est
    fill_in "Comm Finance", with: @appraisal_calc.comm_finance
    fill_in "Cont Earn Out", with: @appraisal_calc.cont_earn_out
    fill_in "Debt Assum", with: @appraisal_calc.debt_assum
    fill_in "Est Loan Fee", with: @appraisal_calc.est_loan_fee
    fill_in "Est Value Rev", with: @appraisal_calc.est_value_rev
    fill_in "Est Value Rev Var", with: @appraisal_calc.est_value_rev_var
    fill_in "Facility Type", with: @appraisal_calc.facility_type_id
    fill_in "Income Approach Conf", with: @appraisal_calc.income_approach_conf
    fill_in "Interim End", with: @appraisal_calc.interim_end
    fill_in "Interim Start", with: @appraisal_calc.interim_start
    fill_in "Market Approach Conf", with: @appraisal_calc.market_approach_conf
    fill_in "Mult Of Rev", with: @appraisal_calc.mult_of_rev
    fill_in "Mult Of Rev Var", with: @appraisal_calc.mult_of_rev_var
    fill_in "Mult Of Sde", with: @appraisal_calc.mult_of_sde
    fill_in "Mult Of Sde Var", with: @appraisal_calc.mult_of_sde_var
    fill_in "New Owner Sal", with: @appraisal_calc.new_owner_sal
    fill_in "Organization", with: @appraisal_calc.organization_id
    fill_in "Projected Conf", with: @appraisal_calc.projected_conf
    fill_in "Report Date", with: @appraisal_calc.report_date
    fill_in "Seller Finance", with: @appraisal_calc.seller_finance
    fill_in "Term", with: @appraisal_calc.term
    fill_in "Trans Found", with: @appraisal_calc.trans_found
    fill_in "Year One Conf", with: @appraisal_calc.year_one_conf
    fill_in "Year Three Conf", with: @appraisal_calc.year_three_conf
    fill_in "Year Two Conf", with: @appraisal_calc.year_two_conf
    click_on "Create Appraisal calc"

    assert_text "Appraisal calc was successfully created"
    click_on "Back"
  end

  test "updating a Appraisal calc" do
    visit appraisal_calcs_url
    click_on "Edit", match: :first

    fill_in "Appraisal Calc Status", with: @appraisal_calc.appraisal_calc_status_id
    fill_in "Asset Approach Conf", with: @appraisal_calc.asset_approach_conf
    fill_in "Business Type", with: @appraisal_calc.business_type_id
    fill_in "Buyer Down Pmt", with: @appraisal_calc.buyer_down_pmt
    fill_in "Capt Expend Est", with: @appraisal_calc.capt_expend_est
    fill_in "Comm Finance", with: @appraisal_calc.comm_finance
    fill_in "Cont Earn Out", with: @appraisal_calc.cont_earn_out
    fill_in "Debt Assum", with: @appraisal_calc.debt_assum
    fill_in "Est Loan Fee", with: @appraisal_calc.est_loan_fee
    fill_in "Est Value Rev", with: @appraisal_calc.est_value_rev
    fill_in "Est Value Rev Var", with: @appraisal_calc.est_value_rev_var
    fill_in "Facility Type", with: @appraisal_calc.facility_type_id
    fill_in "Income Approach Conf", with: @appraisal_calc.income_approach_conf
    fill_in "Interim End", with: @appraisal_calc.interim_end
    fill_in "Interim Start", with: @appraisal_calc.interim_start
    fill_in "Market Approach Conf", with: @appraisal_calc.market_approach_conf
    fill_in "Mult Of Rev", with: @appraisal_calc.mult_of_rev
    fill_in "Mult Of Rev Var", with: @appraisal_calc.mult_of_rev_var
    fill_in "Mult Of Sde", with: @appraisal_calc.mult_of_sde
    fill_in "Mult Of Sde Var", with: @appraisal_calc.mult_of_sde_var
    fill_in "New Owner Sal", with: @appraisal_calc.new_owner_sal
    fill_in "Organization", with: @appraisal_calc.organization_id
    fill_in "Projected Conf", with: @appraisal_calc.projected_conf
    fill_in "Report Date", with: @appraisal_calc.report_date
    fill_in "Seller Finance", with: @appraisal_calc.seller_finance
    fill_in "Term", with: @appraisal_calc.term
    fill_in "Trans Found", with: @appraisal_calc.trans_found
    fill_in "Year One Conf", with: @appraisal_calc.year_one_conf
    fill_in "Year Three Conf", with: @appraisal_calc.year_three_conf
    fill_in "Year Two Conf", with: @appraisal_calc.year_two_conf
    click_on "Update Appraisal calc"

    assert_text "Appraisal calc was successfully updated"
    click_on "Back"
  end

  test "destroying a Appraisal calc" do
    visit appraisal_calcs_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Appraisal calc was successfully destroyed"
  end
end
