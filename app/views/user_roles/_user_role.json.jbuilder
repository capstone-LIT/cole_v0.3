json.extract! user_role, :id, :user_role_desc, :created_at, :updated_at
json.url user_role_url(user_role, format: :json)
