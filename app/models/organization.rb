class Organization < ApplicationRecord
  belongs_to :state
  belongs_to :ownership_type
  belongs_to :real_estate_type
  belongs_to :hist_earning_ranking
  belongs_to :finance_rec_ranking
  belongs_to :depth_of_management_ranking
  belongs_to :loc_and_fac_ranking
  belongs_to :competition_ranking
  belongs_to :diversification_ranking
  belongs_to :equipment_sys_ranking
  belongs_to :employee_skill_turnover_ranking
  belongs_to :desirability_ranking
  belongs_to :hist_org_performance_ranking
  belongs_to :org_status
  belongs_to :user
  has_many :income_statements
  accepts_nested_attributes_for :income_statements
  has_many :appraisal_calcs
  has_many :balance_sheets
  accepts_nested_attributes_for :balance_sheets
  has_many :organization_stakeholders
  has_many :stakeholders, through: :organization_stakeholders
  accepts_nested_attributes_for :stakeholders
end

public

def check_balance_sheet_pending
  sheets = self.balance_sheets
  sheets.each do |s|
    if s.balance_sheet_status.sheet_desc == 'Pending'
      x = 'Pending'
      return x
    else
      x = 'Complete'
      return x
    end
  end
  'Incomplete'
end

def check_stmt_pending
  stmts = self.income_statements
  years = 4
  if (Date.today.year - 1 - self.founding_year) < years
    years = Date.today.year - 1 - self.founding_year
  end

  if stmts.size >= years
    stmts.each do |s|
      if s.income_stmt_status.stmt_status_desc == 'Pending'
        x = 'Pending'
        return x
      end
    end
    x = 'Complete'
    return x
  else
    'Unfinished'
  end
end

def get_balance_sheet
  sheets = self.balance_sheets
  sheets.each do |s|
    #if s.balance_sheet_status.sheet_desc == 'Complete' || s.balance_sheet_status.sheet_desc == 'Pending'
      return s
    #end
  end
  nil
end

def get_stmts
  stmts = self.income_statements
  stmts.each do |s|
    if s.income_stmt_status_id == 3 || s.income_stmt_status_id == 1
      return s
  end
  nil
  end
end

def create_appraisal
  if check_balance_sheet_pending == 'Complete' && check_stmt_pending == 'Complete'
    appraisal = AppraisalCalc.new(:organization_id => self.id, :appraisal_calc_status_id => 2,
                      :business_type_id => 1, :facility_type_id => 1, :report_date => Date.today,
                                  :year_one_conf => 0.1, :year_two_conf => 0.3, :year_three_conf => 0.4,
                                  :projected_conf => 0.2, :mult_of_rev => 0.52, :mult_of_sde => 2.76,
                                  :mult_of_rev_var => 0.43, :mult_of_sde_var => 0.2, :est_val_rev_conf => 0,
                                  :est_value_sde_conf => 1, :asset_approach_conf => 0, :income_approach_conf => 0.5,
                                  :market_approach_conf => 0.5, :buyer_down_pmt => 0.2, :term => 10,
                                  :capt_expend_est => 2500, :new_owner_sal => 75000, :debt_assum => 0,
                                  :seller_finance => 0.1, :comm_finance => 0.7, :cont_earn_out => 0,
                                  :est_loan_fee => 0.025)
    appraisal.save
  end
end
