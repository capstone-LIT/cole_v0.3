require "application_system_test_case"

class DepthOfManagementRankingsTest < ApplicationSystemTestCase
  setup do
    @depth_of_management_ranking = depth_of_management_rankings(:one)
  end

  test "visiting the index" do
    visit depth_of_management_rankings_url
    assert_selector "h1", text: "Depth Of Management Rankings"
  end

  test "creating a Depth of management ranking" do
    visit depth_of_management_rankings_url
    click_on "New Depth Of Management Ranking"

    fill_in "Depth Of Mgmt Desc", with: @depth_of_management_ranking.depth_of_mgmt_desc
    click_on "Create Depth of management ranking"

    assert_text "Depth of management ranking was successfully created"
    click_on "Back"
  end

  test "updating a Depth of management ranking" do
    visit depth_of_management_rankings_url
    click_on "Edit", match: :first

    fill_in "Depth Of Mgmt Desc", with: @depth_of_management_ranking.depth_of_mgmt_desc
    click_on "Update Depth of management ranking"

    assert_text "Depth of management ranking was successfully updated"
    click_on "Back"
  end

  test "destroying a Depth of management ranking" do
    visit depth_of_management_rankings_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Depth of management ranking was successfully destroyed"
  end
end
