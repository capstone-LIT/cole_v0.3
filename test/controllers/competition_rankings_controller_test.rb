require 'test_helper'

class CompetitionRankingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @competition_ranking = competition_rankings(:one)
  end

  test "should get index" do
    get competition_rankings_url
    assert_response :success
  end

  test "should get new" do
    get new_competition_ranking_url
    assert_response :success
  end

  test "should create competition_ranking" do
    assert_difference('CompetitionRanking.count') do
      post competition_rankings_url, params: { competition_ranking: { competition_desc: @competition_ranking.competition_desc } }
    end

    assert_redirected_to competition_ranking_url(CompetitionRanking.last)
  end

  test "should show competition_ranking" do
    get competition_ranking_url(@competition_ranking)
    assert_response :success
  end

  test "should get edit" do
    get edit_competition_ranking_url(@competition_ranking)
    assert_response :success
  end

  test "should update competition_ranking" do
    patch competition_ranking_url(@competition_ranking), params: { competition_ranking: { competition_desc: @competition_ranking.competition_desc } }
    assert_redirected_to competition_ranking_url(@competition_ranking)
  end

  test "should destroy competition_ranking" do
    assert_difference('CompetitionRanking.count', -1) do
      delete competition_ranking_url(@competition_ranking)
    end

    assert_redirected_to competition_rankings_url
  end
end
