class BalanceSheetStatusesController < ApplicationController
  before_action :set_balance_sheet_status, only: [:show, :edit, :update, :destroy]

  # GET /balance_sheet_statuses
  # GET /balance_sheet_statuses.json
  def index
    @balance_sheet_statuses = BalanceSheetStatus.all
  end

  # GET /balance_sheet_statuses/1
  # GET /balance_sheet_statuses/1.json
  def show
  end

  # GET /balance_sheet_statuses/new
  def new
    @balance_sheet_status = BalanceSheetStatus.new
  end

  # GET /balance_sheet_statuses/1/edit
  def edit
  end

  # POST /balance_sheet_statuses
  # POST /balance_sheet_statuses.json
  def create
    @balance_sheet_status = BalanceSheetStatus.new(balance_sheet_status_params)

    respond_to do |format|
      if @balance_sheet_status.save
        format.html { redirect_to @balance_sheet_status, notice: 'Balance sheet status was successfully created.' }
        format.json { render :show, status: :created, location: @balance_sheet_status }
      else
        format.html { render :new }
        format.json { render json: @balance_sheet_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /balance_sheet_statuses/1
  # PATCH/PUT /balance_sheet_statuses/1.json
  def update
    respond_to do |format|
      if @balance_sheet_status.update(balance_sheet_status_params)
        format.html { redirect_to @balance_sheet_status, notice: 'Balance sheet status was successfully updated.' }
        format.json { render :show, status: :ok, location: @balance_sheet_status }
      else
        format.html { render :edit }
        format.json { render json: @balance_sheet_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /balance_sheet_statuses/1
  # DELETE /balance_sheet_statuses/1.json
  def destroy
    @balance_sheet_status.destroy
    respond_to do |format|
      format.html { redirect_to balance_sheet_statuses_url, notice: 'Balance sheet status was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_balance_sheet_status
      @balance_sheet_status = BalanceSheetStatus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def balance_sheet_status_params
      params.require(:balance_sheet_status).permit(:sheet_desc)
    end
end
