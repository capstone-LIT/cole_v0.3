class CreateFinanceRecRankings < ActiveRecord::Migration[5.2]
  def change
    create_table :finance_rec_rankings do |t|
      t.string :finance_rec_desc

      t.timestamps
    end
  end
end
