class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :trackable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :timeoutable, :validatable
  has_many :organizations
  after_create :set_account_type

  def is_admin?
    i = self.user_role_id
    role = UserRole.find_by_id(i)
    # returns true if user role is admin
    role.user_role_desc == "Admin"
  end

  def is_broker?
    i = self.user_role_id
    role = UserRole.find_by_id(i)
    # returns true if user role is either admin or broker
    role.user_role_desc == "Broker" || role.user_role_desc == "Admin"
  end

  def get_org
    i = self.id
    # find all organizations with the user's id
    orgs  = Organization.where(user_id: i)

    # cycle through all selected organizations. If an organization is still active, return.
    orgs.each do |org|
      if org.org_status_id == 1
        return org
      end
    end
    # if no organization is active, or no organizations are found, return nil.
    nil
  end

  def set_account_type
    self.update_column( 'user_role_id', 3)
  end
end
