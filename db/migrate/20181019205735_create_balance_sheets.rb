class CreateBalanceSheets < ActiveRecord::Migration[5.2]
  def change
    create_table :balance_sheets do |t|
      t.references :organization, foreign_key: true
      t.references :financial_source_type, foreign_key: true
      t.date :sheet_date
      t.references :accounting_method, foreign_key: true
      t.float :total_asset
      t.float :cash_on_hand
      t.float :accounts_r
      t.float :inventory
      t.float :real_estate_value
      t.float :ffe_v
      t.float :accum_d
      t.float :accum_a
      t.float :other_asset
      t.float :total_liability
      t.float :accounts_p
      t.float :bank_debt
      t.float :other_liability
      t.float :cash_req
      t.float :inv_req
      t.float :curr_ffe_v
      t.float :real_estate_est
      t.text :sheet_comment
      t.references :balance_sheet_status, foreign_key: true

      t.timestamps
    end
  end
end
