require "application_system_test_case"

class RealEstateTypesTest < ApplicationSystemTestCase
  setup do
    @real_estate_type = real_estate_types(:one)
  end

  test "visiting the index" do
    visit real_estate_types_url
    assert_selector "h1", text: "Real Estate Types"
  end

  test "creating a Real estate type" do
    visit real_estate_types_url
    click_on "New Real Estate Type"

    fill_in "Real Estate Type Desc", with: @real_estate_type.real_estate_type_desc
    click_on "Create Real estate type"

    assert_text "Real estate type was successfully created"
    click_on "Back"
  end

  test "updating a Real estate type" do
    visit real_estate_types_url
    click_on "Edit", match: :first

    fill_in "Real Estate Type Desc", with: @real_estate_type.real_estate_type_desc
    click_on "Update Real estate type"

    assert_text "Real estate type was successfully updated"
    click_on "Back"
  end

  test "destroying a Real estate type" do
    visit real_estate_types_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Real estate type was successfully destroyed"
  end
end
