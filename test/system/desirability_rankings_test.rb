require "application_system_test_case"

class DesirabilityRankingsTest < ApplicationSystemTestCase
  setup do
    @desirability_ranking = desirability_rankings(:one)
  end

  test "visiting the index" do
    visit desirability_rankings_url
    assert_selector "h1", text: "Desirability Rankings"
  end

  test "creating a Desirability ranking" do
    visit desirability_rankings_url
    click_on "New Desirability Ranking"

    fill_in "Desirability Desc", with: @desirability_ranking.desirability_desc
    click_on "Create Desirability ranking"

    assert_text "Desirability ranking was successfully created"
    click_on "Back"
  end

  test "updating a Desirability ranking" do
    visit desirability_rankings_url
    click_on "Edit", match: :first

    fill_in "Desirability Desc", with: @desirability_ranking.desirability_desc
    click_on "Update Desirability ranking"

    assert_text "Desirability ranking was successfully updated"
    click_on "Back"
  end

  test "destroying a Desirability ranking" do
    visit desirability_rankings_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Desirability ranking was successfully destroyed"
  end
end
