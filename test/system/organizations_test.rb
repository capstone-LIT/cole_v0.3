require "application_system_test_case"

class OrganizationsTest < ApplicationSystemTestCase
  setup do
    @organization = organizations(:one)
  end

  test "visiting the index" do
    visit organizations_url
    assert_selector "h1", text: "Organizations"
  end

  test "creating a Organization" do
    visit organizations_url
    click_on "New Organization"

    fill_in "Brief Desc Of Services", with: @organization.brief_desc_of_services
    fill_in "Competition Ranking", with: @organization.competition_ranking_id
    fill_in "Depth Of Management Ranking", with: @organization.depth_of_management_ranking_id
    fill_in "Desirability Ranking", with: @organization.desirability_ranking_id
    fill_in "Diversification Ranking", with: @organization.diversification_ranking_id
    fill_in "Employee Skill Turnover Ranking", with: @organization.employee_skill_turnover_ranking_id
    fill_in "Equipment Sys Ranking", with: @organization.equipment_sys_ranking_id
    fill_in "Extend Date", with: @organization.extend_date
    fill_in "Finance Rec Ranking", with: @organization.finance_rec_ranking_id
    fill_in "Founding Year", with: @organization.founding_year
    fill_in "Full Emp", with: @organization.full_emp
    fill_in "Futr Rent", with: @organization.futr_rent
    fill_in "Hist Earning Ranking", with: @organization.hist_earning_ranking_id
    fill_in "Hist Org Performance Ranking", with: @organization.hist_org_performance_ranking_id
    fill_in "Lease Expir Date", with: @organization.lease_expir_date
    fill_in "Loc And Fac Ranking", with: @organization.loc_and_fac_ranking_id
    fill_in "Org Address", with: @organization.org_address
    fill_in "Org City", with: @organization.org_city
    fill_in "Org Comment", with: @organization.org_comment
    fill_in "Org Name", with: @organization.org_name
    fill_in "Org Website", with: @organization.org_website
    fill_in "Org Zip", with: @organization.org_zip
    fill_in "Ownership Type", with: @organization.ownership_type_id
    fill_in "Part Emp", with: @organization.part_emp
    fill_in "Prim Naics", with: @organization.prim_naics
    fill_in "Real Estate Included", with: @organization.real_estate_included
    fill_in "Real Estate Type", with: @organization.real_estate_type_id
    fill_in "Sec Naics", with: @organization.sec_naics
    fill_in "State", with: @organization.state_id
    click_on "Create Organization"

    assert_text "Organization was successfully created"
    click_on "Back"
  end

  test "updating a Organization" do
    visit organizations_url
    click_on "Edit", match: :first

    fill_in "Brief Desc Of Services", with: @organization.brief_desc_of_services
    fill_in "Competition Ranking", with: @organization.competition_ranking_id
    fill_in "Depth Of Management Ranking", with: @organization.depth_of_management_ranking_id
    fill_in "Desirability Ranking", with: @organization.desirability_ranking_id
    fill_in "Diversification Ranking", with: @organization.diversification_ranking_id
    fill_in "Employee Skill Turnover Ranking", with: @organization.employee_skill_turnover_ranking_id
    fill_in "Equipment Sys Ranking", with: @organization.equipment_sys_ranking_id
    fill_in "Extend Date", with: @organization.extend_date
    fill_in "Finance Rec Ranking", with: @organization.finance_rec_ranking_id
    fill_in "Founding Year", with: @organization.founding_year
    fill_in "Full Emp", with: @organization.full_emp
    fill_in "Futr Rent", with: @organization.futr_rent
    fill_in "Hist Earning Ranking", with: @organization.hist_earning_ranking_id
    fill_in "Hist Org Performance Ranking", with: @organization.hist_org_performance_ranking_id
    fill_in "Lease Expir Date", with: @organization.lease_expir_date
    fill_in "Loc And Fac Ranking", with: @organization.loc_and_fac_ranking_id
    fill_in "Org Address", with: @organization.org_address
    fill_in "Org City", with: @organization.org_city
    fill_in "Org Comment", with: @organization.org_comment
    fill_in "Org Name", with: @organization.org_name
    fill_in "Org Website", with: @organization.org_website
    fill_in "Org Zip", with: @organization.org_zip
    fill_in "Ownership Type", with: @organization.ownership_type_id
    fill_in "Part Emp", with: @organization.part_emp
    fill_in "Prim Naics", with: @organization.prim_naics
    fill_in "Real Estate Included", with: @organization.real_estate_included
    fill_in "Real Estate Type", with: @organization.real_estate_type_id
    fill_in "Sec Naics", with: @organization.sec_naics
    fill_in "State", with: @organization.state_id
    click_on "Update Organization"

    assert_text "Organization was successfully updated"
    click_on "Back"
  end

  test "destroying a Organization" do
    visit organizations_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Organization was successfully destroyed"
  end
end
