class IncomeStatement < ApplicationRecord
  belongs_to :income_stmt_status
  belongs_to :organization
  before_create :set_status
  before_save :update_status
  after_save :check_completion

  public
    def percent_of_revenue
      self.pretax_profit / self.total_sales
    end
    def calc_owners_perks
      self.owners_travel_exp + self.own_insurance_exp + self.own_veh_exp
    end
    def calc_rent_adjustment
      self.rent - self.organization.futr_rent
    end
    def calc_discretionary_earnings
      self.pretax_profit + self.dep_an_am + self.interest_exp + self.partner_comp +
          self.sal_adj + self.own_retirement + self.calc_owners_perks + self.calc_rent_adjustment + self.non_rec_exp
    end

    def set_status
      self.income_stmt_status_id = 2
    end
  def check_completion
    self.organization.create_appraisal
  end

  def update_status
    self.attributes.each do |obj|
      if obj.nil?
        return
      end
    end
    if self.income_stmt_status_id != 1
      self.income_stmt_status_id = 2
    end

  end

end