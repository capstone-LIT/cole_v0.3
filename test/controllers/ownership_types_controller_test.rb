require 'test_helper'

class OwnershipTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ownership_type = ownership_types(:one)
  end

  test "should get index" do
    get ownership_types_url
    assert_response :success
  end

  test "should get new" do
    get new_ownership_type_url
    assert_response :success
  end

  test "should create ownership_type" do
    assert_difference('OwnershipType.count') do
      post ownership_types_url, params: { ownership_type: { ownership_type_desc: @ownership_type.ownership_type_desc } }
    end

    assert_redirected_to ownership_type_url(OwnershipType.last)
  end

  test "should show ownership_type" do
    get ownership_type_url(@ownership_type)
    assert_response :success
  end

  test "should get edit" do
    get edit_ownership_type_url(@ownership_type)
    assert_response :success
  end

  test "should update ownership_type" do
    patch ownership_type_url(@ownership_type), params: { ownership_type: { ownership_type_desc: @ownership_type.ownership_type_desc } }
    assert_redirected_to ownership_type_url(@ownership_type)
  end

  test "should destroy ownership_type" do
    assert_difference('OwnershipType.count', -1) do
      delete ownership_type_url(@ownership_type)
    end

    assert_redirected_to ownership_types_url
  end
end
