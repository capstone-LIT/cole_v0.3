json.extract! appraisal_calc_status, :id, :calc_status_desc, :created_at, :updated_at
json.url appraisal_calc_status_url(appraisal_calc_status, format: :json)
