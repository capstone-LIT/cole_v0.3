require 'test_helper'

class BalanceSheetStatusesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @balance_sheet_status = balance_sheet_statuses(:one)
  end

  test "should get index" do
    get balance_sheet_statuses_url
    assert_response :success
  end

  test "should get new" do
    get new_balance_sheet_status_url
    assert_response :success
  end

  test "should create balance_sheet_status" do
    assert_difference('BalanceSheetStatus.count') do
      post balance_sheet_statuses_url, params: { balance_sheet_status: { sheet_desc: @balance_sheet_status.sheet_desc } }
    end

    assert_redirected_to balance_sheet_status_url(BalanceSheetStatus.last)
  end

  test "should show balance_sheet_status" do
    get balance_sheet_status_url(@balance_sheet_status)
    assert_response :success
  end

  test "should get edit" do
    get edit_balance_sheet_status_url(@balance_sheet_status)
    assert_response :success
  end

  test "should update balance_sheet_status" do
    patch balance_sheet_status_url(@balance_sheet_status), params: { balance_sheet_status: { sheet_desc: @balance_sheet_status.sheet_desc } }
    assert_redirected_to balance_sheet_status_url(@balance_sheet_status)
  end

  test "should destroy balance_sheet_status" do
    assert_difference('BalanceSheetStatus.count', -1) do
      delete balance_sheet_status_url(@balance_sheet_status)
    end

    assert_redirected_to balance_sheet_statuses_url
  end
end
