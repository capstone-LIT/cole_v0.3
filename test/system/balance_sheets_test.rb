require "application_system_test_case"

class BalanceSheetsTest < ApplicationSystemTestCase
  setup do
    @balance_sheet = balance_sheets(:one)
  end

  test "visiting the index" do
    visit balance_sheets_url
    assert_selector "h1", text: "Balance Sheets"
  end

  test "creating a Balance sheet" do
    visit balance_sheets_url
    click_on "New Balance Sheet"

    fill_in "Accounting Method", with: @balance_sheet.accounting_method_id
    fill_in "Accounts P", with: @balance_sheet.accounts_p
    fill_in "Accounts R", with: @balance_sheet.accounts_r
    fill_in "Accum A", with: @balance_sheet.accum_a
    fill_in "Accum D", with: @balance_sheet.accum_d
    fill_in "Balance Sheet Status", with: @balance_sheet.balance_sheet_status_id
    fill_in "Bank Debt", with: @balance_sheet.bank_debt
    fill_in "Cash On Hand", with: @balance_sheet.cash_on_hand
    fill_in "Cash Req", with: @balance_sheet.cash_req
    fill_in "Curr Ffe V", with: @balance_sheet.curr_ffe_v
    fill_in "Ffe V", with: @balance_sheet.ffe_v
    fill_in "Financial Source Type", with: @balance_sheet.financial_source_type_id
    fill_in "In Req", with: @balance_sheet.in_req
    fill_in "Inventory", with: @balance_sheet.inventory
    fill_in "Organization", with: @balance_sheet.organization_id
    fill_in "Other Asset", with: @balance_sheet.other_asset
    fill_in "Other Liability", with: @balance_sheet.other_liability
    fill_in "Real Estate Est", with: @balance_sheet.real_estate_est
    fill_in "Real Estate Value", with: @balance_sheet.real_estate_value
    fill_in "Sheet Comment", with: @balance_sheet.sheet_comment
    fill_in "Sheet Date", with: @balance_sheet.sheet_date
    fill_in "Total Asset", with: @balance_sheet.total_asset
    fill_in "Total Liability", with: @balance_sheet.total_liability
    click_on "Create Balance sheet"

    assert_text "Balance sheet was successfully created"
    click_on "Back"
  end

  test "updating a Balance sheet" do
    visit balance_sheets_url
    click_on "Edit", match: :first

    fill_in "Accounting Method", with: @balance_sheet.accounting_method_id
    fill_in "Accounts P", with: @balance_sheet.accounts_p
    fill_in "Accounts R", with: @balance_sheet.accounts_r
    fill_in "Accum A", with: @balance_sheet.accum_a
    fill_in "Accum D", with: @balance_sheet.accum_d
    fill_in "Balance Sheet Status", with: @balance_sheet.balance_sheet_status_id
    fill_in "Bank Debt", with: @balance_sheet.bank_debt
    fill_in "Cash On Hand", with: @balance_sheet.cash_on_hand
    fill_in "Cash Req", with: @balance_sheet.cash_req
    fill_in "Curr Ffe V", with: @balance_sheet.curr_ffe_v
    fill_in "Ffe V", with: @balance_sheet.ffe_v
    fill_in "Financial Source Type", with: @balance_sheet.financial_source_type_id
    fill_in "In Req", with: @balance_sheet.in_req
    fill_in "Inventory", with: @balance_sheet.inventory
    fill_in "Organization", with: @balance_sheet.organization_id
    fill_in "Other Asset", with: @balance_sheet.other_asset
    fill_in "Other Liability", with: @balance_sheet.other_liability
    fill_in "Real Estate Est", with: @balance_sheet.real_estate_est
    fill_in "Real Estate Value", with: @balance_sheet.real_estate_value
    fill_in "Sheet Comment", with: @balance_sheet.sheet_comment
    fill_in "Sheet Date", with: @balance_sheet.sheet_date
    fill_in "Total Asset", with: @balance_sheet.total_asset
    fill_in "Total Liability", with: @balance_sheet.total_liability
    click_on "Update Balance sheet"

    assert_text "Balance sheet was successfully updated"
    click_on "Back"
  end

  test "destroying a Balance sheet" do
    visit balance_sheets_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Balance sheet was successfully destroyed"
  end
end
