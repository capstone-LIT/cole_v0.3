class CreateCompetitionRankings < ActiveRecord::Migration[5.2]
  def change
    create_table :competition_rankings do |t|
      t.string :competition_desc

      t.timestamps
    end
  end
end
