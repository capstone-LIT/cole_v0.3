class OrgStatusesController < ApplicationController
  before_action :set_org_status, only: [:show, :edit, :update, :destroy]

  # GET /org_statuses
  # GET /org_statuses.json
  def index
    @org_statuses = OrgStatus.all
  end

  # GET /org_statuses/1
  # GET /org_statuses/1.json
  def show
  end

  # GET /org_statuses/new
  def new
    @org_status = OrgStatus.new
  end

  # GET /org_statuses/1/edit
  def edit
  end

  # POST /org_statuses
  # POST /org_statuses.json
  def create
    @org_status = OrgStatus.new(org_status_params)

    respond_to do |format|
      if @org_status.save
        format.html { redirect_to @org_status, notice: 'Org status was successfully created.' }
        format.json { render :show, status: :created, location: @org_status }
      else
        format.html { render :new }
        format.json { render json: @org_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /org_statuses/1
  # PATCH/PUT /org_statuses/1.json
  def update
    respond_to do |format|
      if @org_status.update(org_status_params)
        format.html { redirect_to @org_status, notice: 'Org status was successfully updated.' }
        format.json { render :show, status: :ok, location: @org_status }
      else
        format.html { render :edit }
        format.json { render json: @org_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /org_statuses/1
  # DELETE /org_statuses/1.json
  def destroy
    @org_status.destroy
    respond_to do |format|
      format.html { redirect_to org_statuses_url, notice: 'Org status was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_org_status
      @org_status = OrgStatus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def org_status_params
      params.require(:org_status).permit(:status_desc)
    end
end
