json.extract! finance_rec_ranking, :id, :finance_rec_desc, :created_at, :updated_at
json.url finance_rec_ranking_url(finance_rec_ranking, format: :json)
