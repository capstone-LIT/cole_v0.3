require 'test_helper'

class AppraisalCalcStatusesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @appraisal_calc_status = appraisal_calc_statuses(:one)
  end

  test "should get index" do
    get appraisal_calc_statuses_url
    assert_response :success
  end

  test "should get new" do
    get new_appraisal_calc_status_url
    assert_response :success
  end

  test "should create appraisal_calc_status" do
    assert_difference('AppraisalCalcStatus.count') do
      post appraisal_calc_statuses_url, params: { appraisal_calc_status: { calc_status_desc: @appraisal_calc_status.calc_status_desc } }
    end

    assert_redirected_to appraisal_calc_status_url(AppraisalCalcStatus.last)
  end

  test "should show appraisal_calc_status" do
    get appraisal_calc_status_url(@appraisal_calc_status)
    assert_response :success
  end

  test "should get edit" do
    get edit_appraisal_calc_status_url(@appraisal_calc_status)
    assert_response :success
  end

  test "should update appraisal_calc_status" do
    patch appraisal_calc_status_url(@appraisal_calc_status), params: { appraisal_calc_status: { calc_status_desc: @appraisal_calc_status.calc_status_desc } }
    assert_redirected_to appraisal_calc_status_url(@appraisal_calc_status)
  end

  test "should destroy appraisal_calc_status" do
    assert_difference('AppraisalCalcStatus.count', -1) do
      delete appraisal_calc_status_url(@appraisal_calc_status)
    end

    assert_redirected_to appraisal_calc_statuses_url
  end
end
