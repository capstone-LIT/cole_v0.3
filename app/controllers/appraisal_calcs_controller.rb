class AppraisalCalcsController < ApplicationController
  before_action :set_appraisal_calc, only: [:show, :edit, :update, :destroy, :refresh]
  before_action :authorize_appraisal_calc
  respond_to :docx

  # GET /appraisal_calcs
  # GET /appraisal_calcs.json
  def index
    if current_user.is_broker?
      @appraisal_calcs = AppraisalCalc.all
    else
      redirect_to '/client_home'
      end
  end

  def appraisal_report
    @appraisal_calc = AppraisalCalc.find(params[:id])
    respond_to do |format|
      format.html
      format.json
      format.pdf do
        render pdf: "appraisal_report",
               template: "/appraisal_calcs/appraisal_report.pdf.erb",
               locals: {:Appraisal_calc => @appraisal_calc}
      end
    end
  end

  def download2
    @appraisal_calc = AppraisalCalc.find(params[:appraisal_calc])
    respond_with(@appraisal_calc, filename: 'appraisal.docx', word_template: 'download2.docx')
  end

  def pending_appraisal_report
    @appraisal_calcs = AppraisalCalc.all
  end

  def completed_appraisal_report
    @appraisal_calcs = AppraisalCalc.all
  end

  # GET /appraisal_calcs/1
  # GET /appraisal_calcs/1.json
  def show
    @appraisal_calc = AppraisalCalc.find(params[:id])
    respond_to do |format|
      format.html
      format.json
      format.pdf do
        render pdf: "appraisal_report",
               template: "/appraisal_calcs/show.pdf.erb",
               locals: {:Appraisal_calc => @appraisal_calc}
      end
    end
  end

  # GET /appraisal_calcs/new
  def new
    @appraisal_calc = AppraisalCalc.new
  end

  # GET /appraisal_calcs/1/edit
  def edit
    @stmt = IncomeStatement.all
  end

  # POST /appraisal_calcs
  # POST /appraisal_calcs.json
  def create
    @appraisal_calc = AppraisalCalc.new(appraisal_calc_params)
    respond_to do |format|
      if @appraisal_calc.save
        format.html { redirect_to @appraisal_calc, notice: 'Appraisal calc was successfully created.' }
        format.json { render :show, status: :created, location: @appraisal_calc }
      else
        format.html { render :new }
        format.json { render json: @appraisal_calc.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /appraisal_calcs/1
  # PATCH/PUT /appraisal_calcs/1.json
  def update
    respond_to do |format|
      if @appraisal_calc.update(appraisal_calc_params)
        format.html { render :edit, notice: 'Appraisal calc was successfully updated.' }
        format.json { render :edit, status: :ok, location: @appraisal_calc }
      else
        format.html { render :edit }
        format.json { render json: @appraisal_calc.errors, status: :unprocessable_entity }
      end
    end
  end

  def refresh
    respond_to do |format|
      if @appraisal_calc.update(appraisal_calc_params)
        format.html { redirect_to @appraisal_calc, notice: 'Appraisal calc was successfully updated.' }
        format.json { render :edit, status: :ok, location: @appraisal_calc }
      else
        format.html { render :edit }
        format.json { render json: @appraisal_calc.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /appraisal_calcs/1
  # DELETE /appraisal_calcs/1.json
  def destroy
    @appraisal_calc.appraisal_calc_status_id = 3
    respond_to do |format|
      format.html { redirect_to appraisal_calcs_url, notice: 'Appraisal calc was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_appraisal_calc
      @appraisal_calc = AppraisalCalc.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def appraisal_calc_params
      params.require(:appraisal_calc).permit(:organization_id, :year_one_conf, :year_two_conf, :year_three_conf,
                                             :projected_conf, :trans_found, :mult_of_rev, :mult_of_rev_var,
                                             :mult_of_sde, :mult_of_sde_var, :est_value_rev, :est_value_sde_conf,
                                             :est_val_rev_conf, :asset_approach_conf, :income_approach_conf,
                                             :market_approach_conf, :buyer_down_pmt, :debt_assum, :seller_finance,
                                             :comm_finance, :cont_earn_out, :report_date, :interim_start,
                                             :interim_end, :business_type_id, :facility_type_id, :est_loan_fee,
                                             :term, :capt_expend_est, :new_owner_sal, :appraisal_calc_status_id)
    end

  def authorize_appraisal_calc
    if !current_user.is_broker?
      redirect_to '/client_home'
    end
  end
end
