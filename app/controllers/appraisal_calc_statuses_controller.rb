class AppraisalCalcStatusesController < ApplicationController
  before_action :set_appraisal_calc_status, only: [:show, :edit, :update, :destroy]

  # GET /appraisal_calc_statuses
  # GET /appraisal_calc_statuses.json
  def index
    @appraisal_calc_statuses = AppraisalCalcStatus.all
  end

  # GET /appraisal_calc_statuses/1
  # GET /appraisal_calc_statuses/1.json
  def show
  end

  # GET /appraisal_calc_statuses/new
  def new
    @appraisal_calc_status = AppraisalCalcStatus.new
  end

  # GET /appraisal_calc_statuses/1/edit
  def edit
  end

  # POST /appraisal_calc_statuses
  # POST /appraisal_calc_statuses.json
  def create
    @appraisal_calc_status = AppraisalCalcStatus.new(appraisal_calc_status_params)

    respond_to do |format|
      if @appraisal_calc_status.save
        format.html { redirect_to @appraisal_calc_status, notice: 'Appraisal calc status was successfully created.' }
        format.json { render :show, status: :created, location: @appraisal_calc_status }
      else
        format.html { render :new }
        format.json { render json: @appraisal_calc_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /appraisal_calc_statuses/1
  # PATCH/PUT /appraisal_calc_statuses/1.json
  def update
    respond_to do |format|
      if @appraisal_calc_status.update(appraisal_calc_status_params)
        format.html { redirect_to @appraisal_calc_status, notice: 'Appraisal calc status was successfully updated.' }
        format.json { render :show, status: :ok, location: @appraisal_calc_status }
      else
        format.html { render :edit }
        format.json { render json: @appraisal_calc_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /appraisal_calc_statuses/1
  # DELETE /appraisal_calc_statuses/1.json
  def destroy
    @appraisal_calc_status.destroy
    respond_to do |format|
      format.html { redirect_to appraisal_calc_statuses_url, notice: 'Appraisal calc status was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_appraisal_calc_status
      @appraisal_calc_status = AppraisalCalcStatus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def appraisal_calc_status_params
      params.require(:appraisal_calc_status).permit(:calc_status_desc)
    end
end
