require "application_system_test_case"

class EquipmentSysRankingsTest < ApplicationSystemTestCase
  setup do
    @equipment_sys_ranking = equipment_sys_rankings(:one)
  end

  test "visiting the index" do
    visit equipment_sys_rankings_url
    assert_selector "h1", text: "Equipment Sys Rankings"
  end

  test "creating a Equipment sys ranking" do
    visit equipment_sys_rankings_url
    click_on "New Equipment Sys Ranking"

    fill_in "Eqp Sys Desc", with: @equipment_sys_ranking.eqp_sys_desc
    click_on "Create Equipment sys ranking"

    assert_text "Equipment sys ranking was successfully created"
    click_on "Back"
  end

  test "updating a Equipment sys ranking" do
    visit equipment_sys_rankings_url
    click_on "Edit", match: :first

    fill_in "Eqp Sys Desc", with: @equipment_sys_ranking.eqp_sys_desc
    click_on "Update Equipment sys ranking"

    assert_text "Equipment sys ranking was successfully updated"
    click_on "Back"
  end

  test "destroying a Equipment sys ranking" do
    visit equipment_sys_rankings_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Equipment sys ranking was successfully destroyed"
  end
end
