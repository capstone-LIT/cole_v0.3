class FinanceRecRankingsController < ApplicationController
  before_action :set_finance_rec_ranking, only: [:show, :edit, :update, :destroy]

  # GET /finance_rec_rankings
  # GET /finance_rec_rankings.json
  def index
    @finance_rec_rankings = FinanceRecRanking.all
  end

  # GET /finance_rec_rankings/1
  # GET /finance_rec_rankings/1.json
  def show
  end

  # GET /finance_rec_rankings/new
  def new
    @finance_rec_ranking = FinanceRecRanking.new
  end

  # GET /finance_rec_rankings/1/edit
  def edit
  end

  # POST /finance_rec_rankings
  # POST /finance_rec_rankings.json
  def create
    @finance_rec_ranking = FinanceRecRanking.new(finance_rec_ranking_params)

    respond_to do |format|
      if @finance_rec_ranking.save
        format.html { redirect_to @finance_rec_ranking, notice: 'Finance rec ranking was successfully created.' }
        format.json { render :show, status: :created, location: @finance_rec_ranking }
      else
        format.html { render :new }
        format.json { render json: @finance_rec_ranking.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /finance_rec_rankings/1
  # PATCH/PUT /finance_rec_rankings/1.json
  def update
    respond_to do |format|
      if @finance_rec_ranking.update(finance_rec_ranking_params)
        format.html { redirect_to @finance_rec_ranking, notice: 'Finance rec ranking was successfully updated.' }
        format.json { render :show, status: :ok, location: @finance_rec_ranking }
      else
        format.html { render :edit }
        format.json { render json: @finance_rec_ranking.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /finance_rec_rankings/1
  # DELETE /finance_rec_rankings/1.json
  def destroy
    @finance_rec_ranking.destroy
    respond_to do |format|
      format.html { redirect_to finance_rec_rankings_url, notice: 'Finance rec ranking was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_finance_rec_ranking
      @finance_rec_ranking = FinanceRecRanking.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def finance_rec_ranking_params
      params.require(:finance_rec_ranking).permit(:finance_rec_desc)
    end
end
