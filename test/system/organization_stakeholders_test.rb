require "application_system_test_case"

class OrganizationStakeholdersTest < ApplicationSystemTestCase
  setup do
    @organization_stakeholder = organization_stakeholders(:one)
  end

  test "visiting the index" do
    visit organization_stakeholders_url
    assert_selector "h1", text: "Organization Stakeholders"
  end

  test "creating a Organization stakeholder" do
    visit organization_stakeholders_url
    click_on "New Organization Stakeholder"

    fill_in "Organization", with: @organization_stakeholder.organization_id
    fill_in "Percent Owned", with: @organization_stakeholder.percent_owned
    fill_in "Stakeholder", with: @organization_stakeholder.stakeholder_id
    fill_in "Stakeholder Rank", with: @organization_stakeholder.stakeholder_rank_id
    click_on "Create Organization stakeholder"

    assert_text "Organization stakeholder was successfully created"
    click_on "Back"
  end

  test "updating a Organization stakeholder" do
    visit organization_stakeholders_url
    click_on "Edit", match: :first

    fill_in "Organization", with: @organization_stakeholder.organization_id
    fill_in "Percent Owned", with: @organization_stakeholder.percent_owned
    fill_in "Stakeholder", with: @organization_stakeholder.stakeholder_id
    fill_in "Stakeholder Rank", with: @organization_stakeholder.stakeholder_rank_id
    click_on "Update Organization stakeholder"

    assert_text "Organization stakeholder was successfully updated"
    click_on "Back"
  end

  test "destroying a Organization stakeholder" do
    visit organization_stakeholders_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Organization stakeholder was successfully destroyed"
  end
end
