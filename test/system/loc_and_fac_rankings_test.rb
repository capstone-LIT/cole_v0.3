require "application_system_test_case"

class LocAndFacRankingsTest < ApplicationSystemTestCase
  setup do
    @loc_and_fac_ranking = loc_and_fac_rankings(:one)
  end

  test "visiting the index" do
    visit loc_and_fac_rankings_url
    assert_selector "h1", text: "Loc And Fac Rankings"
  end

  test "creating a Loc and fac ranking" do
    visit loc_and_fac_rankings_url
    click_on "New Loc And Fac Ranking"

    fill_in "Loc And Fac Desc", with: @loc_and_fac_ranking.loc_and_fac_desc
    click_on "Create Loc and fac ranking"

    assert_text "Loc and fac ranking was successfully created"
    click_on "Back"
  end

  test "updating a Loc and fac ranking" do
    visit loc_and_fac_rankings_url
    click_on "Edit", match: :first

    fill_in "Loc And Fac Desc", with: @loc_and_fac_ranking.loc_and_fac_desc
    click_on "Update Loc and fac ranking"

    assert_text "Loc and fac ranking was successfully updated"
    click_on "Back"
  end

  test "destroying a Loc and fac ranking" do
    visit loc_and_fac_rankings_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Loc and fac ranking was successfully destroyed"
  end
end
