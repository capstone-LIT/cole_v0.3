class BalanceSheetStatus < ApplicationRecord
  has_many :balance_sheets
end
