class StakeholderRank < ApplicationRecord
  has_many :organization_stakeholders
  has_many :stakeholders, through: :organization_stakeholders
end
