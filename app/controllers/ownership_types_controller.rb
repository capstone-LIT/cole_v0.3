class OwnershipTypesController < ApplicationController
  before_action :set_ownership_type, only: [:show, :edit, :update, :destroy]

  # GET /ownership_types
  # GET /ownership_types.json
  def index
    @ownership_types = OwnershipType.all
  end

  # GET /ownership_types/1
  # GET /ownership_types/1.json
  def show
  end

  # GET /ownership_types/new
  def new
    @ownership_type = OwnershipType.new
  end

  # GET /ownership_types/1/edit
  def edit
  end

  # POST /ownership_types
  # POST /ownership_types.json
  def create
    @ownership_type = OwnershipType.new(ownership_type_params)

    respond_to do |format|
      if @ownership_type.save
        format.html { redirect_to @ownership_type, notice: 'Ownership type was successfully created.' }
        format.json { render :show, status: :created, location: @ownership_type }
      else
        format.html { render :new }
        format.json { render json: @ownership_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ownership_types/1
  # PATCH/PUT /ownership_types/1.json
  def update
    respond_to do |format|
      if @ownership_type.update(ownership_type_params)
        format.html { redirect_to @ownership_type, notice: 'Ownership type was successfully updated.' }
        format.json { render :show, status: :ok, location: @ownership_type }
      else
        format.html { render :edit }
        format.json { render json: @ownership_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ownership_types/1
  # DELETE /ownership_types/1.json
  def destroy
    @ownership_type.destroy
    respond_to do |format|
      format.html { redirect_to ownership_types_url, notice: 'Ownership type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ownership_type
      @ownership_type = OwnershipType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ownership_type_params
      params.require(:ownership_type).permit(:ownership_type_desc)
    end
end
