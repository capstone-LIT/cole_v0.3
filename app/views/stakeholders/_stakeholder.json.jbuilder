json.extract! stakeholder, :id, :stakeholder_status_id, :stakeholder_fname, :stakeholder_lname, :stakeholder_phone, :stakeholder_email, :created_at, :updated_at
json.url stakeholder_url(stakeholder, format: :json)
