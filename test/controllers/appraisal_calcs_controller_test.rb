require 'test_helper'

class AppraisalCalcsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @appraisal_calc = appraisal_calcs(:one)
  end

  test "should get index" do
    get appraisal_calcs_url
    assert_response :success
  end

  test "should get new" do
    get new_appraisal_calc_url
    assert_response :success
  end

  test "should create appraisal_calc" do
    assert_difference('AppraisalCalc.count') do
      post appraisal_calcs_url, params: { appraisal_calc: { appraisal_calc_status_id: @appraisal_calc.appraisal_calc_status_id, asset_approach_conf: @appraisal_calc.asset_approach_conf, business_type_id: @appraisal_calc.business_type_id, buyer_down_pmt: @appraisal_calc.buyer_down_pmt, capt_expend_est: @appraisal_calc.capt_expend_est, comm_finance: @appraisal_calc.comm_finance, cont_earn_out: @appraisal_calc.cont_earn_out, debt_assum: @appraisal_calc.debt_assum, est_loan_fee: @appraisal_calc.est_loan_fee, est_value_rev: @appraisal_calc.est_value_rev, est_value_rev_var: @appraisal_calc.est_value_rev_var, facility_type_id: @appraisal_calc.facility_type_id, income_approach_conf: @appraisal_calc.income_approach_conf, interim_end: @appraisal_calc.interim_end, interim_start: @appraisal_calc.interim_start, market_approach_conf: @appraisal_calc.market_approach_conf, mult_of_rev: @appraisal_calc.mult_of_rev, mult_of_rev_var: @appraisal_calc.mult_of_rev_var, mult_of_sde: @appraisal_calc.mult_of_sde, mult_of_sde_var: @appraisal_calc.mult_of_sde_var, new_owner_sal: @appraisal_calc.new_owner_sal, organization_id: @appraisal_calc.organization_id, projected_conf: @appraisal_calc.projected_conf, report_date: @appraisal_calc.report_date, seller_finance: @appraisal_calc.seller_finance, term: @appraisal_calc.term, trans_found: @appraisal_calc.trans_found, year_one_conf: @appraisal_calc.year_one_conf, year_three_conf: @appraisal_calc.year_three_conf, year_two_conf: @appraisal_calc.year_two_conf } }
    end

    assert_redirected_to appraisal_calc_url(AppraisalCalc.last)
  end

  test "should show appraisal_calc" do
    get appraisal_calc_url(@appraisal_calc)
    assert_response :success
  end

  test "should get edit" do
    get edit_appraisal_calc_url(@appraisal_calc)
    assert_response :success
  end

  test "should update appraisal_calc" do
    patch appraisal_calc_url(@appraisal_calc), params: { appraisal_calc: { appraisal_calc_status_id: @appraisal_calc.appraisal_calc_status_id, asset_approach_conf: @appraisal_calc.asset_approach_conf, business_type_id: @appraisal_calc.business_type_id, buyer_down_pmt: @appraisal_calc.buyer_down_pmt, capt_expend_est: @appraisal_calc.capt_expend_est, comm_finance: @appraisal_calc.comm_finance, cont_earn_out: @appraisal_calc.cont_earn_out, debt_assum: @appraisal_calc.debt_assum, est_loan_fee: @appraisal_calc.est_loan_fee, est_value_rev: @appraisal_calc.est_value_rev, est_value_rev_var: @appraisal_calc.est_value_rev_var, facility_type_id: @appraisal_calc.facility_type_id, income_approach_conf: @appraisal_calc.income_approach_conf, interim_end: @appraisal_calc.interim_end, interim_start: @appraisal_calc.interim_start, market_approach_conf: @appraisal_calc.market_approach_conf, mult_of_rev: @appraisal_calc.mult_of_rev, mult_of_rev_var: @appraisal_calc.mult_of_rev_var, mult_of_sde: @appraisal_calc.mult_of_sde, mult_of_sde_var: @appraisal_calc.mult_of_sde_var, new_owner_sal: @appraisal_calc.new_owner_sal, organization_id: @appraisal_calc.organization_id, projected_conf: @appraisal_calc.projected_conf, report_date: @appraisal_calc.report_date, seller_finance: @appraisal_calc.seller_finance, term: @appraisal_calc.term, trans_found: @appraisal_calc.trans_found, year_one_conf: @appraisal_calc.year_one_conf, year_three_conf: @appraisal_calc.year_three_conf, year_two_conf: @appraisal_calc.year_two_conf } }
    assert_redirected_to appraisal_calc_url(@appraisal_calc)
  end

  test "should destroy appraisal_calc" do
    assert_difference('AppraisalCalc.count', -1) do
      delete appraisal_calc_url(@appraisal_calc)
    end

    assert_redirected_to appraisal_calcs_url
  end
end
