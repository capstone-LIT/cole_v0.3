class HistOrgPerformanceRankingsController < ApplicationController
  before_action :set_hist_org_performance_ranking, only: [:show, :edit, :update, :destroy]

  # GET /hist_org_performance_rankings
  # GET /hist_org_performance_rankings.json
  def index
    @hist_org_performance_rankings = HistOrgPerformanceRanking.all
  end

  # GET /hist_org_performance_rankings/1
  # GET /hist_org_performance_rankings/1.json
  def show
  end

  # GET /hist_org_performance_rankings/new
  def new
    @hist_org_performance_ranking = HistOrgPerformanceRanking.new
  end

  # GET /hist_org_performance_rankings/1/edit
  def edit
  end

  # POST /hist_org_performance_rankings
  # POST /hist_org_performance_rankings.json
  def create
    @hist_org_performance_ranking = HistOrgPerformanceRanking.new(hist_org_performance_ranking_params)

    respond_to do |format|
      if @hist_org_performance_ranking.save
        format.html { redirect_to @hist_org_performance_ranking, notice: 'Hist org performance ranking was successfully created.' }
        format.json { render :show, status: :created, location: @hist_org_performance_ranking }
      else
        format.html { render :new }
        format.json { render json: @hist_org_performance_ranking.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /hist_org_performance_rankings/1
  # PATCH/PUT /hist_org_performance_rankings/1.json
  def update
    respond_to do |format|
      if @hist_org_performance_ranking.update(hist_org_performance_ranking_params)
        format.html { redirect_to @hist_org_performance_ranking, notice: 'Hist org performance ranking was successfully updated.' }
        format.json { render :show, status: :ok, location: @hist_org_performance_ranking }
      else
        format.html { render :edit }
        format.json { render json: @hist_org_performance_ranking.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hist_org_performance_rankings/1
  # DELETE /hist_org_performance_rankings/1.json
  def destroy
    @hist_org_performance_ranking.destroy
    respond_to do |format|
      format.html { redirect_to hist_org_performance_rankings_url, notice: 'Hist org performance ranking was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_hist_org_performance_ranking
      @hist_org_performance_ranking = HistOrgPerformanceRanking.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def hist_org_performance_ranking_params
      params.require(:hist_org_performance_ranking).permit(:hist_org_performance_ranking_desc)
    end
end
