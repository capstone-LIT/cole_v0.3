require 'test_helper'

class LocAndFacRankingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @loc_and_fac_ranking = loc_and_fac_rankings(:one)
  end

  test "should get index" do
    get loc_and_fac_rankings_url
    assert_response :success
  end

  test "should get new" do
    get new_loc_and_fac_ranking_url
    assert_response :success
  end

  test "should create loc_and_fac_ranking" do
    assert_difference('LocAndFacRanking.count') do
      post loc_and_fac_rankings_url, params: { loc_and_fac_ranking: { loc_and_fac_desc: @loc_and_fac_ranking.loc_and_fac_desc } }
    end

    assert_redirected_to loc_and_fac_ranking_url(LocAndFacRanking.last)
  end

  test "should show loc_and_fac_ranking" do
    get loc_and_fac_ranking_url(@loc_and_fac_ranking)
    assert_response :success
  end

  test "should get edit" do
    get edit_loc_and_fac_ranking_url(@loc_and_fac_ranking)
    assert_response :success
  end

  test "should update loc_and_fac_ranking" do
    patch loc_and_fac_ranking_url(@loc_and_fac_ranking), params: { loc_and_fac_ranking: { loc_and_fac_desc: @loc_and_fac_ranking.loc_and_fac_desc } }
    assert_redirected_to loc_and_fac_ranking_url(@loc_and_fac_ranking)
  end

  test "should destroy loc_and_fac_ranking" do
    assert_difference('LocAndFacRanking.count', -1) do
      delete loc_and_fac_ranking_url(@loc_and_fac_ranking)
    end

    assert_redirected_to loc_and_fac_rankings_url
  end
end
