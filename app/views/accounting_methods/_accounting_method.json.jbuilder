json.extract! accounting_method, :id, :accounting_method_desc, :created_at, :updated_at
json.url accounting_method_url(accounting_method, format: :json)
