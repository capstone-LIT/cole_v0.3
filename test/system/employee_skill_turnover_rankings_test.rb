require "application_system_test_case"

class EmployeeSkillTurnoverRankingsTest < ApplicationSystemTestCase
  setup do
    @employee_skill_turnover_ranking = employee_skill_turnover_rankings(:one)
  end

  test "visiting the index" do
    visit employee_skill_turnover_rankings_url
    assert_selector "h1", text: "Employee Skill Turnover Rankings"
  end

  test "creating a Employee skill turnover ranking" do
    visit employee_skill_turnover_rankings_url
    click_on "New Employee Skill Turnover Ranking"

    fill_in "Emp Skill Turn Desc", with: @employee_skill_turnover_ranking.emp_skill_turn_desc
    click_on "Create Employee skill turnover ranking"

    assert_text "Employee skill turnover ranking was successfully created"
    click_on "Back"
  end

  test "updating a Employee skill turnover ranking" do
    visit employee_skill_turnover_rankings_url
    click_on "Edit", match: :first

    fill_in "Emp Skill Turn Desc", with: @employee_skill_turnover_ranking.emp_skill_turn_desc
    click_on "Update Employee skill turnover ranking"

    assert_text "Employee skill turnover ranking was successfully updated"
    click_on "Back"
  end

  test "destroying a Employee skill turnover ranking" do
    visit employee_skill_turnover_rankings_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Employee skill turnover ranking was successfully destroyed"
  end
end
