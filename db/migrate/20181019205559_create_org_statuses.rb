class CreateOrgStatuses < ActiveRecord::Migration[5.2]
  def change
    create_table :org_statuses do |t|
      t.string :status_desc

      t.timestamps
    end
  end
end
