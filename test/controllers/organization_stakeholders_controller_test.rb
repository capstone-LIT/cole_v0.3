require 'test_helper'

class OrganizationStakeholdersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @organization_stakeholder = organization_stakeholders(:one)
  end

  test "should get index" do
    get organization_stakeholders_url
    assert_response :success
  end

  test "should get new" do
    get new_organization_stakeholder_url
    assert_response :success
  end

  test "should create organization_stakeholder" do
    assert_difference('OrganizationStakeholder.count') do
      post organization_stakeholders_url, params: { organization_stakeholder: { organization_id: @organization_stakeholder.organization_id, percent_owned: @organization_stakeholder.percent_owned, stakeholder_id: @organization_stakeholder.stakeholder_id, stakeholder_rank_id: @organization_stakeholder.stakeholder_rank_id } }
    end

    assert_redirected_to organization_stakeholder_url(OrganizationStakeholder.last)
  end

  test "should show organization_stakeholder" do
    get organization_stakeholder_url(@organization_stakeholder)
    assert_response :success
  end

  test "should get edit" do
    get edit_organization_stakeholder_url(@organization_stakeholder)
    assert_response :success
  end

  test "should update organization_stakeholder" do
    patch organization_stakeholder_url(@organization_stakeholder), params: { organization_stakeholder: { organization_id: @organization_stakeholder.organization_id, percent_owned: @organization_stakeholder.percent_owned, stakeholder_id: @organization_stakeholder.stakeholder_id, stakeholder_rank_id: @organization_stakeholder.stakeholder_rank_id } }
    assert_redirected_to organization_stakeholder_url(@organization_stakeholder)
  end

  test "should destroy organization_stakeholder" do
    assert_difference('OrganizationStakeholder.count', -1) do
      delete organization_stakeholder_url(@organization_stakeholder)
    end

    assert_redirected_to organization_stakeholders_url
  end
end
