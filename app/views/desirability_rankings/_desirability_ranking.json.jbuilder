json.extract! desirability_ranking, :id, :desirability_desc, :created_at, :updated_at
json.url desirability_ranking_url(desirability_ranking, format: :json)
