class OrganizationsController < ApplicationController
  before_action :set_organization, only: [:show, :edit, :update, :destroy, :org_statements]
  respond_to :docx

  #htmltoword download
  def download1
    @complete_client_report = Organization.all
    respond_with(@complete_client_report, filename: 'complete_client_report.docx', word_template: 'download1.docx')
  end

  # GET /organizations
  # GET /organizations.json
  def index
    if current_user.is_broker?
        @organizations = Organization.all
    else
      redirect_to '/client_home'
      end
  end

  def complete_client_report
    if current_user.is_broker?
      @complete_client_report = Organization.all
    else
      redirect_to '/client_home'
    end
  end

  def pending_client_input_report
    if current_user.is_broker?
      @pending_client_input_report = Organization.all
    else
      redirect_to '/client_home'
    end
  end

  def org_statements
    if !current_user.is_broker? && current_user.get_org != @organization
      redirect_to '/client_home'
    else
    @income_statements = IncomeStatement.where(organization_id: @organization.id).where.not(income_stmt_status_id: 3)
      end
  end

  # GET /organizations/1
  # GET /organizations/1.json
  def show
    if !current_user.is_broker? && current_user.get_org != @organization
      redirect_to '/client_home'
    end
  end

  # GET /organizations/new
  def new
    if current_user.get_org.nil?
    @organization = Organization.new
    @stakeholder1 = Stakeholder.new
    @stakeholder2 = Stakeholder.new
    @stakeholder3 = Stakeholder.new
    else
      redirect_to '/client_home'
      end
  end

  # GET /organizations/1/edit
  def edit
    if !current_user.is_broker? && current_user.get_org != @organization
      redirect_to '/client_home'
    end
    stakeholders = OrganizationStakeholder.where(:organization_id => @organization.id)
    @stakeholder1 = stakeholders.where(:stakeholder_rank_id => 1)
    @stakeholder2 = stakeholders.where(:stakeholder_rank_id => 2)
    @stakeholder3 = stakeholders.where(:stakeholder_rank_id => 3)
  end

  # POST /organizations
  # POST /organizations.json
  def create
    if !current_user.is_broker? && current_user.get_org != @organization
      redirect_to '/client_home'
    else

    @organization = Organization.new(organization_params)
    @organization.user_id = current_user.id
    @organization.org_status_id = 1
    respond_to do |format|
      if @organization.save
        if current_user.is_broker?
          format.html { redirect_to '/appraiser_home', notice: 'Organization was successfully created.' }
          format.json { render :show, status: :created, location: @balance_sheet }
        else
          format.html { redirect_to '/client_home', notice: 'Organization was successfully created.' }
          format.json { render :show, status: :created, location: @balance_sheet }
        end
      else
        format.html { render :new }
        format.json { render json: @organization.errors, status: :unprocessable_entity }
      end
    end
    end
  end

  # PATCH/PUT /organizations/1
  # PATCH/PUT /organizations/1.json
  def update
    if !current_user.is_broker? && current_user.get_org != @organization
      redirect_to '/client_home'
    else

      respond_to do |format|
      if @organization.update(organization_params)
        if current_user.is_broker?
          format.html { redirect_to '/appraiser_home' }
          format.json { render :show, status: :created, location: @balance_sheet }
        else
          format.html { redirect_to '/client_home' }
          format.json { render :show, status: :created, location: @balance_sheet }
        end
      else
        format.html { render :edit }
        format.json { render json: @organization.errors, status: :unprocessable_entity }
      end
      end
      end
  end

  # DELETE /organizations/1
  # DELETE /organizations/1.json
  def destroy
    if !current_user.is_broker? && current_user.get_org != @organization
      redirect_to '/client_home'
    else

      @organization.org_status_id = 2
      @organization.save
    sheets = @organization.balance_sheets
    sheets.each do |st|
    st.balance_sheet_status_id = 4
      st.save
    end
    income = @organization.income_statements
    income.each do |inc|
      inc.income_stmt_status_id = 4
      inc.save
    end
     appraise = @organization.appraisal_calcs
    appraise.each do |app|
      app.appraisal_calc_status_id = 3
      app.save
    end
      user = @organization.user
        user.user_role_id = 4
        user.encrypted_password = 'deleted'
        user.save
    respond_to do |format|
      format.html { redirect_to organizations_url, notice: 'Organization was successfully destroyed.' }
      format.json { head :no_content }
    end
      end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_organization
      @organization = Organization.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def organization_params
      params.require(:organization).permit(:org_name, :org_address, :org_city, :state_id, :org_zip, :org_website, :full_emp, :part_emp, :founding_year, :ownership_type_id, :prim_naics, :sec_naics, :brief_desc_of_services, :real_estate_type_id, :real_estate_included, :futr_rent, :lease_expir_date, :extend_date, :hist_earning_ranking_id, :finance_rec_ranking_id, :depth_of_management_ranking_id, :loc_and_fac_ranking_id, :competition_ranking_id, :diversification_ranking_id, :equipment_sys_ranking_id, :employee_skill_turnover_ranking_id, :desirability_ranking_id, :hist_org_performance_ranking_id, :org_comment, :balance_sheets_attributes => [:balance_sheet_id, :balance_sheet_status_id, :sheet_desc], :income_statement_attributes => [:income_stmt_id, :income_stmt_statuses])
    end
end
