json.extract! balance_sheet_status, :id, :sheet_desc, :created_at, :updated_at
json.url balance_sheet_status_url(balance_sheet_status, format: :json)
