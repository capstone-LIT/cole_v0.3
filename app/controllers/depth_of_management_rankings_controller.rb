class DepthOfManagementRankingsController < ApplicationController
  before_action :set_depth_of_management_ranking, only: [:show, :edit, :update, :destroy]

  # GET /depth_of_management_rankings
  # GET /depth_of_management_rankings.json
  def index
    @depth_of_management_rankings = DepthOfManagementRanking.all
  end

  # GET /depth_of_management_rankings/1
  # GET /depth_of_management_rankings/1.json
  def show
  end

  # GET /depth_of_management_rankings/new
  def new
    @depth_of_management_ranking = DepthOfManagementRanking.new
  end

  # GET /depth_of_management_rankings/1/edit
  def edit
  end

  # POST /depth_of_management_rankings
  # POST /depth_of_management_rankings.json
  def create
    @depth_of_management_ranking = DepthOfManagementRanking.new(depth_of_management_ranking_params)

    respond_to do |format|
      if @depth_of_management_ranking.save
        format.html { redirect_to @depth_of_management_ranking, notice: 'Depth of management ranking was successfully created.' }
        format.json { render :show, status: :created, location: @depth_of_management_ranking }
      else
        format.html { render :new }
        format.json { render json: @depth_of_management_ranking.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /depth_of_management_rankings/1
  # PATCH/PUT /depth_of_management_rankings/1.json
  def update
    respond_to do |format|
      if @depth_of_management_ranking.update(depth_of_management_ranking_params)
        format.html { redirect_to @depth_of_management_ranking, notice: 'Depth of management ranking was successfully updated.' }
        format.json { render :show, status: :ok, location: @depth_of_management_ranking }
      else
        format.html { render :edit }
        format.json { render json: @depth_of_management_ranking.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /depth_of_management_rankings/1
  # DELETE /depth_of_management_rankings/1.json
  def destroy
    @depth_of_management_ranking.destroy
    respond_to do |format|
      format.html { redirect_to depth_of_management_rankings_url, notice: 'Depth of management ranking was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_depth_of_management_ranking
      @depth_of_management_ranking = DepthOfManagementRanking.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def depth_of_management_ranking_params
      params.require(:depth_of_management_ranking).permit(:depth_of_mgmt_desc)
    end
end
