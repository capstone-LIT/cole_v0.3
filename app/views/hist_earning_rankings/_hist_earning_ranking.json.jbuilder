json.extract! hist_earning_ranking, :id, :hist_earning_desc, :created_at, :updated_at
json.url hist_earning_ranking_url(hist_earning_ranking, format: :json)
