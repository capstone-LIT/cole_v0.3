json.extract! competition_ranking, :id, :competition_desc, :created_at, :updated_at
json.url competition_ranking_url(competition_ranking, format: :json)
