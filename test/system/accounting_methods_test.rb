require "application_system_test_case"

class AccountingMethodsTest < ApplicationSystemTestCase
  setup do
    @accounting_method = accounting_methods(:one)
  end

  test "visiting the index" do
    visit accounting_methods_url
    assert_selector "h1", text: "Accounting Methods"
  end

  test "creating a Accounting method" do
    visit accounting_methods_url
    click_on "New Accounting Method"

    fill_in "Accounting Method Desc", with: @accounting_method.accounting_method_desc
    click_on "Create Accounting method"

    assert_text "Accounting method was successfully created"
    click_on "Back"
  end

  test "updating a Accounting method" do
    visit accounting_methods_url
    click_on "Edit", match: :first

    fill_in "Accounting Method Desc", with: @accounting_method.accounting_method_desc
    click_on "Update Accounting method"

    assert_text "Accounting method was successfully updated"
    click_on "Back"
  end

  test "destroying a Accounting method" do
    visit accounting_methods_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Accounting method was successfully destroyed"
  end
end
