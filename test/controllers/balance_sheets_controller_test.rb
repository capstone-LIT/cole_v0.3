require 'test_helper'

class BalanceSheetsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @balance_sheet = balance_sheets(:one)
  end

  test "should get index" do
    get balance_sheets_url
    assert_response :success
  end

  test "should get new" do
    get new_balance_sheet_url
    assert_response :success
  end

  test "should create balance_sheet" do
    assert_difference('BalanceSheet.count') do
      post balance_sheets_url, params: { balance_sheet: { accounting_method_id: @balance_sheet.accounting_method_id, accounts_p: @balance_sheet.accounts_p, accounts_r: @balance_sheet.accounts_r, accum_a: @balance_sheet.accum_a, accum_d: @balance_sheet.accum_d, balance_sheet_status_id: @balance_sheet.balance_sheet_status_id, bank_debt: @balance_sheet.bank_debt, cash_on_hand: @balance_sheet.cash_on_hand, cash_req: @balance_sheet.cash_req, curr_ffe_v: @balance_sheet.curr_ffe_v, ffe_v: @balance_sheet.ffe_v, financial_source_type_id: @balance_sheet.financial_source_type_id, in_req: @balance_sheet.in_req, inventory: @balance_sheet.inventory, organization_id: @balance_sheet.organization_id, other_asset: @balance_sheet.other_asset, other_liability: @balance_sheet.other_liability, real_estate_est: @balance_sheet.real_estate_est, real_estate_value: @balance_sheet.real_estate_value, sheet_comment: @balance_sheet.sheet_comment, sheet_date: @balance_sheet.sheet_date, total_asset: @balance_sheet.total_asset, total_liability: @balance_sheet.total_liability } }
    end

    assert_redirected_to balance_sheet_url(BalanceSheet.last)
  end

  test "should show balance_sheet" do
    get balance_sheet_url(@balance_sheet)
    assert_response :success
  end

  test "should get edit" do
    get edit_balance_sheet_url(@balance_sheet)
    assert_response :success
  end

  test "should update balance_sheet" do
    patch balance_sheet_url(@balance_sheet), params: { balance_sheet: { accounting_method_id: @balance_sheet.accounting_method_id, accounts_p: @balance_sheet.accounts_p, accounts_r: @balance_sheet.accounts_r, accum_a: @balance_sheet.accum_a, accum_d: @balance_sheet.accum_d, balance_sheet_status_id: @balance_sheet.balance_sheet_status_id, bank_debt: @balance_sheet.bank_debt, cash_on_hand: @balance_sheet.cash_on_hand, cash_req: @balance_sheet.cash_req, curr_ffe_v: @balance_sheet.curr_ffe_v, ffe_v: @balance_sheet.ffe_v, financial_source_type_id: @balance_sheet.financial_source_type_id, in_req: @balance_sheet.in_req, inventory: @balance_sheet.inventory, organization_id: @balance_sheet.organization_id, other_asset: @balance_sheet.other_asset, other_liability: @balance_sheet.other_liability, real_estate_est: @balance_sheet.real_estate_est, real_estate_value: @balance_sheet.real_estate_value, sheet_comment: @balance_sheet.sheet_comment, sheet_date: @balance_sheet.sheet_date, total_asset: @balance_sheet.total_asset, total_liability: @balance_sheet.total_liability } }
    assert_redirected_to balance_sheet_url(@balance_sheet)
  end

  test "should destroy balance_sheet" do
    assert_difference('BalanceSheet.count', -1) do
      delete balance_sheet_url(@balance_sheet)
    end

    assert_redirected_to balance_sheets_url
  end
end
