require 'test_helper'

class GitControllerTest < ActionDispatch::IntegrationTest
  test "should get reset" do
    get git_reset_url
    assert_response :success
  end

end
