class CreateAppraisalCalcStatuses < ActiveRecord::Migration[5.2]
  def change
    create_table :appraisal_calc_statuses do |t|
      t.string :calc_status_desc

      t.timestamps
    end
  end
end
