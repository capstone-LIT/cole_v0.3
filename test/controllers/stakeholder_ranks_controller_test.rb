require 'test_helper'

class StakeholderRanksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @stakeholder_rank = stakeholder_ranks(:one)
  end

  test "should get index" do
    get stakeholder_ranks_url
    assert_response :success
  end

  test "should get new" do
    get new_stakeholder_rank_url
    assert_response :success
  end

  test "should create stakeholder_rank" do
    assert_difference('StakeholderRank.count') do
      post stakeholder_ranks_url, params: { stakeholder_rank: { stakeholder_rank_desc: @stakeholder_rank.stakeholder_rank_desc } }
    end

    assert_redirected_to stakeholder_rank_url(StakeholderRank.last)
  end

  test "should show stakeholder_rank" do
    get stakeholder_rank_url(@stakeholder_rank)
    assert_response :success
  end

  test "should get edit" do
    get edit_stakeholder_rank_url(@stakeholder_rank)
    assert_response :success
  end

  test "should update stakeholder_rank" do
    patch stakeholder_rank_url(@stakeholder_rank), params: { stakeholder_rank: { stakeholder_rank_desc: @stakeholder_rank.stakeholder_rank_desc } }
    assert_redirected_to stakeholder_rank_url(@stakeholder_rank)
  end

  test "should destroy stakeholder_rank" do
    assert_difference('StakeholderRank.count', -1) do
      delete stakeholder_rank_url(@stakeholder_rank)
    end

    assert_redirected_to stakeholder_ranks_url
  end
end
