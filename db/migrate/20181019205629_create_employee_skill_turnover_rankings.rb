class CreateEmployeeSkillTurnoverRankings < ActiveRecord::Migration[5.2]
  def change
    create_table :employee_skill_turnover_rankings do |t|
      t.string :emp_skill_turn_desc

      t.timestamps
    end
  end
end
