class DesirabilityRankingsController < ApplicationController
  before_action :set_desirability_ranking, only: [:show, :edit, :update, :destroy]

  # GET /desirability_rankings
  # GET /desirability_rankings.json
  def index
    @desirability_rankings = DesirabilityRanking.all
  end

  # GET /desirability_rankings/1
  # GET /desirability_rankings/1.json
  def show
  end

  # GET /desirability_rankings/new
  def new
    @desirability_ranking = DesirabilityRanking.new
  end

  # GET /desirability_rankings/1/edit
  def edit
  end

  # POST /desirability_rankings
  # POST /desirability_rankings.json
  def create
    @desirability_ranking = DesirabilityRanking.new(desirability_ranking_params)

    respond_to do |format|
      if @desirability_ranking.save
        format.html { redirect_to @desirability_ranking, notice: 'Desirability ranking was successfully created.' }
        format.json { render :show, status: :created, location: @desirability_ranking }
      else
        format.html { render :new }
        format.json { render json: @desirability_ranking.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /desirability_rankings/1
  # PATCH/PUT /desirability_rankings/1.json
  def update
    respond_to do |format|
      if @desirability_ranking.update(desirability_ranking_params)
        format.html { redirect_to @desirability_ranking, notice: 'Desirability ranking was successfully updated.' }
        format.json { render :show, status: :ok, location: @desirability_ranking }
      else
        format.html { render :edit }
        format.json { render json: @desirability_ranking.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /desirability_rankings/1
  # DELETE /desirability_rankings/1.json
  def destroy
    @desirability_ranking.destroy
    respond_to do |format|
      format.html { redirect_to desirability_rankings_url, notice: 'Desirability ranking was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_desirability_ranking
      @desirability_ranking = DesirabilityRanking.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def desirability_ranking_params
      params.require(:desirability_ranking).permit(:desirability_desc)
    end
end
