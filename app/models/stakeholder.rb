class Stakeholder < ApplicationRecord
  belongs_to :stakeholder_status
  has_many :organization_stakeholders
  has_many :organizations, through: :organization_stakeholders
  has_many :stakeholder_ranks, through: :organization_stakeholders
end
