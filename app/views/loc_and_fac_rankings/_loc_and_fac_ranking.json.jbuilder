json.extract! loc_and_fac_ranking, :id, :loc_and_fac_desc, :created_at, :updated_at
json.url loc_and_fac_ranking_url(loc_and_fac_ranking, format: :json)
