class StakeholderRanksController < ApplicationController
  before_action :set_stakeholder_rank, only: [:show, :edit, :update, :destroy]

  # GET /stakeholder_ranks
  # GET /stakeholder_ranks.json
  def index
    @stakeholder_ranks = StakeholderRank.all
  end

  # GET /stakeholder_ranks/1
  # GET /stakeholder_ranks/1.json
  def show
  end

  # GET /stakeholder_ranks/new
  def new
    @stakeholder_rank = StakeholderRank.new
  end

  # GET /stakeholder_ranks/1/edit
  def edit
  end

  # POST /stakeholder_ranks
  # POST /stakeholder_ranks.json
  def create
    @stakeholder_rank = StakeholderRank.new(stakeholder_rank_params)

    respond_to do |format|
      if @stakeholder_rank.save
        format.html { redirect_to @stakeholder_rank, notice: 'Stakeholder rank was successfully created.' }
        format.json { render :show, status: :created, location: @stakeholder_rank }
      else
        format.html { render :new }
        format.json { render json: @stakeholder_rank.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /stakeholder_ranks/1
  # PATCH/PUT /stakeholder_ranks/1.json
  def update
    respond_to do |format|
      if @stakeholder_rank.update(stakeholder_rank_params)
        format.html { redirect_to @stakeholder_rank, notice: 'Stakeholder rank was successfully updated.' }
        format.json { render :show, status: :ok, location: @stakeholder_rank }
      else
        format.html { render :edit }
        format.json { render json: @stakeholder_rank.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stakeholder_ranks/1
  # DELETE /stakeholder_ranks/1.json
  def destroy
    @stakeholder_rank.destroy
    respond_to do |format|
      format.html { redirect_to stakeholder_ranks_url, notice: 'Stakeholder rank was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_stakeholder_rank
      @stakeholder_rank = StakeholderRank.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def stakeholder_rank_params
      params.require(:stakeholder_rank).permit(:stakeholder_rank_desc)
    end
end
