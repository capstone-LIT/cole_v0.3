json.extract! business_type, :id, :business_type_desc, :created_at, :updated_at
json.url business_type_url(business_type, format: :json)
