class BusInfoStepsController < ApplicationController

  include Wicked::Wizard
  steps :business, :bus_cont

  def show
    @user =current_user
    @user.update_attributes(params{:user})
    render_wizard
  end

end
