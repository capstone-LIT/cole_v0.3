Rails.application.routes.draw do
  get '' => 'pages#login'
  namespace :users do
    get 'invites/new'
  end
  resources :user_roles
  devise_for :users, controllers:  {
    sessions: 'users/sessions',
    registrations: 'users/registrations'

  }

 get 'download1' => 'organizations#download1', format: 'docx'
  get 'download2' => 'appraisal_calcs#download2', format: 'docx'
  get 'appraisal_report' => 'appraisal_calcs#appraisal_report', format: 'pdf'
  get 'interim_period/new'
  get 'interim_period/edit'
  get 'interim_period/show'
  get 'interim_period/new'
  get 'interim_period/create'
  #root 'pages#home'
  # config/initializers/high_voltage.rb
HighVoltage.configure do |config|
  config.home_page = 'login'
end

# config/initializers/high_voltage.rb
HighVoltage.configure do |config|
  config.route_drawer = HighVoltage::RouteDrawers::Root
end
  get '/organizations/complete_client_report' => 'organizations#complete_client_report'
  get '/organizations/org_statements/:id' => 'organizations#org_statements'
  get '/appraisal_calcs/pending_appraisal_report' => 'appraisal_calcs#pending_appraisal_report'
  get '/appraisal_calcs/completed_appraisal_report' => 'appraisal_calcs#completed_appraisal_report'
  get '/organizations/pending_client_input_report' => 'organizations#pending_client_input_report'
  patch '/appraisal_calcs/refresh' => 'appraisal_calcs#refresh'
  post '/user/invites/create' => 'users/invites#create'
  resources :appraisal_calcs
  resources :income_statements
  resources :balance_sheets
  resources :organization_stakeholders
  resources :organizations
  resources :stakeholders
  #resources :pages
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
