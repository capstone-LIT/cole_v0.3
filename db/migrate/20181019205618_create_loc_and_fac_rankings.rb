class CreateLocAndFacRankings < ActiveRecord::Migration[5.2]
  def change
    create_table :loc_and_fac_rankings do |t|
      t.string :loc_and_fac_desc

      t.timestamps
    end
  end
end
