class CreateStakeholderStatuses < ActiveRecord::Migration[5.2]
  def change
    create_table :stakeholder_statuses do |t|
      t.string :stakeholder_status_desc

      t.timestamps
    end
  end
end
