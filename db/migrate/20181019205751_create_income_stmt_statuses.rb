class CreateIncomeStmtStatuses < ActiveRecord::Migration[5.2]
  def change
    create_table :income_stmt_statuses do |t|
      t.string :stmt_status_desc

      t.timestamps
    end
  end
end
