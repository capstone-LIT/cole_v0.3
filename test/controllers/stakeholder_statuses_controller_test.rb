require 'test_helper'

class StakeholderStatusesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @stakeholder_status = stakeholder_statuses(:one)
  end

  test "should get index" do
    get stakeholder_statuses_url
    assert_response :success
  end

  test "should get new" do
    get new_stakeholder_status_url
    assert_response :success
  end

  test "should create stakeholder_status" do
    assert_difference('StakeholderStatus.count') do
      post stakeholder_statuses_url, params: { stakeholder_status: { stakeholder_status_desc: @stakeholder_status.stakeholder_status_desc } }
    end

    assert_redirected_to stakeholder_status_url(StakeholderStatus.last)
  end

  test "should show stakeholder_status" do
    get stakeholder_status_url(@stakeholder_status)
    assert_response :success
  end

  test "should get edit" do
    get edit_stakeholder_status_url(@stakeholder_status)
    assert_response :success
  end

  test "should update stakeholder_status" do
    patch stakeholder_status_url(@stakeholder_status), params: { stakeholder_status: { stakeholder_status_desc: @stakeholder_status.stakeholder_status_desc } }
    assert_redirected_to stakeholder_status_url(@stakeholder_status)
  end

  test "should destroy stakeholder_status" do
    assert_difference('StakeholderStatus.count', -1) do
      delete stakeholder_status_url(@stakeholder_status)
    end

    assert_redirected_to stakeholder_statuses_url
  end
end
