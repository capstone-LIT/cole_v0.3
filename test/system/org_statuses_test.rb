require "application_system_test_case"

class OrgStatusesTest < ApplicationSystemTestCase
  setup do
    @org_status = org_statuses(:one)
  end

  test "visiting the index" do
    visit org_statuses_url
    assert_selector "h1", text: "Org Statuses"
  end

  test "creating a Org status" do
    visit org_statuses_url
    click_on "New Org Status"

    fill_in "Status Desc", with: @org_status.status_desc
    click_on "Create Org status"

    assert_text "Org status was successfully created"
    click_on "Back"
  end

  test "updating a Org status" do
    visit org_statuses_url
    click_on "Edit", match: :first

    fill_in "Status Desc", with: @org_status.status_desc
    click_on "Update Org status"

    assert_text "Org status was successfully updated"
    click_on "Back"
  end

  test "destroying a Org status" do
    visit org_statuses_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Org status was successfully destroyed"
  end
end
