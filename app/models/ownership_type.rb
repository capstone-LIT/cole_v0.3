class OwnershipType < ApplicationRecord
  has_many :organizations
end
