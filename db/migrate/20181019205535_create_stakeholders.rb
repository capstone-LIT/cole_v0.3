class CreateStakeholders < ActiveRecord::Migration[5.2]
  def change
    create_table :stakeholders do |t|
      t.references :stakeholder_status, foreign_key: true
      t.string :stakeholder_fname
      t.string :stakeholder_lname
      t.string :stakeholder_phone
      t.string :stakeholder_email

      t.timestamps
    end
  end
end
