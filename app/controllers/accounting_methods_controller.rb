class AccountingMethodsController < ApplicationController
  before_action :set_accounting_method, only: [:show, :edit, :update, :destroy]

  # GET /accounting_methods
  # GET /accounting_methods.json
  def index
    @accounting_methods = AccountingMethod.all
  end

  # GET /accounting_methods/1
  # GET /accounting_methods/1.json
  def show
  end

  # GET /accounting_methods/new
  def new
    @accounting_method = AccountingMethod.new
  end

  # GET /accounting_methods/1/edit
  def edit
  end

  # POST /accounting_methods
  # POST /accounting_methods.json
  def create
    @accounting_method = AccountingMethod.new(accounting_method_params)

    respond_to do |format|
      if @accounting_method.save
        format.html { redirect_to @accounting_method, notice: 'Accounting method was successfully created.' }
        format.json { render :show, status: :created, location: @accounting_method }
      else
        format.html { render :new }
        format.json { render json: @accounting_method.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /accounting_methods/1
  # PATCH/PUT /accounting_methods/1.json
  def update
    respond_to do |format|
      if @accounting_method.update(accounting_method_params)
        format.html { redirect_to @accounting_method, notice: 'Accounting method was successfully updated.' }
        format.json { render :show, status: :ok, location: @accounting_method }
      else
        format.html { render :edit }
        format.json { render json: @accounting_method.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /accounting_methods/1
  # DELETE /accounting_methods/1.json
  def destroy
    @accounting_method.destroy
    respond_to do |format|
      format.html { redirect_to accounting_methods_url, notice: 'Accounting method was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_accounting_method
      @accounting_method = AccountingMethod.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def accounting_method_params
      params.require(:accounting_method).permit(:accounting_method_desc)
    end
end
