require "application_system_test_case"

class StakeholdersTest < ApplicationSystemTestCase
  setup do
    @stakeholder = stakeholders(:one)
  end

  test "visiting the index" do
    visit stakeholders_url
    assert_selector "h1", text: "Stakeholders"
  end

  test "creating a Stakeholder" do
    visit stakeholders_url
    click_on "New Stakeholder"

    fill_in "Stakeholder Email", with: @stakeholder.stakeholder_email
    fill_in "Stakeholder Fname", with: @stakeholder.stakeholder_fname
    fill_in "Stakeholder Lname", with: @stakeholder.stakeholder_lname
    fill_in "Stakeholder Phone", with: @stakeholder.stakeholder_phone
    fill_in "Stakeholder Status", with: @stakeholder.stakeholder_status_id
    click_on "Create Stakeholder"

    assert_text "Stakeholder was successfully created"
    click_on "Back"
  end

  test "updating a Stakeholder" do
    visit stakeholders_url
    click_on "Edit", match: :first

    fill_in "Stakeholder Email", with: @stakeholder.stakeholder_email
    fill_in "Stakeholder Fname", with: @stakeholder.stakeholder_fname
    fill_in "Stakeholder Lname", with: @stakeholder.stakeholder_lname
    fill_in "Stakeholder Phone", with: @stakeholder.stakeholder_phone
    fill_in "Stakeholder Status", with: @stakeholder.stakeholder_status_id
    click_on "Update Stakeholder"

    assert_text "Stakeholder was successfully updated"
    click_on "Back"
  end

  test "destroying a Stakeholder" do
    visit stakeholders_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Stakeholder was successfully destroyed"
  end
end
