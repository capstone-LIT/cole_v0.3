json.extract! employee_skill_turnover_ranking, :id, :emp_skill_turn_desc, :created_at, :updated_at
json.url employee_skill_turnover_ranking_url(employee_skill_turnover_ranking, format: :json)
