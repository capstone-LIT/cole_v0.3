class HistEarningRankingsController < ApplicationController
  before_action :set_hist_earning_ranking, only: [:show, :edit, :update, :destroy]

  # GET /hist_earning_rankings
  # GET /hist_earning_rankings.json
  def index
    @hist_earning_rankings = HistEarningRanking.all
  end

  # GET /hist_earning_rankings/1
  # GET /hist_earning_rankings/1.json
  def show
  end

  # GET /hist_earning_rankings/new
  def new
    @hist_earning_ranking = HistEarningRanking.new
  end

  # GET /hist_earning_rankings/1/edit
  def edit
  end

  # POST /hist_earning_rankings
  # POST /hist_earning_rankings.json
  def create
    @hist_earning_ranking = HistEarningRanking.new(hist_earning_ranking_params)

    respond_to do |format|
      if @hist_earning_ranking.save
        format.html { redirect_to @hist_earning_ranking, notice: 'Hist earning ranking was successfully created.' }
        format.json { render :show, status: :created, location: @hist_earning_ranking }
      else
        format.html { render :new }
        format.json { render json: @hist_earning_ranking.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /hist_earning_rankings/1
  # PATCH/PUT /hist_earning_rankings/1.json
  def update
    respond_to do |format|
      if @hist_earning_ranking.update(hist_earning_ranking_params)
        format.html { redirect_to @hist_earning_ranking, notice: 'Hist earning ranking was successfully updated.' }
        format.json { render :show, status: :ok, location: @hist_earning_ranking }
      else
        format.html { render :edit }
        format.json { render json: @hist_earning_ranking.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hist_earning_rankings/1
  # DELETE /hist_earning_rankings/1.json
  def destroy
    @hist_earning_ranking.destroy
    respond_to do |format|
      format.html { redirect_to hist_earning_rankings_url, notice: 'Hist earning ranking was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_hist_earning_ranking
      @hist_earning_ranking = HistEarningRanking.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def hist_earning_ranking_params
      params.require(:hist_earning_ranking).permit(:hist_earning_desc)
    end
end
