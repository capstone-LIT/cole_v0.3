require "application_system_test_case"

class FinancialSourceTypesTest < ApplicationSystemTestCase
  setup do
    @financial_source_type = financial_source_types(:one)
  end

  test "visiting the index" do
    visit financial_source_types_url
    assert_selector "h1", text: "Financial Source Types"
  end

  test "creating a Financial source type" do
    visit financial_source_types_url
    click_on "New Financial Source Type"

    fill_in "Financial Source Type Desc", with: @financial_source_type.financial_source_type_desc
    click_on "Create Financial source type"

    assert_text "Financial source type was successfully created"
    click_on "Back"
  end

  test "updating a Financial source type" do
    visit financial_source_types_url
    click_on "Edit", match: :first

    fill_in "Financial Source Type Desc", with: @financial_source_type.financial_source_type_desc
    click_on "Update Financial source type"

    assert_text "Financial source type was successfully updated"
    click_on "Back"
  end

  test "destroying a Financial source type" do
    visit financial_source_types_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Financial source type was successfully destroyed"
  end
end
