class PagesController < ApplicationController
  def login
    if current_user.is_broker?
      redirect_to '/appraiser_home'
    else
      redirect_to '/client_home'
    end
  end
end
