class IncomeStmtStatus < ApplicationRecord
  has_many :income_statements
end
