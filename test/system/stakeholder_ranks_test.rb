require "application_system_test_case"

class StakeholderRanksTest < ApplicationSystemTestCase
  setup do
    @stakeholder_rank = stakeholder_ranks(:one)
  end

  test "visiting the index" do
    visit stakeholder_ranks_url
    assert_selector "h1", text: "Stakeholder Ranks"
  end

  test "creating a Stakeholder rank" do
    visit stakeholder_ranks_url
    click_on "New Stakeholder Rank"

    fill_in "Stakeholder Rank Desc", with: @stakeholder_rank.stakeholder_rank_desc
    click_on "Create Stakeholder rank"

    assert_text "Stakeholder rank was successfully created"
    click_on "Back"
  end

  test "updating a Stakeholder rank" do
    visit stakeholder_ranks_url
    click_on "Edit", match: :first

    fill_in "Stakeholder Rank Desc", with: @stakeholder_rank.stakeholder_rank_desc
    click_on "Update Stakeholder rank"

    assert_text "Stakeholder rank was successfully updated"
    click_on "Back"
  end

  test "destroying a Stakeholder rank" do
    visit stakeholder_ranks_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Stakeholder rank was successfully destroyed"
  end
end
