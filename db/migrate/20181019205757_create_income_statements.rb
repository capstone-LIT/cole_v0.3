class CreateIncomeStatements < ActiveRecord::Migration[5.2]
  def change
    create_table :income_statements do |t|
      t.integer :stmt_year
      t.float :total_sales
      t.float :pretax_profit
      t.float :partner_comp
      t.float :dep_an_am
      t.float :interest_exp
      t.float :non_rec_exp
      t.float :own_veh_exp
      t.float :own_insurance_exp
      t.references :income_stmt_status, foreign_key: true
      t.float :owners_travel_exp
      t.float :sal_adj
      t.float :own_retirement
      t.float :cost_of_goods
      t.references :organization, foreign_key: true
      t.float :payroll
      t.float :maintenance
      t.float :bad_debt
      t.float :rent
      t.float :tax_and_lic
      t.float :advertising
      t.float :pension_profit_share
      t.float :emp_benefit_cost
      t.float :bank_cc_fee
      t.float :insurance_cost
      t.float :legal_acct_exp
      t.float :outside_exp
      t.float :utility
      t.float :veh_exp
      t.float :est_growth_rate
      t.date :int_month
      t.float :total_sales_prev
      t.float :pretax_profit_prev
      t.float :total_sales_curr
      t.float :pretax_profit_curr
      t.text :stmt_comment

      t.timestamps
    end
  end
end
