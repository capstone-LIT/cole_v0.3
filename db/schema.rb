# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_10_30_030839) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounting_methods", force: :cascade do |t|
    t.string "accounting_method_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "appraisal_calc_statuses", force: :cascade do |t|
    t.string "calc_status_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "appraisal_calcs", force: :cascade do |t|
    t.bigint "organization_id"
    t.float "year_one_conf"
    t.float "year_two_conf"
    t.float "year_three_conf"
    t.float "projected_conf"
    t.integer "trans_found"
    t.float "mult_of_rev"
    t.float "mult_of_rev_var"
    t.float "mult_of_sde"
    t.float "mult_of_sde_var"
    t.float "est_val_rev_conf"
    t.float "est_value_sde_conf"
    t.float "asset_approach_conf"
    t.float "income_approach_conf"
    t.float "market_approach_conf"
    t.float "buyer_down_pmt"
    t.float "debt_assum"
    t.float "seller_finance"
    t.float "comm_finance"
    t.float "cont_earn_out"
    t.date "report_date"
    t.string "interim_start"
    t.string "interim_end"
    t.bigint "business_type_id"
    t.bigint "facility_type_id"
    t.float "est_loan_fee"
    t.integer "term"
    t.float "capt_expend_est"
    t.float "new_owner_sal"
    t.bigint "appraisal_calc_status_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["appraisal_calc_status_id"], name: "index_appraisal_calcs_on_appraisal_calc_status_id"
    t.index ["business_type_id"], name: "index_appraisal_calcs_on_business_type_id"
    t.index ["facility_type_id"], name: "index_appraisal_calcs_on_facility_type_id"
    t.index ["organization_id"], name: "index_appraisal_calcs_on_organization_id"
  end

  create_table "balance_sheet_statuses", force: :cascade do |t|
    t.string "sheet_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "balance_sheets", force: :cascade do |t|
    t.bigint "organization_id"
    t.bigint "financial_source_type_id"
    t.date "sheet_date"
    t.bigint "accounting_method_id"
    t.float "total_asset"
    t.float "cash_on_hand"
    t.float "accounts_r"
    t.float "inventory"
    t.float "real_estate_value"
    t.float "ffe_v"
    t.float "accum_d"
    t.float "accum_a"
    t.float "other_asset"
    t.float "total_liability"
    t.float "accounts_p"
    t.float "bank_debt"
    t.float "other_liability"
    t.float "cash_req"
    t.float "inv_req"
    t.float "curr_ffe_v"
    t.float "real_estate_est"
    t.text "sheet_comment"
    t.bigint "balance_sheet_status_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["accounting_method_id"], name: "index_balance_sheets_on_accounting_method_id"
    t.index ["balance_sheet_status_id"], name: "index_balance_sheets_on_balance_sheet_status_id"
    t.index ["financial_source_type_id"], name: "index_balance_sheets_on_financial_source_type_id"
    t.index ["organization_id"], name: "index_balance_sheets_on_organization_id"
  end

  create_table "business_types", force: :cascade do |t|
    t.string "business_type_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "competition_rankings", force: :cascade do |t|
    t.string "competition_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "depth_of_management_rankings", force: :cascade do |t|
    t.string "depth_of_mgmt_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "desirability_rankings", force: :cascade do |t|
    t.string "desirability_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "diversification_rankings", force: :cascade do |t|
    t.string "diversification_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "employee_skill_turnover_rankings", force: :cascade do |t|
    t.string "emp_skill_turn_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "equipment_sys_rankings", force: :cascade do |t|
    t.string "eqp_sys_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "facility_types", force: :cascade do |t|
    t.string "facility_type_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "finance_rec_rankings", force: :cascade do |t|
    t.string "finance_rec_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "financial_source_types", force: :cascade do |t|
    t.string "financial_source_type_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "hist_earning_rankings", force: :cascade do |t|
    t.string "hist_earning_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "hist_org_performance_rankings", force: :cascade do |t|
    t.string "hist_org_performance_ranking_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "income_statements", force: :cascade do |t|
    t.integer "stmt_year"
    t.float "total_sales"
    t.float "pretax_profit"
    t.float "partner_comp"
    t.float "dep_an_am"
    t.float "interest_exp"
    t.float "non_rec_exp"
    t.float "own_veh_exp"
    t.float "own_insurance_exp"
    t.bigint "income_stmt_status_id"
    t.float "owners_travel_exp"
    t.float "sal_adj"
    t.float "own_retirement"
    t.float "cost_of_goods"
    t.bigint "organization_id"
    t.float "payroll"
    t.float "maintenance"
    t.float "bad_debt"
    t.float "rent"
    t.float "tax_and_lic"
    t.float "advertising"
    t.float "pension_profit_share"
    t.float "emp_benefit_cost"
    t.float "bank_cc_fee"
    t.float "insurance_cost"
    t.float "legal_acct_exp"
    t.float "outside_exp"
    t.float "utility"
    t.float "veh_exp"
    t.float "est_growth_rate"
    t.date "int_month"
    t.float "total_sales_prev"
    t.float "pretax_profit_prev"
    t.float "total_sales_curr"
    t.float "pretax_profit_curr"
    t.text "stmt_comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["income_stmt_status_id"], name: "index_income_statements_on_income_stmt_status_id"
    t.index ["organization_id"], name: "index_income_statements_on_organization_id"
  end

  create_table "income_stmt_statuses", force: :cascade do |t|
    t.string "stmt_status_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "loc_and_fac_rankings", force: :cascade do |t|
    t.string "loc_and_fac_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "org_statuses", force: :cascade do |t|
    t.string "status_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "organization_stakeholders", force: :cascade do |t|
    t.bigint "organization_id"
    t.bigint "stakeholder_id"
    t.bigint "stakeholder_rank_id"
    t.float "percent_owned"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["organization_id"], name: "index_organization_stakeholders_on_organization_id"
    t.index ["stakeholder_id"], name: "index_organization_stakeholders_on_stakeholder_id"
    t.index ["stakeholder_rank_id"], name: "index_organization_stakeholders_on_stakeholder_rank_id"
  end

  create_table "organizations", force: :cascade do |t|
    t.string "org_name"
    t.string "org_address"
    t.string "org_city"
    t.bigint "state_id"
    t.integer "org_zip"
    t.string "org_website"
    t.integer "full_emp"
    t.integer "part_emp"
    t.integer "founding_year"
    t.bigint "ownership_type_id"
    t.integer "prim_naics"
    t.integer "sec_naics"
    t.text "brief_desc_of_services"
    t.bigint "real_estate_type_id"
    t.boolean "real_estate_included"
    t.float "futr_rent"
    t.date "lease_expir_date"
    t.date "extend_date"
    t.bigint "hist_earning_ranking_id"
    t.bigint "finance_rec_ranking_id"
    t.bigint "depth_of_management_ranking_id"
    t.bigint "loc_and_fac_ranking_id"
    t.bigint "competition_ranking_id"
    t.bigint "diversification_ranking_id"
    t.bigint "equipment_sys_ranking_id"
    t.bigint "employee_skill_turnover_ranking_id"
    t.bigint "desirability_ranking_id"
    t.bigint "hist_org_performance_ranking_id"
    t.bigint "org_status_id"
    t.text "org_comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["competition_ranking_id"], name: "index_organizations_on_competition_ranking_id"
    t.index ["depth_of_management_ranking_id"], name: "index_organizations_on_depth_of_management_ranking_id"
    t.index ["desirability_ranking_id"], name: "index_organizations_on_desirability_ranking_id"
    t.index ["diversification_ranking_id"], name: "index_organizations_on_diversification_ranking_id"
    t.index ["employee_skill_turnover_ranking_id"], name: "index_organizations_on_employee_skill_turnover_ranking_id"
    t.index ["equipment_sys_ranking_id"], name: "index_organizations_on_equipment_sys_ranking_id"
    t.index ["finance_rec_ranking_id"], name: "index_organizations_on_finance_rec_ranking_id"
    t.index ["hist_earning_ranking_id"], name: "index_organizations_on_hist_earning_ranking_id"
    t.index ["hist_org_performance_ranking_id"], name: "index_organizations_on_hist_org_performance_ranking_id"
    t.index ["loc_and_fac_ranking_id"], name: "index_organizations_on_loc_and_fac_ranking_id"
    t.index ["org_status_id"], name: "index_organizations_on_org_status_id"
    t.index ["ownership_type_id"], name: "index_organizations_on_ownership_type_id"
    t.index ["real_estate_type_id"], name: "index_organizations_on_real_estate_type_id"
    t.index ["state_id"], name: "index_organizations_on_state_id"
    t.index ["user_id"], name: "index_organizations_on_user_id"
  end

  create_table "ownership_types", force: :cascade do |t|
    t.string "ownership_type_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "real_estate_types", force: :cascade do |t|
    t.string "real_estate_type_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "stakeholder_ranks", force: :cascade do |t|
    t.string "stakeholder_rank_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "stakeholder_statuses", force: :cascade do |t|
    t.string "stakeholder_status_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "stakeholders", force: :cascade do |t|
    t.bigint "stakeholder_status_id"
    t.string "stakeholder_fname"
    t.string "stakeholder_lname"
    t.string "stakeholder_phone"
    t.string "stakeholder_email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["stakeholder_status_id"], name: "index_stakeholders_on_stakeholder_status_id"
  end

  create_table "states", force: :cascade do |t|
    t.string "state_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_roles", force: :cascade do |t|
    t.string "user_role_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer "invitation_limit"
    t.string "invited_by_type"
    t.bigint "invited_by_id"
    t.integer "invitations_count", default: 0
    t.bigint "user_role_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["invitation_token"], name: "index_users_on_invitation_token", unique: true
    t.index ["invitations_count"], name: "index_users_on_invitations_count"
    t.index ["invited_by_id"], name: "index_users_on_invited_by_id"
    t.index ["invited_by_type", "invited_by_id"], name: "index_users_on_invited_by_type_and_invited_by_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["user_role_id"], name: "index_users_on_user_role_id"
  end

  add_foreign_key "appraisal_calcs", "appraisal_calc_statuses"
  add_foreign_key "appraisal_calcs", "business_types"
  add_foreign_key "appraisal_calcs", "facility_types"
  add_foreign_key "appraisal_calcs", "organizations"
  add_foreign_key "balance_sheets", "accounting_methods"
  add_foreign_key "balance_sheets", "balance_sheet_statuses"
  add_foreign_key "balance_sheets", "financial_source_types"
  add_foreign_key "balance_sheets", "organizations"
  add_foreign_key "income_statements", "income_stmt_statuses"
  add_foreign_key "income_statements", "organizations"
  add_foreign_key "organization_stakeholders", "organizations"
  add_foreign_key "organization_stakeholders", "stakeholder_ranks"
  add_foreign_key "organization_stakeholders", "stakeholders"
  add_foreign_key "organizations", "competition_rankings"
  add_foreign_key "organizations", "depth_of_management_rankings"
  add_foreign_key "organizations", "desirability_rankings"
  add_foreign_key "organizations", "diversification_rankings"
  add_foreign_key "organizations", "employee_skill_turnover_rankings"
  add_foreign_key "organizations", "equipment_sys_rankings"
  add_foreign_key "organizations", "finance_rec_rankings"
  add_foreign_key "organizations", "hist_earning_rankings"
  add_foreign_key "organizations", "hist_org_performance_rankings"
  add_foreign_key "organizations", "loc_and_fac_rankings"
  add_foreign_key "organizations", "org_statuses"
  add_foreign_key "organizations", "ownership_types"
  add_foreign_key "organizations", "real_estate_types"
  add_foreign_key "organizations", "states"
  add_foreign_key "organizations", "users"
  add_foreign_key "stakeholders", "stakeholder_statuses"
  add_foreign_key "users", "user_roles"
end
