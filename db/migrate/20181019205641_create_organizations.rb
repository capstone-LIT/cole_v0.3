class CreateOrganizations < ActiveRecord::Migration[5.2]
  def change
    create_table :organizations do |t|
      t.string :org_name
      t.string :org_address
      t.string :org_city
      t.references :state, foreign_key: true
      t.integer :org_zip
      t.string :org_website
      t.integer :full_emp
      t.integer :part_emp
      t.integer :founding_year
      t.references :ownership_type, foreign_key: true
      t.integer :prim_naics
      t.integer :sec_naics
      t.text :brief_desc_of_services
      t.references :real_estate_type, foreign_key: true
      t.boolean :real_estate_included
      t.float :futr_rent
      t.date :lease_expir_date
      t.date :extend_date
      t.references :hist_earning_ranking, foreign_key: true
      t.references :finance_rec_ranking, foreign_key: true
      t.references :depth_of_management_ranking, foreign_key: true
      t.references :loc_and_fac_ranking, foreign_key: true
      t.references :competition_ranking, foreign_key: true
      t.references :diversification_ranking, foreign_key: true
      t.references :equipment_sys_ranking, foreign_key: true
      t.references :employee_skill_turnover_ranking, foreign_key: true
      t.references :desirability_ranking, foreign_key: true
      t.references :hist_org_performance_ranking, foreign_key: true
      t.references :org_status, foreign_key: true
      t.text :org_comment

      t.timestamps
    end
  end
end
