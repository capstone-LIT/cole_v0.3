class AppraisalCalc < ApplicationRecord
  belongs_to :organization
  belongs_to :business_type
  belongs_to :facility_type
  belongs_to :appraisal_calc_status
  has_many :income_statements, through: :organizations
  has_many :balance_sheets, through: :organizations

  public
    def get_balance_sheet
      sheets = self.organization.balance_sheets
      find_year = self.report_date.year - 1
      sheets.each do |s|
        if s.sheet_date.year == find_year && s.balance_sheet_status != 3
          return s
        end
      end
      BalanceSheet.find_by_id(1)
    end
    def get_last_statement
      stmts = self.organization.income_statements
      find_year = self.report_date.year - 1
      stmts.each do |s|
        if s.stmt_year == find_year && s.income_stmt_status != 3
          return s
        end
      end
      nil
    end
  def get_middle_statement
    stmts = self.organization.income_statements
    find_year = self.report_date.year - 2
    stmts.each do |s|
      if s.stmt_year == find_year && s.income_stmt_status != 3
        return s
      end
    end
    self.get_last_statement
  end
  def get_earliest_statement
    stmts = self.organization.income_statements
    find_year = self.report_date.year - 3
    stmts.each do |s|
      if s.stmt_year == find_year && s.income_stmt_status != 3
        return s
      end
    end
    self.get_middle_statement
  end
  def get_partial_statement
    stmts = self.organization.income_statements
    find_year = self.report_date.year - 4
    stmts.each do |s|
      if s.stmt_year == find_year && s.income_stmt_status != 3
        return s
      end
    end
    self.get_earliest_statement
  end
  def get_early_growth
    ( self.get_earliest_statement.total_sales - self.get_partial_statement.total_sales ) / self.get_partial_statement.total_sales
  end
  def get_middle_growth
    ( self.get_middle_statement.total_sales - self.get_earliest_statement.total_sales ) / self.get_earliest_statement.total_sales
  end
  def get_last_growth
    ( self.get_last_statement.total_sales - self.get_middle_statement.total_sales ) / self.get_middle_statement.total_sales
  end

  def get_adjusted_net_value_of_business
    self.get_balance_sheet.total_asset + self.get_balance_sheet.total_asset_adjustment + self.get_balance_sheet.total_liability + self.get_balance_sheet.total_liabilities_adjustment
  end

  def get_year_growth(year1, year2)
    ( year2 - year1 ) / year1
  end

  def get_projected_earnings
    self.get_last_statement.calc_discretionary_earnings * (self.get_last_statement.est_growth_rate + 1)
  end

  def get_early_extension
    self.get_earliest_statement.calc_discretionary_earnings * self.year_one_conf
  end

  def get_middle_extension
    self.get_middle_statement.calc_discretionary_earnings * self.year_two_conf
  end

  def get_last_extension
    self.get_last_statement.calc_discretionary_earnings * self.year_three_conf
  end

  def get_projected_extension
    self.get_projected_earnings * self.projected_conf
  end

  def get_aggregate_weighted_earnings
    self.get_early_extension + self.get_middle_extension + self.get_last_extension + self.get_projected_extension
  end

  def get_historical_earnings_rating
    self.organization.hist_earning_ranking.id * 10
  end

  def get_financial_records_rating
    self.organization.finance_rec_ranking.id * 9
  end

  def get_depth_management_rating
    self.organization.depth_of_management_ranking.id * 8
  end

  def get_industry_performance_rating
    self.organization.hist_org_performance_ranking.id * 7
  end

  def get_location_and_facilities_rating
    self.organization.loc_and_fac_ranking.id * 6
  end

  def get_competition_rating
    self.organization.competition_ranking.id * 5
  end

  def get_diversification_rating
    self.organization.diversification_ranking.id * 4
  end

  def get_stability_skills_rating
    self.organization.employee_skill_turnover_ranking.id * 3
  end

  def get_equipment_systems_rating
    self.organization.equipment_sys_ranking.id * 2
  end

  def get_desirability_marketability_rating
    self.organization.desirability_ranking.id * 1
  end

  def get_total_factor_ratings
    self.get_historical_earnings_rating + self.get_financial_records_rating + self.get_depth_management_rating + self.get_industry_performance_rating +
        self.get_location_and_facilities_rating + self.get_competition_rating + self.get_diversification_rating + self.get_stability_skills_rating +
        self.get_equipment_systems_rating + self.get_desirability_marketability_rating
  end

  def get_valuation_multiple
    fl = 500.1
    fl = (self.get_total_factor_ratings/ 55.0)
    return fl.round(5)
  end

  def get_inverted_valuation_multiple
    1 / self.get_valuation_multiple
  end

  def get_selected_earnings_over_cap_rate
    self.get_aggregate_weighted_earnings / self.get_inverted_valuation_multiple
  end

  def get_market_data_revenue
    self.get_last_statement.total_sales * self.mult_of_rev
  end

  def get_market_data_earnings
    self.get_aggregate_weighted_earnings * self.mult_of_sde
  end

  def get_market_data_value
    (self.get_market_data_earnings * self.est_value_sde_conf) + (self.get_market_data_revenue * (self.est_val_rev_conf))
  end

  def asset_approach_extension
    self.get_adjusted_net_value_of_business * self.asset_approach_conf
  end

  def market_approach_extension
    self.get_market_data_value * self.market_approach_conf
  end

  def income_approach_extension
    self.get_selected_earnings_over_cap_rate * self.income_approach_conf
  end

  def reconciliation_of_values
    asset_approach_extension + market_approach_extension + income_approach_extension
  end

 # Pricing and Deals structure
  def  get_buyer_down_payment(price)
    price * buyer_down_pmt
end

  def get_assumption_of_debt(price)
    price * debt_assum
end

def    get_seller_financing(price)
  price * seller_finance
end

  def  get_comm_finance(price)
    price * comm_finance
   end

   def  get_cont_earn_out(price)
     price * cont_earn_out
    end


#Ability to Pay

    def financed_amount(price)
      price - get_buyer_down_payment(price)
end


  $interest_rate = 0.0775

  def  annual_debt_service(price)
    pmt(( $interest_rate / 12 ), (term * 12),  financed_amount(price), 0, 0) * 12
  end

    def debt_service_cushion(price)
      annual_debt_service(price) * 0.25
    end

    def remaining_earnings(price)
       get_aggregate_weighted_earnings + annual_debt_service(price) + debt_service_cushion(price) - capt_expend_est - new_owner_sal
     end

    def cash_return_on_purch_price(price)
      (remaining_earnings(price))  /  total_asset
    end

    def  cash_return_on_down_payment(price)
      (remaining_earnings(price))  /  down_payment
    end

    def sale_price_sde
      total_asset / get_aggregate_weighted_earnings
    end

  #Seller note
def seller_note_payment(price) # this is also seller estimated monthly payment
  pmt($interest_rate/12,60, get_seller_financing(price))
end

#Buyer vs seller view of transaction
  def assumed_purch_price # does not change & is the same between buyers and sellers
    total_asset
  end


def buyer_int_income(price)
 -annual_debt_service(price) * term - financed_amount(price)
end

def seller_int_income(price)
 -(get_seller_financing(price) + (seller_note_payment(price) * 60))
end

def buyer_working_capital
  self.get_balance_sheet.cash_req
end

def seller_working_capital
self.get_balance_sheet.cash_on_hand
end

def est_loan_fees(price) #only for buyers
  get_comm_finance(price) * est_loan_fee
end

def ar_retained #only for sellers
  self.get_balance_sheet.accounts_r
end

def excess_def_inventory # this is the same for buyers and sellers
  self.get_balance_sheet.inventory -   self.get_balance_sheet.inv_req
end

def liabilities_at_closing #only for sellers
  self.get_balance_sheet.accounts_p -   self.get_balance_sheet.bank_debt
end

def buyer_monthly_payment(price)
-annual_debt_service(price)/12
end

def buyer_total_cost(price)
  price + buyer_int_income(price) + buyer_working_capital + est_loan_fees(price) + excess_def_inventory
end

def seller_total_cost(price)
price + seller_int_income(price) + seller_working_capital + ar_retained + excess_def_inventory + liabilities_at_closing
end

def buyer_cost_pro_me(price)
  buyer_total_cost(price) / get_aggregate_weighted_earnings
end

def seller_cost_pro_me(price)
  seller_total_cost(price) / get_aggregate_weighted_earnings

end



    def pmt(rate, nper, pv, fv=0, type=0)
      ((-pv * pvif(rate, nper) - fv ) / ((1.0 + rate * type) * fvifa(rate, nper)))
    end

    def ipmt(rate, per, nper, pv, fv=0, type=0)
      p = pmt(rate, nper, pv, fv, 0);
      ip = -(pv * pow1p(rate, per - 1) * rate + p * pow1pm1(rate, per - 1))
      (type == 0) ? ip : ip / (1 + rate)
    end

    def ppmt(rate, per, nper, pv, fv=0, type=0)
     p = pmt(rate, nper, pv, fv, type)
     ip = ipmt(rate, per, nper, pv, fv, type)
     p - ip
    end

    protected

    def pow1pm1(x, y)
      (x <= -1) ? ((1 + x) ** y) - 1 : Math.exp(y * Math.log(1.0 + x)) - 1
    end

    def pow1p(x, y)
      (x.abs > 0.5) ? ((1 + x) ** y) : Math.exp(y * Math.log(1.0 + x))
    end

    def pvif(rate, nper)
      pow1p(rate, nper)
    end

    def fvifa(rate, nper)
      (rate == 0) ? nper : pow1pm1(rate, nper) / rate
    end


end

