class CreateOwnershipTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :ownership_types do |t|
      t.string :ownership_type_desc

      t.timestamps
    end
  end
end
