json.extract! hist_org_performance_ranking, :id, :hist_org_performance_ranking_desc, :created_at, :updated_at
json.url hist_org_performance_ranking_url(hist_org_performance_ranking, format: :json)
