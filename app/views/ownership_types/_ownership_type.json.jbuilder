json.extract! ownership_type, :id, :ownership_type_desc, :created_at, :updated_at
json.url ownership_type_url(ownership_type, format: :json)
