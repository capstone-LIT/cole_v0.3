require 'test_helper'

class IncomeStmtStatusesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @income_stmt_status = income_stmt_statuses(:one)
  end

  test "should get index" do
    get income_stmt_statuses_url
    assert_response :success
  end

  test "should get new" do
    get new_income_stmt_status_url
    assert_response :success
  end

  test "should create income_stmt_status" do
    assert_difference('IncomeStmtStatus.count') do
      post income_stmt_statuses_url, params: { income_stmt_status: { stmt_status_desc: @income_stmt_status.stmt_status_desc } }
    end

    assert_redirected_to income_stmt_status_url(IncomeStmtStatus.last)
  end

  test "should show income_stmt_status" do
    get income_stmt_status_url(@income_stmt_status)
    assert_response :success
  end

  test "should get edit" do
    get edit_income_stmt_status_url(@income_stmt_status)
    assert_response :success
  end

  test "should update income_stmt_status" do
    patch income_stmt_status_url(@income_stmt_status), params: { income_stmt_status: { stmt_status_desc: @income_stmt_status.stmt_status_desc } }
    assert_redirected_to income_stmt_status_url(@income_stmt_status)
  end

  test "should destroy income_stmt_status" do
    assert_difference('IncomeStmtStatus.count', -1) do
      delete income_stmt_status_url(@income_stmt_status)
    end

    assert_redirected_to income_stmt_statuses_url
  end
end
