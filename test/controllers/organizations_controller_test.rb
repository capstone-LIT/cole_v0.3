require 'test_helper'

class OrganizationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @organization = organizations(:one)
  end

  test "should get index" do
    get organizations_url
    assert_response :success
  end

  test "should get new" do
    get new_organization_url
    assert_response :success
  end

  test "should create organization" do
    assert_difference('Organization.count') do
      post organizations_url, params: { organization: { brief_desc_of_services: @organization.brief_desc_of_services, competition_ranking_id: @organization.competition_ranking_id, depth_of_management_ranking_id: @organization.depth_of_management_ranking_id, desirability_ranking_id: @organization.desirability_ranking_id, diversification_ranking_id: @organization.diversification_ranking_id, employee_skill_turnover_ranking_id: @organization.employee_skill_turnover_ranking_id, equipment_sys_ranking_id: @organization.equipment_sys_ranking_id, extend_date: @organization.extend_date, finance_rec_ranking_id: @organization.finance_rec_ranking_id, founding_year: @organization.founding_year, full_emp: @organization.full_emp, futr_rent: @organization.futr_rent, hist_earning_ranking_id: @organization.hist_earning_ranking_id, hist_org_performance_ranking_id: @organization.hist_org_performance_ranking_id, lease_expir_date: @organization.lease_expir_date, loc_and_fac_ranking_id: @organization.loc_and_fac_ranking_id, org_address: @organization.org_address, org_city: @organization.org_city, org_comment: @organization.org_comment, org_name: @organization.org_name, org_website: @organization.org_website, org_zip: @organization.org_zip, ownership_type_id: @organization.ownership_type_id, part_emp: @organization.part_emp, prim_naics: @organization.prim_naics, real_estate_included: @organization.real_estate_included, real_estate_type_id: @organization.real_estate_type_id, sec_naics: @organization.sec_naics, state_id: @organization.state_id } }
    end

    assert_redirected_to organization_url(Organization.last)
  end

  test "should show organization" do
    get organization_url(@organization)
    assert_response :success
  end

  test "should get edit" do
    get edit_organization_url(@organization)
    assert_response :success
  end

  test "should update organization" do
    patch organization_url(@organization), params: { organization: { brief_desc_of_services: @organization.brief_desc_of_services, competition_ranking_id: @organization.competition_ranking_id, depth_of_management_ranking_id: @organization.depth_of_management_ranking_id, desirability_ranking_id: @organization.desirability_ranking_id, diversification_ranking_id: @organization.diversification_ranking_id, employee_skill_turnover_ranking_id: @organization.employee_skill_turnover_ranking_id, equipment_sys_ranking_id: @organization.equipment_sys_ranking_id, extend_date: @organization.extend_date, finance_rec_ranking_id: @organization.finance_rec_ranking_id, founding_year: @organization.founding_year, full_emp: @organization.full_emp, futr_rent: @organization.futr_rent, hist_earning_ranking_id: @organization.hist_earning_ranking_id, hist_org_performance_ranking_id: @organization.hist_org_performance_ranking_id, lease_expir_date: @organization.lease_expir_date, loc_and_fac_ranking_id: @organization.loc_and_fac_ranking_id, org_address: @organization.org_address, org_city: @organization.org_city, org_comment: @organization.org_comment, org_name: @organization.org_name, org_website: @organization.org_website, org_zip: @organization.org_zip, ownership_type_id: @organization.ownership_type_id, part_emp: @organization.part_emp, prim_naics: @organization.prim_naics, real_estate_included: @organization.real_estate_included, real_estate_type_id: @organization.real_estate_type_id, sec_naics: @organization.sec_naics, state_id: @organization.state_id } }
    assert_redirected_to organization_url(@organization)
  end

  test "should destroy organization" do
    assert_difference('Organization.count', -1) do
      delete organization_url(@organization)
    end

    assert_redirected_to organizations_url
  end
end
