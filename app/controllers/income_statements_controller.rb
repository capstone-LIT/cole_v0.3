class IncomeStatementsController < ApplicationController
  before_action :set_income_statement, only: [:show, :edit, :update, :destroy]

  # GET /income_statements
  # GET /income_statements.json
  def index
    if !current_user.is_broker?
      i = Organization.find_by_user_id(current_user.id).id
      @income_statements = IncomeStatement.where(organization_id: i)
    else
    @income_statements = IncomeStatement.all
      end
  end

  # GET /income_statements/1
  # GET /income_statements/1.json
  def show
    if current_user.is_broker? || @income_statement.organization == current_user.get_org
    else
      redirect_to 'client_home'
    end
  end

  # GET /income_statements/new
  def new
    @income_statement = IncomeStatement.new
  end

  # GET /income_statements/1/edit
  def edit
    if current_user.is_broker? || @income_statement.organization == current_user.get_org
    else
      redirect_to 'client_home'
      end
  end

  # POST /income_statements
  # POST /income_statements.json
  def create
    @income_statement = IncomeStatement.new(income_statement_params)    
    @income_statement.organization_id = current_user.get_org.id
    @income_statement.income_stmt_status_id = 3
    respond_to do |format|
      if @income_statement.save
        if current_user.is_broker?
          format.html { redirect_to '/appraiser_home', notice: 'Income Statement was successfully created.' }
          format.json { render :show, status: :created, location: @balance_sheet }
        else
          format.html { redirect_to '/client_home', notice: 'Income statement was successfully created.' }
          format.json { render :show, status: :created, location: @balance_sheet }
        end
      else
        format.html { render :new }
        format.json { render json: @income_statement.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /income_statements/1
  # PATCH/PUT /income_statements/1.json
  def update
    if current_user.is_broker? || @income_statement.income_stmt_status_id != 1
    respond_to do |format|
      if @income_statement.update(income_statement_params)
        if current_user.is_broker?
          format.html { redirect_to '/appraiser_home' }
          format.json { render :show, status: :created, location: @balance_sheet }
        else
          format.html { redirect_to '/client_home' }
          format.json { render :show, status: :created, location: @balance_sheet }
        end
      else
        format.html { render :edit }
        format.json { render json: @income_statement.errors, status: :unprocessable_entity }
      end
    end
    else
      redirect_to '/client_home'
    end

  end

  # DELETE /income_statements/1
  # DELETE /income_statements/1.json
  def destroy
    if current_user.is_broker?
    @income_statement.income_stmt_status_id = 3
    respond_to do |format|
      format.html { redirect_to income_statements_url, notice: 'Income statement was successfully destroyed.' }
      format.json { head :no_content }
    end
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_income_statement
      @income_statement = IncomeStatement.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def income_statement_params
      params.require(:income_statement).permit(:stmt_year, :total_sales, :pretax_profit, :partner_comp, :dep_an_am,
                                               :interest_exp, :non_rec_exp, :own_veh_exp, :own_insurance_exp,
                                               :income_stmt_status_id, :owners_travel_exp, :sal_adj, :own_retirement,
                                               :cost_of_goods, :organization_id, :payroll, :maintenance, :bad_debt,
                                               :rent, :tax_and_lic, :advertising, :pension_profit_share,
                                               :emp_benefit_cost, :bank_cc_fee, :insurance_cost, :legal_acct_exp,
                                               :outside_exp, :utility, :veh_exp, :est_growth_rate, :stmt_comment,
                                               :int_month, :total_sales_curr, :total_sales_prev, :pretax_profit_curr,
                                               :pretax_profit_prev)
    end
end
