class CreateOrganizationStakeholders < ActiveRecord::Migration[5.2]
  def change
    create_table :organization_stakeholders do |t|
      t.references :organization, foreign_key: true
      t.references :stakeholder, foreign_key: true
      t.references :stakeholder_rank, foreign_key: true
      t.float :percent_owned

      t.timestamps
    end
  end
end
