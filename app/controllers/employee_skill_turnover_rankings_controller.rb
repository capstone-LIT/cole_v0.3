class EmployeeSkillTurnoverRankingsController < ApplicationController
  before_action :set_employee_skill_turnover_ranking, only: [:show, :edit, :update, :destroy]

  # GET /employee_skill_turnover_rankings
  # GET /employee_skill_turnover_rankings.json
  def index
    @employee_skill_turnover_rankings = EmployeeSkillTurnoverRanking.all
  end

  # GET /employee_skill_turnover_rankings/1
  # GET /employee_skill_turnover_rankings/1.json
  def show
  end

  # GET /employee_skill_turnover_rankings/new
  def new
    @employee_skill_turnover_ranking = EmployeeSkillTurnoverRanking.new
  end

  # GET /employee_skill_turnover_rankings/1/edit
  def edit
  end

  # POST /employee_skill_turnover_rankings
  # POST /employee_skill_turnover_rankings.json
  def create
    @employee_skill_turnover_ranking = EmployeeSkillTurnoverRanking.new(employee_skill_turnover_ranking_params)

    respond_to do |format|
      if @employee_skill_turnover_ranking.save
        format.html { redirect_to @employee_skill_turnover_ranking, notice: 'Employee skill turnover ranking was successfully created.' }
        format.json { render :show, status: :created, location: @employee_skill_turnover_ranking }
      else
        format.html { render :new }
        format.json { render json: @employee_skill_turnover_ranking.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /employee_skill_turnover_rankings/1
  # PATCH/PUT /employee_skill_turnover_rankings/1.json
  def update
    respond_to do |format|
      if @employee_skill_turnover_ranking.update(employee_skill_turnover_ranking_params)
        format.html { redirect_to @employee_skill_turnover_ranking, notice: 'Employee skill turnover ranking was successfully updated.' }
        format.json { render :show, status: :ok, location: @employee_skill_turnover_ranking }
      else
        format.html { render :edit }
        format.json { render json: @employee_skill_turnover_ranking.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /employee_skill_turnover_rankings/1
  # DELETE /employee_skill_turnover_rankings/1.json
  def destroy
    @employee_skill_turnover_ranking.destroy
    respond_to do |format|
      format.html { redirect_to employee_skill_turnover_rankings_url, notice: 'Employee skill turnover ranking was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_employee_skill_turnover_ranking
      @employee_skill_turnover_ranking = EmployeeSkillTurnoverRanking.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def employee_skill_turnover_ranking_params
      params.require(:employee_skill_turnover_ranking).permit(:emp_skill_turn_desc)
    end
end
