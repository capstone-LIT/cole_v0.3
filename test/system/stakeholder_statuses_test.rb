require "application_system_test_case"

class StakeholderStatusesTest < ApplicationSystemTestCase
  setup do
    @stakeholder_status = stakeholder_statuses(:one)
  end

  test "visiting the index" do
    visit stakeholder_statuses_url
    assert_selector "h1", text: "Stakeholder Statuses"
  end

  test "creating a Stakeholder status" do
    visit stakeholder_statuses_url
    click_on "New Stakeholder Status"

    fill_in "Stakeholder Status Desc", with: @stakeholder_status.stakeholder_status_desc
    click_on "Create Stakeholder status"

    assert_text "Stakeholder status was successfully created"
    click_on "Back"
  end

  test "updating a Stakeholder status" do
    visit stakeholder_statuses_url
    click_on "Edit", match: :first

    fill_in "Stakeholder Status Desc", with: @stakeholder_status.stakeholder_status_desc
    click_on "Update Stakeholder status"

    assert_text "Stakeholder status was successfully updated"
    click_on "Back"
  end

  test "destroying a Stakeholder status" do
    visit stakeholder_statuses_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Stakeholder status was successfully destroyed"
  end
end
