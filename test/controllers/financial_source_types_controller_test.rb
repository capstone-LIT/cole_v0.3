require 'test_helper'

class FinancialSourceTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @financial_source_type = financial_source_types(:one)
  end

  test "should get index" do
    get financial_source_types_url
    assert_response :success
  end

  test "should get new" do
    get new_financial_source_type_url
    assert_response :success
  end

  test "should create financial_source_type" do
    assert_difference('FinancialSourceType.count') do
      post financial_source_types_url, params: { financial_source_type: { financial_source_type_desc: @financial_source_type.financial_source_type_desc } }
    end

    assert_redirected_to financial_source_type_url(FinancialSourceType.last)
  end

  test "should show financial_source_type" do
    get financial_source_type_url(@financial_source_type)
    assert_response :success
  end

  test "should get edit" do
    get edit_financial_source_type_url(@financial_source_type)
    assert_response :success
  end

  test "should update financial_source_type" do
    patch financial_source_type_url(@financial_source_type), params: { financial_source_type: { financial_source_type_desc: @financial_source_type.financial_source_type_desc } }
    assert_redirected_to financial_source_type_url(@financial_source_type)
  end

  test "should destroy financial_source_type" do
    assert_difference('FinancialSourceType.count', -1) do
      delete financial_source_type_url(@financial_source_type)
    end

    assert_redirected_to financial_source_types_url
  end
end
