class CreateFinancialSourceTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :financial_source_types do |t|
      t.string :financial_source_type_desc

      t.timestamps
    end
  end
end
