require 'test_helper'

class DesirabilityRankingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @desirability_ranking = desirability_rankings(:one)
  end

  test "should get index" do
    get desirability_rankings_url
    assert_response :success
  end

  test "should get new" do
    get new_desirability_ranking_url
    assert_response :success
  end

  test "should create desirability_ranking" do
    assert_difference('DesirabilityRanking.count') do
      post desirability_rankings_url, params: { desirability_ranking: { desirability_desc: @desirability_ranking.desirability_desc } }
    end

    assert_redirected_to desirability_ranking_url(DesirabilityRanking.last)
  end

  test "should show desirability_ranking" do
    get desirability_ranking_url(@desirability_ranking)
    assert_response :success
  end

  test "should get edit" do
    get edit_desirability_ranking_url(@desirability_ranking)
    assert_response :success
  end

  test "should update desirability_ranking" do
    patch desirability_ranking_url(@desirability_ranking), params: { desirability_ranking: { desirability_desc: @desirability_ranking.desirability_desc } }
    assert_redirected_to desirability_ranking_url(@desirability_ranking)
  end

  test "should destroy desirability_ranking" do
    assert_difference('DesirabilityRanking.count', -1) do
      delete desirability_ranking_url(@desirability_ranking)
    end

    assert_redirected_to desirability_rankings_url
  end
end
