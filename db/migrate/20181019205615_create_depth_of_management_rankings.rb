class CreateDepthOfManagementRankings < ActiveRecord::Migration[5.2]
  def change
    create_table :depth_of_management_rankings do |t|
      t.string :depth_of_mgmt_desc

      t.timestamps
    end
  end
end
