json.extract! organization_stakeholder, :id, :organization_id, :stakeholder_id, :stakeholder_rank_id, :percent_owned, :created_at, :updated_at
json.url organization_stakeholder_url(organization_stakeholder, format: :json)
