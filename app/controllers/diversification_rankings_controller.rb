class DiversificationRankingsController < ApplicationController
  before_action :set_diversification_ranking, only: [:show, :edit, :update, :destroy]

  # GET /diversification_rankings
  # GET /diversification_rankings.json
  def index
    @diversification_rankings = DiversificationRanking.all
  end

  # GET /diversification_rankings/1
  # GET /diversification_rankings/1.json
  def show
  end

  # GET /diversification_rankings/new
  def new
    @diversification_ranking = DiversificationRanking.new
  end

  # GET /diversification_rankings/1/edit
  def edit
  end

  # POST /diversification_rankings
  # POST /diversification_rankings.json
  def create
    @diversification_ranking = DiversificationRanking.new(diversification_ranking_params)

    respond_to do |format|
      if @diversification_ranking.save
        format.html { redirect_to @diversification_ranking, notice: 'Diversification ranking was successfully created.' }
        format.json { render :show, status: :created, location: @diversification_ranking }
      else
        format.html { render :new }
        format.json { render json: @diversification_ranking.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /diversification_rankings/1
  # PATCH/PUT /diversification_rankings/1.json
  def update
    respond_to do |format|
      if @diversification_ranking.update(diversification_ranking_params)
        format.html { redirect_to @diversification_ranking, notice: 'Diversification ranking was successfully updated.' }
        format.json { render :show, status: :ok, location: @diversification_ranking }
      else
        format.html { render :edit }
        format.json { render json: @diversification_ranking.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /diversification_rankings/1
  # DELETE /diversification_rankings/1.json
  def destroy
    @diversification_ranking.destroy
    respond_to do |format|
      format.html { redirect_to diversification_rankings_url, notice: 'Diversification ranking was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_diversification_ranking
      @diversification_ranking = DiversificationRanking.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def diversification_ranking_params
      params.require(:diversification_ranking).permit(:diversification_desc)
    end
end
