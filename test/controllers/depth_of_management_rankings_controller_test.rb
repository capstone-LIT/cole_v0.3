require 'test_helper'

class DepthOfManagementRankingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @depth_of_management_ranking = depth_of_management_rankings(:one)
  end

  test "should get index" do
    get depth_of_management_rankings_url
    assert_response :success
  end

  test "should get new" do
    get new_depth_of_management_ranking_url
    assert_response :success
  end

  test "should create depth_of_management_ranking" do
    assert_difference('DepthOfManagementRanking.count') do
      post depth_of_management_rankings_url, params: { depth_of_management_ranking: { depth_of_mgmt_desc: @depth_of_management_ranking.depth_of_mgmt_desc } }
    end

    assert_redirected_to depth_of_management_ranking_url(DepthOfManagementRanking.last)
  end

  test "should show depth_of_management_ranking" do
    get depth_of_management_ranking_url(@depth_of_management_ranking)
    assert_response :success
  end

  test "should get edit" do
    get edit_depth_of_management_ranking_url(@depth_of_management_ranking)
    assert_response :success
  end

  test "should update depth_of_management_ranking" do
    patch depth_of_management_ranking_url(@depth_of_management_ranking), params: { depth_of_management_ranking: { depth_of_mgmt_desc: @depth_of_management_ranking.depth_of_mgmt_desc } }
    assert_redirected_to depth_of_management_ranking_url(@depth_of_management_ranking)
  end

  test "should destroy depth_of_management_ranking" do
    assert_difference('DepthOfManagementRanking.count', -1) do
      delete depth_of_management_ranking_url(@depth_of_management_ranking)
    end

    assert_redirected_to depth_of_management_rankings_url
  end
end
