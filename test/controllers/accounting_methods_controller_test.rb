require 'test_helper'

class AccountingMethodsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @accounting_method = accounting_methods(:one)
  end

  test "should get index" do
    get accounting_methods_url
    assert_response :success
  end

  test "should get new" do
    get new_accounting_method_url
    assert_response :success
  end

  test "should create accounting_method" do
    assert_difference('AccountingMethod.count') do
      post accounting_methods_url, params: { accounting_method: { accounting_method_desc: @accounting_method.accounting_method_desc } }
    end

    assert_redirected_to accounting_method_url(AccountingMethod.last)
  end

  test "should show accounting_method" do
    get accounting_method_url(@accounting_method)
    assert_response :success
  end

  test "should get edit" do
    get edit_accounting_method_url(@accounting_method)
    assert_response :success
  end

  test "should update accounting_method" do
    patch accounting_method_url(@accounting_method), params: { accounting_method: { accounting_method_desc: @accounting_method.accounting_method_desc } }
    assert_redirected_to accounting_method_url(@accounting_method)
  end

  test "should destroy accounting_method" do
    assert_difference('AccountingMethod.count', -1) do
      delete accounting_method_url(@accounting_method)
    end

    assert_redirected_to accounting_methods_url
  end
end
