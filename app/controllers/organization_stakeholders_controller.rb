class OrganizationStakeholdersController < ApplicationController
  before_action :set_organization_stakeholder, only: [:show, :edit, :update, :destroy]

  # GET /organization_stakeholders
  # GET /organization_stakeholders.json
  def index
    @organization_stakeholders = OrganizationStakeholder.all
  end

  # GET /organization_stakeholders/1
  # GET /organization_stakeholders/1.json
  def show
  end

  # GET /organization_stakeholders/new
  def new
    @organization_stakeholder = OrganizationStakeholder.new
  end

  # GET /organization_stakeholders/1/edit
  def edit
  end

  # POST /organization_stakeholders
  # POST /organization_stakeholders.json
  def create
    @organization_stakeholder = OrganizationStakeholder.new(organization_stakeholder_params)

    respond_to do |format|
      if @organization_stakeholder.save
        format.html { redirect_to @organization_stakeholder, notice: 'Organization stakeholder was successfully created.' }
        format.json { render :show, status: :created, location: @organization_stakeholder }
      else
        format.html { render :new }
        format.json { render json: @organization_stakeholder.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /organization_stakeholders/1
  # PATCH/PUT /organization_stakeholders/1.json
  def update
    respond_to do |format|
      if @organization_stakeholder.update(organization_stakeholder_params)
        format.html { redirect_to @organization_stakeholder, notice: 'Organization stakeholder was successfully updated.' }
        format.json { render :show, status: :ok, location: @organization_stakeholder }
      else
        format.html { render :edit }
        format.json { render json: @organization_stakeholder.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /organization_stakeholders/1
  # DELETE /organization_stakeholders/1.json
  def destroy
    @organization_stakeholder.destroy
    respond_to do |format|
      format.html { redirect_to organization_stakeholders_url, notice: 'Organization stakeholder was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_organization_stakeholder
      @organization_stakeholder = OrganizationStakeholder.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def organization_stakeholder_params
      params.require(:organization_stakeholder).permit(:organization_id, :stakeholder_id, :stakeholder_rank_id, :percent_owned)
    end
end
