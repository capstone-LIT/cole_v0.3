require "application_system_test_case"

class FinanceRecRankingsTest < ApplicationSystemTestCase
  setup do
    @finance_rec_ranking = finance_rec_rankings(:one)
  end

  test "visiting the index" do
    visit finance_rec_rankings_url
    assert_selector "h1", text: "Finance Rec Rankings"
  end

  test "creating a Finance rec ranking" do
    visit finance_rec_rankings_url
    click_on "New Finance Rec Ranking"

    fill_in "Finance Rec Desc", with: @finance_rec_ranking.finance_rec_desc
    click_on "Create Finance rec ranking"

    assert_text "Finance rec ranking was successfully created"
    click_on "Back"
  end

  test "updating a Finance rec ranking" do
    visit finance_rec_rankings_url
    click_on "Edit", match: :first

    fill_in "Finance Rec Desc", with: @finance_rec_ranking.finance_rec_desc
    click_on "Update Finance rec ranking"

    assert_text "Finance rec ranking was successfully updated"
    click_on "Back"
  end

  test "destroying a Finance rec ranking" do
    visit finance_rec_rankings_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Finance rec ranking was successfully destroyed"
  end
end
