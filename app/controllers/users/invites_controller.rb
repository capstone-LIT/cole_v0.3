class Users::InvitesController < ApplicationController
  def create
    p 'sending invite'
    p current_user
    p ActionMailer::Base.default_url_options
    if User.find_by_email(invites_params[:email]).nil?
      User.invite!({:email => invites_params[:email]}, current_user)
    p 'sent invite'
    end
  end

  def invites_params
    params.require(:user).permit(:email)
  end
end
