AccountingMethod.create!([
  {accounting_method_desc: "Accrual"},
  {accounting_method_desc: "Cash Basis"}
])

AppraisalCalcStatus.create!([
  {calc_status_desc: "Completed"},
  {calc_status_desc: "Pending"},
  {calc_status_desc: "Archived"}
])

BalanceSheetStatus.create!([
  {sheet_desc: "Completed"},
  {sheet_desc: "Pending"},
  {sheet_desc: "Incomplete"}
])

BusinessType.create!([
  {business_type_desc: "Glass Company"},
  {business_type_desc: "Fast Food"},
  {business_type_desc: "Restaurant"},
  {business_type_desc: "Record Company"},
  {business_type_desc: "Lawn Care Company"},
  {business_type_desc: "Dry Cleaners"},
  {business_type_desc: "Software Company"},
  {business_type_desc: "Construction Company"},
  {business_type_desc: "Bank"},
  {business_type_desc: "Law Office"},
  {business_type_desc: "Electronics Store"},
  {business_type_desc: "Mattress Store"},
  {business_type_desc: "Pet Shop"},
  {business_type_desc: "Hair/Nail Salon"},
  {business_type_desc: "Auto Repairs"},
  {business_type_desc: "Liquor Store"},
  {business_type_desc: "Craft Store"}
])
CompetitionRanking.create!([
  {competition_desc: "Highly competitive or unstable markets."},
  {competition_desc: "Some competition but stable markets."},
  {competition_desc: "Few competitors in an industry with high start-up costs."}
])
DepthOfManagementRanking.create!([
  {depth_of_mgmt_desc: "No management other than owner that can run the business."},
  {depth_of_mgmt_desc: "Owner is involved but has experienced management that can manage the business during Owners absence."},
  {depth_of_mgmt_desc: "Owner works less than 30 hours per week and has multiple layers of professional staff and management."}
])
DesirabilityRanking.create!([
  {desirability_desc: "Highly technical expertise or license required. "},
  {desirability_desc: "Respectable/challenging business environment."},
  {desirability_desc: "A good business person could easily learn my business. "}
])
DiversificationRanking.create!([
  {diversification_desc: "Limited product lines, services or any customer representing more than 10% of annual revenue."},
  {diversification_desc: "Diversification of products, services and all customers are less than 10% of annual revenue."},
  {diversification_desc: "National and/or international markets and all customers are less than 5% of annual revenue."}
])
EmployeeSkillTurnoverRanking.create!([
  {emp_skill_turn_desc: "High turnover and/or unskilled employees."},
  {emp_skill_turn_desc: "Average to low turnover & well-trained."},
  {emp_skill_turn_desc: "Long-term, well-trained staff all under 60 years old."}
])
EquipmentSysRanking.create!([
  {eqp_sys_desc: "Some new equipment or equipment repair is needed, I'm excluding something from the sale or my information systems are dated."},
  {eqp_sys_desc: "All equipment and systems or operable and adequate for my current level of business and nothing is excluded."},
  {eqp_sys_desc: "Major equipment and systems have been recently upgraded and nothing is excluded."}
])
FacilityType.create!([
  {facility_type_desc: "Leased"},
  {facility_type_desc: "Owned"}
])
FinanceRecRanking.create!([
  {finance_rec_desc: "Tax returns and financial statements are prepared in-house or, modern financial software is not used in operations."},
  {finance_rec_desc: "Internal financial statements are created periodically and an accountant is used for the business tax returns."},
  {finance_rec_desc: "The tax returns match the internal P&L's, both of which are professionally prepared and tax returns are never extended."}
])
FinancialSourceType.create!([
  {financial_source_type_desc: "Tax Return"},
  {financial_source_type_desc: "P&L"}
])
HistEarningRanking.create!([
  {hist_earning_desc: "The business has been up and down over the past 5 years or there is less than a 5 year history."},
  {hist_earning_desc: "The business has been consistent the past 5 years with no declines in revenue of more than 2% from year to year."},
  {hist_earning_desc: "The business has grown in each of the last 5 years."}
])
HistOrgPerformanceRanking.create!([
  {hist_org_performance_ranking_desc: "The business has performed below industry in part or all of the last 5 years."},
  {hist_org_performance_ranking_desc: "The business has performed about the same as the industry the past 5 years."},
  {hist_org_performance_ranking_desc: "The business has performed above industry in each of the past 5 years."}
])

IncomeStmtStatus.create!([
  {stmt_status_desc: "Completed"},
  {stmt_status_desc: "Pending"},
  {stmt_status_desc: "Incomplete"}
])
LocAndFacRanking.create!([
  {loc_and_fac_desc: "Poor location, restrictive lease or questionable facilities."},
  {loc_and_fac_desc: "Average location, reasonable lease and well maintained facilities."},
  {loc_and_fac_desc: "Good location, fully transferrable lease and modern facilities."}
])
OrgStatus.create!([
  {status_desc: "Current"},
  {status_desc: "Archived"}
])

OwnershipType.create!([
  {ownership_type_desc: "S-Corp"},
  {ownership_type_desc: "C-Corp"},
  {ownership_type_desc: "LLC"},
  {ownership_type_desc: "Partnership"},
  {ownership_type_desc: "Sole Proprietorship"},
  {ownership_type_desc: "LLP"}
])
RealEstateType.create!([
  {real_estate_type_desc: "I have an arms length lease from a 3rd party that has no ownership in my business."},
  {real_estate_type_desc: "I own the real estate through another entity and lease it to myself."},
  {real_estate_type_desc: "My business is home-based."},
  {real_estate_type_desc: "My business owns the commercial real estate which is listed on my balance sheet."}
])

StakeholderRank.create!([
  {stakeholder_rank_desc: "1"},
  {stakeholder_rank_desc: "2"},
  {stakeholder_rank_desc: "3"},
  {stakeholder_rank_desc: "4"},
  {stakeholder_rank_desc: "5"},
  {stakeholder_rank_desc: "6"},
  {stakeholder_rank_desc: "7"},
  {stakeholder_rank_desc: "8"},
  {stakeholder_rank_desc: "9"},
  {stakeholder_rank_desc: "10"}
])
StakeholderStatus.create!([
  {stakeholder_status_desc: "Current"},
  {stakeholder_status_desc: "Archived"}
])
State.create!([
  {state_name: "Alabama"},
  {state_name: "Alaska"},
  {state_name: "Arizona"},
  {state_name: "Arkansas"},
  {state_name: "California"},
  {state_name: "Colorado"},
  {state_name: "Connecticut"},
  {state_name: "Delaware"},
  {state_name: "Florida"},
  {state_name: "Georgia"},
  {state_name: "Hawaii"},
  {state_name: "Idaho"},
  {state_name: "Illinois"},
  {state_name: "Indiana"},
  {state_name: "Iowa"},
  {state_name: "Kansas"},
  {state_name: "Kentucky"},
  {state_name: "Louisiana"},
  {state_name: "Maine"},
  {state_name: "Maryland"},
  {state_name: "Massachusetts"},
  {state_name: "Michigan"},
  {state_name: "Minnesota"},
  {state_name: "Mississippi"},
  {state_name: "Missouri"},
  {state_name: "Montana"},
  {state_name: "Nebraska"},
  {state_name: "Nevada"},
  {state_name: "New Hampshire"},
  {state_name: "New Jersey"},
  {state_name: "New Mexico"},
  {state_name: "New York"},
  {state_name: "North Carolina"},
  {state_name: "North Dakota"},
  {state_name: "Ohio"},
  {state_name: "Oklahoma"},
  {state_name: "Oregon"},
  {state_name: "Pennsylvania"},
  {state_name: "Rhode Island"},
  {state_name: "South Carolina"},
  {state_name: "South Dakota"},
  {state_name: "Tennessee"},
  {state_name: "Texas"},
  {state_name: "Utah"},
  {state_name: "Vermont"},
  {state_name: "Virginia"},
  {state_name: "Washington"},
  {state_name: "West Virginia"},
  {state_name: "Wisconsin"},
  {state_name: "Wyoming"},
  {state_name: "Washington DC"},
  {state_name: "Puerto Rico"}
])
UserRole.create!([
  {user_role_desc: "Admin"},
  {user_role_desc: "Broker"},
  {user_role_desc: "Client"}
])

User.create!([
                 {email: "johnnybravo@cartoon.net", password: "hubbahubba", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, invitation_token: nil, invitation_created_at: nil, invitation_sent_at: nil, invitation_accepted_at: nil, invitation_limit: nil, invited_by_type: nil, invited_by_id: nil, invitations_count: 0, user_role_id: 3},
                 {email: "bretdet@uh.edu", password: "brettydetty", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, invitation_token: nil, invitation_created_at: nil, invitation_sent_at: nil, invitation_accepted_at: nil, invitation_limit: nil, invited_by_type: nil, invited_by_id: nil, invitations_count: 0, user_role_id: 3},
                 {email: "admin@account.com", password: "adminpass", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, invitation_token: nil, invitation_created_at: nil, invitation_sent_at: nil, invitation_accepted_at: nil, invitation_limit: nil, invited_by_type: nil, invited_by_id: nil, invitations_count: 0, user_role_id: 1},
                 {email: "charlie@cole.com", password: "password", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, invitation_token: nil, invitation_created_at: nil, invitation_sent_at: nil, invitation_accepted_at: nil, invitation_limit: nil, invited_by_type: nil, invited_by_id: nil, invitations_count: 0, user_role_id: 2}
             ])

Stakeholder.create!([
                        {stakeholder_status_id: 1, stakeholder_fname: "John", stakeholder_lname: "Crane", stakeholder_phone: "555-555-5555", stakeholder_email: "jcrane@hotmail.com"},
                        {stakeholder_status_id: 1, stakeholder_fname: "Johnny", stakeholder_lname: "Bravo", stakeholder_phone: "534-634-5555", stakeholder_email: "jbravo@gmail.com"},
                        {stakeholder_status_id: 1, stakeholder_fname: "Bretford", stakeholder_lname: "Detford", stakeholder_phone: "555-783-5285", stakeholder_email: "bretdet@uh.com\n"}
                    ])

Organization.create!([
                         {org_name: "Johnny's Weight Shack", org_address: "12043 Swag Road", org_city: "Flex City", state_id: 3, org_zip: 99231, org_website: "www.GetYourPompOn.com", full_emp: 2, part_emp: 6, founding_year: 1995, ownership_type_id: 3, prim_naics: 555132, sec_naics: nil, brief_desc_of_services: "We take twigs and turn them into pigs", real_estate_type_id: 2, real_estate_included: true, futr_rent: 43425.0, lease_expir_date: "2020-10-09", extend_date: "2021-09-10", hist_earning_ranking_id: 3, finance_rec_ranking_id: 1, depth_of_management_ranking_id: 1, loc_and_fac_ranking_id: 3, competition_ranking_id: 2, diversification_ranking_id: 1, equipment_sys_ranking_id: 1, employee_skill_turnover_ranking_id: 1, desirability_ranking_id: 3, hist_org_performance_ranking_id: 2, org_status_id: 1, org_comment: "We are the best... hubba hubba", user_id: 1},
                         {org_name: "PMP Pimps", org_address: "55532 Main Street", org_city: "Houston", state_id: 43, org_zip: 77432, org_website: "www.pmpimps.net", full_emp: 4, part_emp: 0, founding_year: 2016, ownership_type_id: 2, prim_naics: 664514, sec_naics: nil, brief_desc_of_services: "We prepare people for the PMP exam.", real_estate_type_id: 4, real_estate_included: true, futr_rent: 23523.0, lease_expir_date: "2018-11-10", extend_date: "2019-11-10", hist_earning_ranking_id: 1, finance_rec_ranking_id: 1, depth_of_management_ranking_id: 1, loc_and_fac_ranking_id: 1, competition_ranking_id: 1, diversification_ranking_id: 1, equipment_sys_ranking_id: 1, employee_skill_turnover_ranking_id: 1, desirability_ranking_id: 1, hist_org_performance_ranking_id: 1, org_status_id: 1, org_comment: "We are the best around, if we can't get you to pass then you're an idiot.", user_id: 3}
                     ])

OrganizationStakeholder.create!([
                                    {organization_id: 1, stakeholder_id: 1, stakeholder_rank_id: 2, percent_owned: 5.0},
                                    {organization_id: 1, stakeholder_id: 2, stakeholder_rank_id: 1, percent_owned: 90.0},
                                    {organization_id: 2, stakeholder_id: 3, stakeholder_rank_id: 1, percent_owned: 100.0}
                                ])

IncomeStatement.create!([
                            {stmt_year: 2017, total_sales: nil, pretax_profit: nil, partner_comp: nil, dep_an_am: nil, interest_exp: nil, non_rec_exp: nil, own_veh_exp: nil, own_insurance_exp: nil, income_stmt_status_id: 3, owners_travel_exp: nil, sal_adj: nil, own_retirement: nil, cost_of_goods: nil, organization_id: 2, payroll: nil, maintenance: nil, bad_debt: nil, rent: nil, tax_and_lic: nil, advertising: nil, pension_profit_share: nil, emp_benefit_cost: nil, bank_cc_fee: nil, insurance_cost: nil, legal_acct_exp: nil, outside_exp: nil, utility: nil, veh_exp: nil, est_growth_rate: nil, int_month: nil, total_sales_prev: nil, pretax_profit_prev: nil, total_sales_curr: nil, pretax_profit_curr: nil, stmt_comment: ""},
                            {stmt_year: 2015, total_sales: 742523.0, pretax_profit: 301924.0, partner_comp: 142411.0, dep_an_am: 132.0, interest_exp: 315.0, non_rec_exp: 0.0, own_veh_exp: 4135.0, own_insurance_exp: 313.0, income_stmt_status_id: 1, owners_travel_exp: 15235.0, sal_adj: 0.0, own_retirement: 0.0, cost_of_goods: 315252.0, organization_id: 1, payroll: 98162.0, maintenance: 6271.0, bad_debt: 0.0, rent: 31526.0, tax_and_lic: 152.0, advertising: 12623.0, pension_profit_share: 0.0, emp_benefit_cost: 0.0, bank_cc_fee: 0.0, insurance_cost: 9535.0, legal_acct_exp: 0.0, outside_exp: 0.0, utility: 7261.0, veh_exp: 17734.0, est_growth_rate: 4.0, int_month: nil, total_sales_prev: 142516.0, pretax_profit_prev: 35612.0, total_sales_curr: 162516.0, pretax_profit_curr: 38513.0, stmt_comment: "I barely remember this year. Was too busy getting a gnarly pump on."},
                            {stmt_year: 2016, total_sales: 424543.0, pretax_profit: 185325.0, partner_comp: 90523.0, dep_an_am: 0.0, interest_exp: 526.0, non_rec_exp: 0.0, own_veh_exp: 0.0, own_insurance_exp: 0.0, income_stmt_status_id: 3, owners_travel_exp: 0.0, sal_adj: 0.0, own_retirement: 5000.0, cost_of_goods: 25324.0, organization_id: 2, payroll: 68000.0, maintenance: 0.0, bad_debt: 0.0, rent: 0.0, tax_and_lic: 0.0, advertising: 4000.0, pension_profit_share: 0.0, emp_benefit_cost: 0.0, bank_cc_fee: 0.0, insurance_cost: 0.0, legal_acct_exp: 0.0, outside_exp: 0.0, utility: 400.0, veh_exp: 0.0, est_growth_rate: 2.0, int_month: nil, total_sales_prev: 85325.0, pretax_profit_prev: 13554.0, total_sales_curr: 90523.0, pretax_profit_curr: 16390.0, stmt_comment: "Hello, we did good this year."},
                            {stmt_year: 2016, total_sales: 764524.0, pretax_profit: 321523.0, partner_comp: 16825.0, dep_an_am: 415.0, interest_exp: 532.0, non_rec_exp: 533.0, own_veh_exp: 6002.0, own_insurance_exp: 425.0, income_stmt_status_id: 1, owners_travel_exp: 12523.0, sal_adj: 25215.0, own_retirement: 0.0, cost_of_goods: 3729523.0, organization_id: 1, payroll: 120325.0, maintenance: 7125.0, bad_debt: 0.0, rent: 34152.0, tax_and_lic: 174.0, advertising: 15262.0, pension_profit_share: 0.0, emp_benefit_cost: 0.0, bank_cc_fee: 0.0, insurance_cost: 12522.0, legal_acct_exp: 3521.0, outside_exp: 0.0, utility: 0.0, veh_exp: 8925.0, est_growth_rate: 4.0, int_month: nil, total_sales_prev: 182516.0, pretax_profit_prev: 39523.0, total_sales_curr: 190533.0, pretax_profit_curr: 42616.0, stmt_comment: "I slightly remember this one."},
                            {stmt_year: 2017, total_sales: 725361.0, pretax_profit: 276463.0, partner_comp: 185352.0, dep_an_am: 1000.0, interest_exp: 6346.0, non_rec_exp: 0.0, own_veh_exp: 0.0, own_insurance_exp: 0.0, income_stmt_status_id: 1, owners_travel_exp: 0.0, sal_adj: 0.0, own_retirement: 0.0, cost_of_goods: 329533.0, organization_id: 1, payroll: 60523.0, maintenance: 8054.0, bad_debt: 242.0, rent: 32624.0, tax_and_lic: 143.0, advertising: 10634.0, pension_profit_share: 0.0, emp_benefit_cost: 0.0, bank_cc_fee: 0.0, insurance_cost: 14236.0, legal_acct_exp: 0.0, outside_exp: 36424.0, utility: 6232.0, veh_exp: 4632.0, est_growth_rate: 0.5, int_month: nil, total_sales_prev: 242532.0, pretax_profit_prev: 41253.0, total_sales_curr: 16375.0, pretax_profit_curr: 36536.0, stmt_comment: "AHHHHHHHHHHHHHHHHHHHHHHHHHHHH I'M SELLING!!!!"},
                            {stmt_year: 2014, total_sales: 742523.0, pretax_profit: nil, partner_comp: nil, dep_an_am: nil, interest_exp: nil, non_rec_exp: nil, own_veh_exp: nil, own_insurance_exp: nil, income_stmt_status_id: 2, owners_travel_exp: nil, sal_adj: nil, own_retirement: nil, cost_of_goods: nil, organization_id: 1, payroll: nil, maintenance: nil, bad_debt: nil, rent: nil, tax_and_lic: nil, advertising: nil, pension_profit_share: nil, emp_benefit_cost: nil, bank_cc_fee: nil, insurance_cost: nil, legal_acct_exp: nil, outside_exp: nil, utility: nil, veh_exp: nil, est_growth_rate: nil, int_month: nil, total_sales_prev: nil, pretax_profit_prev: nil, total_sales_curr: nil, pretax_profit_curr: nil, stmt_comment: ""}
                        ])

BalanceSheet.create!([
                         {organization_id: 2, financial_source_type_id: 2, sheet_date: "2018-07-08", accounting_method_id: 2, total_asset: 68236.0, cash_on_hand: 8234.0, accounts_r: 2351.0, inventory: 243.0, real_estate_value: 18532.0, ffe_v: 5325.0, accum_d: 0.0, accum_a: 0.0, other_asset: 33551.0, total_liability: 0.0, accounts_p: 0.0, bank_debt: 0.0, other_liability: 0.0, cash_req: 8436.0, inv_req: 3263.0, curr_ffe_v: 2346.0, real_estate_est: 40000.0, sheet_comment: "This is a perfect balance sheet", balance_sheet_status_id: 2},
                         {organization_id: 1, financial_source_type_id: 1, sheet_date: "2018-11-10", accounting_method_id: 1, total_asset: 72515.0, cash_on_hand: 7251.0, accounts_r: 14125.0, inventory: 12525.0, real_estate_value: 55515.0, ffe_v: 3215.0, accum_d: 6126.0, accum_a: 0.0, other_asset: -26242.0, total_liability: 32414.0, accounts_p: 17324.0, bank_debt: 3251.0, other_liability: 11839.0, cash_req: 51325.0, inv_req: 3413.0, curr_ffe_v: 6000.0, real_estate_est: 40000.0, sheet_comment: "I think this may be right?", balance_sheet_status_id: 2}
                     ])

AppraisalCalc.create!([
                          {organization_id: 1, year_one_conf: 0.1, year_two_conf: 0.3, year_three_conf: 0.4, projected_conf: 0.2, trans_found: nil, mult_of_rev: 0.52, mult_of_rev_var: 0.43, mult_of_sde: 2.76, mult_of_sde_var: 0.2, est_val_rev_conf: 0.0, est_value_sde_conf: 1.0, asset_approach_conf: 0.0, income_approach_conf: 0.5, market_approach_conf: 0.5, buyer_down_pmt: 0.2, debt_assum: 0.0, seller_finance: 0.1, comm_finance: 0.7, cont_earn_out: 0.0, report_date: "2018-11-10", interim_start: nil, interim_end: nil, business_type_id: 1, facility_type_id: 1, est_loan_fee: 0.025, term: 10, capt_expend_est: 2500.0, new_owner_sal: 75000.0, appraisal_calc_status_id: 2}
                      ])