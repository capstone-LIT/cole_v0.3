class CreateAccountingMethods < ActiveRecord::Migration[5.2]
  def change
    create_table :accounting_methods do |t|
      t.string :accounting_method_desc

      t.timestamps
    end
  end
end
