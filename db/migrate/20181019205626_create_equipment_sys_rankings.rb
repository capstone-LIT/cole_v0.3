class CreateEquipmentSysRankings < ActiveRecord::Migration[5.2]
  def change
    create_table :equipment_sys_rankings do |t|
      t.string :eqp_sys_desc

      t.timestamps
    end
  end
end
