require 'test_helper'

class RealEstateTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @real_estate_type = real_estate_types(:one)
  end

  test "should get index" do
    get real_estate_types_url
    assert_response :success
  end

  test "should get new" do
    get new_real_estate_type_url
    assert_response :success
  end

  test "should create real_estate_type" do
    assert_difference('RealEstateType.count') do
      post real_estate_types_url, params: { real_estate_type: { real_estate_type_desc: @real_estate_type.real_estate_type_desc } }
    end

    assert_redirected_to real_estate_type_url(RealEstateType.last)
  end

  test "should show real_estate_type" do
    get real_estate_type_url(@real_estate_type)
    assert_response :success
  end

  test "should get edit" do
    get edit_real_estate_type_url(@real_estate_type)
    assert_response :success
  end

  test "should update real_estate_type" do
    patch real_estate_type_url(@real_estate_type), params: { real_estate_type: { real_estate_type_desc: @real_estate_type.real_estate_type_desc } }
    assert_redirected_to real_estate_type_url(@real_estate_type)
  end

  test "should destroy real_estate_type" do
    assert_difference('RealEstateType.count', -1) do
      delete real_estate_type_url(@real_estate_type)
    end

    assert_redirected_to real_estate_types_url
  end
end
