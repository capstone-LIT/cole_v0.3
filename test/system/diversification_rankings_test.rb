require "application_system_test_case"

class DiversificationRankingsTest < ApplicationSystemTestCase
  setup do
    @diversification_ranking = diversification_rankings(:one)
  end

  test "visiting the index" do
    visit diversification_rankings_url
    assert_selector "h1", text: "Diversification Rankings"
  end

  test "creating a Diversification ranking" do
    visit diversification_rankings_url
    click_on "New Diversification Ranking"

    fill_in "Diversification Desc", with: @diversification_ranking.diversification_desc
    click_on "Create Diversification ranking"

    assert_text "Diversification ranking was successfully created"
    click_on "Back"
  end

  test "updating a Diversification ranking" do
    visit diversification_rankings_url
    click_on "Edit", match: :first

    fill_in "Diversification Desc", with: @diversification_ranking.diversification_desc
    click_on "Update Diversification ranking"

    assert_text "Diversification ranking was successfully updated"
    click_on "Back"
  end

  test "destroying a Diversification ranking" do
    visit diversification_rankings_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Diversification ranking was successfully destroyed"
  end
end
