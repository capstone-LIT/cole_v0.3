class CreateBalanceSheetStatuses < ActiveRecord::Migration[5.2]
  def change
    create_table :balance_sheet_statuses do |t|
      t.string :sheet_desc

      t.timestamps
    end
  end
end
