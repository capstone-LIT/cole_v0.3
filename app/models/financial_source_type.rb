class FinancialSourceType < ApplicationRecord
  has_many :balance_sheets
end
