class CreateHistOrgPerformanceRankings < ActiveRecord::Migration[5.2]
  def change
    create_table :hist_org_performance_rankings do |t|
      t.string :hist_org_performance_ranking_desc

      t.timestamps
    end
  end
end
