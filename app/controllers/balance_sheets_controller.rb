class BalanceSheetsController < ApplicationController
  before_action :set_balance_sheet, only: [:show, :edit, :update, :destroy]

  # GET /balance_sheets
  # GET /balance_sheets.json
  def index
    @balance_sheets = BalanceSheet.all
    if !current_user.is_broker?
      redirect_to '/client_home'
    end
  end

  # GET /balance_sheets/1
  # GET /balance_sheets/1.json
  def show
    if !current_user.is_broker? && current_user.get_org.get_balance_sheet != @balance_sheet
      redirect_to '/client_home'
    end
  end

  # GET /balance_sheets/new
  def new
    @balance_sheet = BalanceSheet.new
  end

  # GET /balance_sheets/1/edit
  def edit
    if !current_user.is_broker? && current_user.get_org.get_balance_sheet != @balance_sheet
      redirect_to '/client_home'
    end
  end

  # POST /balance_sheets
  # POST /balance_sheets.json
  def create
    @balance_sheet = BalanceSheet.new(balance_sheet_params)
    @balance_sheet.organization_id = current_user.get_org.id
    @balance_sheet.balance_sheet_status_id = 3
    respond_to do |format|
      if @balance_sheet.save
        if current_user.is_broker?
        format.html { redirect_to 'appraiser_home', notice: 'Balance sheet was successfully created.' }
        format.json { render :show, status: :created, location: @balance_sheet }
        else
          format.html { redirect_to @balance_sheet, notice: 'Balance sheet was successfully created.' }
          format.json { render :show, status: :created, location: @balance_sheet }
        end

      else
        format.html { render :new }
        format.json { render json: @balance_sheet.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /balance_sheets/1
  # PATCH/PUT /balance_sheets/1.json
  def update
    if current_user.is_broker? || @balance_sheet.balance_sheet_status_id != 1
    respond_to do |format|
      if @balance_sheet.update(balance_sheet_params)
        if current_user.is_broker?
          format.html { redirect_to '/appraiser_home' }
          format.json { render :show, status: :created, location: @balance_sheet }
        else
          format.html { redirect_to '/client_home' }
          format.json { render :show, status: :created, location: @balance_sheet }
        end
      else
        format.html { render :edit }
        format.json { render json: @balance_sheet.errors, status: :unprocessable_entity }
      end
    end
    else
      redirect_to '/client_home'
      end
  end

  # DELETE /balance_sheets/1
  # DELETE /balance_sheets/1.json
  def destroy
  @balance_sheet.balance_sheet_status_id = 3
    respond_to do |format|
      format.html { redirect_to balance_sheets_url, notice: 'Balance sheet was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_balance_sheet
      @balance_sheet = BalanceSheet.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def balance_sheet_params
      params.require(:balance_sheet).permit(:organization_id, :financial_source_type_id, :sheet_date, :accounting_method_id, :total_asset, :cash_on_hand, :accounts_r, :inventory, :real_estate_value, :ffe_v, :accum_d, :accum_a, :other_asset, :total_liability, :accounts_p, :bank_debt, :other_liability, :cash_req, :inv_req, :curr_ffe_v, :real_estate_est, :sheet_comment, :balance_sheet_status_id)
    end
end
