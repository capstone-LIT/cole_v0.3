json.extract! equipment_sys_ranking, :id, :eqp_sys_desc, :created_at, :updated_at
json.url equipment_sys_ranking_url(equipment_sys_ranking, format: :json)
