json.extract! org_status, :id, :status_desc, :created_at, :updated_at
json.url org_status_url(org_status, format: :json)
