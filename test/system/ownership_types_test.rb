require "application_system_test_case"

class OwnershipTypesTest < ApplicationSystemTestCase
  setup do
    @ownership_type = ownership_types(:one)
  end

  test "visiting the index" do
    visit ownership_types_url
    assert_selector "h1", text: "Ownership Types"
  end

  test "creating a Ownership type" do
    visit ownership_types_url
    click_on "New Ownership Type"

    fill_in "Ownership Type Desc", with: @ownership_type.ownership_type_desc
    click_on "Create Ownership type"

    assert_text "Ownership type was successfully created"
    click_on "Back"
  end

  test "updating a Ownership type" do
    visit ownership_types_url
    click_on "Edit", match: :first

    fill_in "Ownership Type Desc", with: @ownership_type.ownership_type_desc
    click_on "Update Ownership type"

    assert_text "Ownership type was successfully updated"
    click_on "Back"
  end

  test "destroying a Ownership type" do
    visit ownership_types_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Ownership type was successfully destroyed"
  end
end
