require 'test_helper'

class HistEarningRankingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @hist_earning_ranking = hist_earning_rankings(:one)
  end

  test "should get index" do
    get hist_earning_rankings_url
    assert_response :success
  end

  test "should get new" do
    get new_hist_earning_ranking_url
    assert_response :success
  end

  test "should create hist_earning_ranking" do
    assert_difference('HistEarningRanking.count') do
      post hist_earning_rankings_url, params: { hist_earning_ranking: { hist_earning_desc: @hist_earning_ranking.hist_earning_desc } }
    end

    assert_redirected_to hist_earning_ranking_url(HistEarningRanking.last)
  end

  test "should show hist_earning_ranking" do
    get hist_earning_ranking_url(@hist_earning_ranking)
    assert_response :success
  end

  test "should get edit" do
    get edit_hist_earning_ranking_url(@hist_earning_ranking)
    assert_response :success
  end

  test "should update hist_earning_ranking" do
    patch hist_earning_ranking_url(@hist_earning_ranking), params: { hist_earning_ranking: { hist_earning_desc: @hist_earning_ranking.hist_earning_desc } }
    assert_redirected_to hist_earning_ranking_url(@hist_earning_ranking)
  end

  test "should destroy hist_earning_ranking" do
    assert_difference('HistEarningRanking.count', -1) do
      delete hist_earning_ranking_url(@hist_earning_ranking)
    end

    assert_redirected_to hist_earning_rankings_url
  end
end
