json.extract! income_stmt_status, :id, :stmt_status_desc, :created_at, :updated_at
json.url income_stmt_status_url(income_stmt_status, format: :json)
