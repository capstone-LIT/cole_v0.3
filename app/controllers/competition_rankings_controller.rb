class CompetitionRankingsController < ApplicationController
  before_action :set_competition_ranking, only: [:show, :edit, :update, :destroy]

  # GET /competition_rankings
  # GET /competition_rankings.json
  def index
    @competition_rankings = CompetitionRanking.all
  end

  # GET /competition_rankings/1
  # GET /competition_rankings/1.json
  def show
  end

  # GET /competition_rankings/new
  def new
    @competition_ranking = CompetitionRanking.new
  end

  # GET /competition_rankings/1/edit
  def edit
  end

  # POST /competition_rankings
  # POST /competition_rankings.json
  def create
    @competition_ranking = CompetitionRanking.new(competition_ranking_params)

    respond_to do |format|
      if @competition_ranking.save
        format.html { redirect_to @competition_ranking, notice: 'Competition ranking was successfully created.' }
        format.json { render :show, status: :created, location: @competition_ranking }
      else
        format.html { render :new }
        format.json { render json: @competition_ranking.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /competition_rankings/1
  # PATCH/PUT /competition_rankings/1.json
  def update
    respond_to do |format|
      if @competition_ranking.update(competition_ranking_params)
        format.html { redirect_to @competition_ranking, notice: 'Competition ranking was successfully updated.' }
        format.json { render :show, status: :ok, location: @competition_ranking }
      else
        format.html { render :edit }
        format.json { render json: @competition_ranking.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /competition_rankings/1
  # DELETE /competition_rankings/1.json
  def destroy
    @competition_ranking.destroy
    respond_to do |format|
      format.html { redirect_to competition_rankings_url, notice: 'Competition ranking was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_competition_ranking
      @competition_ranking = CompetitionRanking.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def competition_ranking_params
      params.require(:competition_ranking).permit(:competition_desc)
    end
end
