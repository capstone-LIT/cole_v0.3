class AccountingMethod < ApplicationRecord
  has_many :balance_sheets
end
