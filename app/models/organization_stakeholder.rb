class OrganizationStakeholder < ApplicationRecord
  belongs_to :organization
  belongs_to :stakeholder
  belongs_to :stakeholder_rank
end
