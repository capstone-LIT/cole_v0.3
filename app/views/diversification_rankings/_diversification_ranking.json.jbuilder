json.extract! diversification_ranking, :id, :diversification_desc, :created_at, :updated_at
json.url diversification_ranking_url(diversification_ranking, format: :json)
