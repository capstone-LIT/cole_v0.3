class CreateStakeholderRanks < ActiveRecord::Migration[5.2]
  def change
    create_table :stakeholder_ranks do |t|
      t.string :stakeholder_rank_desc

      t.timestamps
    end
  end
end
