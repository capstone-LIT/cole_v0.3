require 'test_helper'

class OrgStatusesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @org_status = org_statuses(:one)
  end

  test "should get index" do
    get org_statuses_url
    assert_response :success
  end

  test "should get new" do
    get new_org_status_url
    assert_response :success
  end

  test "should create org_status" do
    assert_difference('OrgStatus.count') do
      post org_statuses_url, params: { org_status: { status_desc: @org_status.status_desc } }
    end

    assert_redirected_to org_status_url(OrgStatus.last)
  end

  test "should show org_status" do
    get org_status_url(@org_status)
    assert_response :success
  end

  test "should get edit" do
    get edit_org_status_url(@org_status)
    assert_response :success
  end

  test "should update org_status" do
    patch org_status_url(@org_status), params: { org_status: { status_desc: @org_status.status_desc } }
    assert_redirected_to org_status_url(@org_status)
  end

  test "should destroy org_status" do
    assert_difference('OrgStatus.count', -1) do
      delete org_status_url(@org_status)
    end

    assert_redirected_to org_statuses_url
  end
end
